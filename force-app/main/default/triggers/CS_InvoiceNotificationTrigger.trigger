/**
*  Created Date                                   : 12-Apr-21
*  Created By                                     : Pratigyan
*  Requirement Reference                          : CCM - 373
*  Description			                          : Trigger class for platform event

*****  MODIFICATION HISTORY
Version Number		Date		Last Modified By		Description/User Story Number

**/
trigger CS_InvoiceNotificationTrigger on CS_Invoice_Notification_PE__e (after insert) {
CS_TriggerDispatcher.Run(new CS_InvoiceNotificationTriggerHandler());
}