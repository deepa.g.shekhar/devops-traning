/**
*  Created Date                                   : 20-Apr-21
*  Created By                                     : Spoorthi B R
*  Requirement Reference                          : CCM - 365
*  Description			                          : Dispatcher to call the shipping notification handler when there is an insert, 
													update or delete action on the CS_Shipping_Notification_PE__e platform event.

*****  MODIFICATION HISTORY
Version Number		Date		Last Modified By		Description/User Story Number

**/
trigger CS_ShippingNotificationTrigger on CS_Shipping_Notification_PE__e (after insert) {    
    CS_TriggerDispatcher.run(new CS_ShippingNotificationTriggerHandler());
}