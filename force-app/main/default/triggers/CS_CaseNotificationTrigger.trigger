trigger CS_CaseNotificationTrigger on CS_Case_Notification_PE__e (after insert) {    
    
    CS_TriggerDispatcher.Run(new CS_CaseNotificationTriggerHandler());
}