trigger CS_CaseTrigger on Case (after insert, before insert, before update) {
    CS_TriggerDispatcher.Run(new CS_CaseTriggerHandler());
}