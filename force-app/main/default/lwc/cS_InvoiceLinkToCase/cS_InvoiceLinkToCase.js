import {
    LightningElement,
    api,
    wire,
    track
} from 'lwc';
// import getCaseList from '@salesforce/apex/CS_LinkInvoiceToCaseHandler.getCase';
import InvoiceDetailsByInvoiceNumber from '@salesforce/apex/CS_SAPSFInvoiceResponseData.getInvoiceDetailsByInvoiceNumber';
import InvoiceDetailsByPONumber from '@salesforce/apex/CS_SAPSFInvoiceResponseData.getInvoiceDetailsByPONumber';
import {
    ShowToastEvent
} from 'lightning/platformShowToastEvent'
const columns = [{
        label: 'SAP Invoice Number',
        fieldName: 'billingDocument',
        type: 'text',
        wrapText: true,
        initialWidth : 175
    },
    {
        label: 'SAP Order Number',
        fieldName: 'orderID',
        type: 'text',
        wrapText: true,
        initialWidth : 175
    },
    {
        label: 'PO Order Number',
        fieldName: 'purchaseOrderByCustomer',
        type: 'text',
        wrapText: true,
        initialWidth : 175
    },
    {
        label: 'Invoice Date',
        fieldName: 'billingDocumentDate',
        type: "date-local",
        typeAttributes: {
            month: "2-digit",
            day: "2-digit"
        },
        wrapText: true,
        initialWidth : 100
    },
    {
        label: 'Created Date',
        fieldName: 'creationDate',
        type: "date-local",
        typeAttributes: {
            month: "2-digit",
            day: "2-digit"
        },
        wrapText: true,
        initialWidth : 150
    },
    {
        label: 'Currency',
        fieldName: 'transactionCurrency',
        type: 'text',
        wrapText: true,
        initialWidth : 150
    },
    {
        label: 'Net Value',
        fieldName: 'totalNetAmount',
        type: 'text',
        wrapText: true,
        initialWidth : 150
    }
];
const FIELDS = [
    'Case.CS_Sold_To__r.CS_SAP_Customer_Id__c',    
];
import { getRecord } from 'lightning/uiRecordApi';
export default class CS_InvoiceLinkToCase extends LightningElement {

    @track error;
    @track loaded = true;
    @track invoiceList = [];
    @api recordId;
    @track caseListWholeData = [];
    columns = columns;
    
    @wire(getRecord, { recordId: '$recordId', fields: FIELDS })
    DataForui;
    connectedCallback() {
       // console.log('DataForui===>>',this.DataForui);
      //  console.log('this.recordId====>', this.recordId);
      /*   getCaseList({
                recId: this.recordId
            })
            .then((result) => {


                if (result !== null && typeof result !== "undefined" && JSON.parse(result) != null && JSON.parse(result).hasOwnProperty('d')) {
                   // console.log('result====>', JSON.parse(result).d.results);
                    let resultArray = [...JSON.parse(result).d.results];
                    let newArr = [];
                    resultArray.forEach(currentItem => {

                        for (const key in currentItem) {

                            if (key === 'CreationDate' || key === 'BillingDocumentDate') {
                               // console.log('key===67=>>', key);
                                let DateDate = Number(currentItem[key].replace('/Date(', '').replace(')/', ''));
                                currentItem[key] = new Date(DateDate)
                               // console.log('====DateDate=====>', DateDate);
                            }
                        }

                    });
                   // console.log('resultArray====>', resultArray);
                  //  this.caseList = [...resultArray];
                    this.caseListWholeData = [...resultArray];
                }


            })
            .catch((error) => {
               // console.log('error====>', error);
                this.error = error;
            }); */
    }

    inputValueObj = {};
    showDataOrNot = false;  
    handleChange(event) {
        
        if (event.target.dataset.inputid) {
            let inputValue = event.target.value;
            this.inputValueObj[event.target.dataset.inputid] = inputValue;

            for (const key in this.inputValueObj) {

                if (this.inputValueObj[key] === null || this.inputValueObj[key] === "") {
                    delete this.inputValueObj[key];
                }
            }
            console.log(this.inputValueObj);
        }


    }
    handleSearch() {
        console.log(this.DataForui);
       console.log('DataForui===>>',this.DataForui.data.fields.CS_Sold_To__r.value.fields.CS_SAP_Customer_Id__c.value);        
        this.loaded = false;
       console.log('this.loaded======>>',this.loaded);

        if (Object.keys(this.inputValueObj) && Object.keys(this.inputValueObj).length === 1) {

            let SAPCustomerId = this.DataForui.data.fields.CS_Sold_To__r && this.DataForui.data.fields.CS_Sold_To__r.value ?  this.DataForui.data.fields.CS_Sold_To__r.value.fields.CS_SAP_Customer_Id__c.value : null;
            console.log('====SAPCustomerId=====>>',SAPCustomerId);
            if (SAPCustomerId) {
                let apexMethod = this.callApexGenricMethod(Object.keys(this.inputValueObj)[0], Object.values(this.inputValueObj)[0], SAPCustomerId);
                console.log('======>>', apexMethod);
                if (apexMethod) {
                    apexMethod.then((result) => {
                            if (result && result.hasOwnProperty('d') && result.d.hasOwnProperty('results') && result.d.results && result.d.results.length>0) {

                                console.log('result====>', result.d.results);
                               
                                    let resultArray = [...JSON.parse(JSON.stringify(result.d.results))];
                                    resultArray.forEach(currentItem => {
    
                                        for (const key in currentItem) {
    
                                            if (key === 'creationDate' || key === 'billingDocumentDate') {
                                                // console.log('key===67=>>', key);
                                                let DateDate = Number(currentItem[key].replace('/Date(', '').replace(')/', ''));
                                                currentItem[key] = new Date(DateDate)
                                                // console.log('====DateDate=====>', DateDate);
                                            }
                                        }
    
                                    });
                                    // console.log('resultArray====>', resultArray);
                                    //  this.caseList = [...resultArray];
                                    this.invoiceList = [...resultArray];
                                    this.showDataOrNot = true;

                                

                            }
                            else{
                                this.showDataOrNot = false;
                                this.showTosterMsg('The given Value is not found in the system Please ensure that you enter a current data or contact the system Administrator', 'Warning', 'warning');
                            }
                            this.loaded = true;

                        })
                        .catch((error) => {
                            // console.log('error====>', error);
                            this.showDataOrNot = false;
                            this.error = error;
                            this.loaded = true;
                        });

                } else {
                    this.loaded = true;
                    this.showDataOrNot = false;
                    this.showTosterMsg(' "SAP Order Number" and "Delivery number" Fields are not configure yet', 'Warning', 'warning');
                }
            }
            

        } else if (Object.keys(this.inputValueObj).length > 1) {
            this.loaded = true;
            this.showDataOrNot = false;
            this.showTosterMsg('Please fill only one fields Data Should be filter on single field', 'Warning', 'warning');
        } else if (Object.keys(this.inputValueObj).length <= 0) {
            this.loaded = true;
            this.showDataOrNot = false;
            this.showTosterMsg('Please fill atleast one fields', 'Warning', 'warning');
        }
        
       




        /* setTimeout(() => {
            if ( Object.keys(this.inputValueObj) && Object.keys(this.inputValueObj).length === 1) {
          
                let newArr = [...this.caseListWholeData];
                newArr = newArr.filter(eachItem => {
    
                    for (const key in this.inputValueObj) {
    
                        
                        if ( eachItem[key].includes(this.inputValueObj[key])) {
                            return eachItem;
                        }
                    }
    
                });
               // console.log('newArr=====>>', newArr);
                this.caseList = [...newArr];
                this.loaded = true;
                this.showDataOrNot = true;
            }else if( Object.keys(this.inputValueObj).length > 1 ){
                this.loaded = true;
                this.showDataOrNot = false;
               // this.loaded = !this.loaded;
                this.showTosterMsg('Please fill only one fields Data Should be filter on single field','Warning','warning');
            }
            else if (Object.keys(this.inputValueObj).length <= 0){
                this.loaded = true;
                this.showDataOrNot = false;
              //  this.loaded = !this.loaded;
                this.showTosterMsg('Please fill atleast one fields','Warning','warning');
            }
        }, 0); */

      //  console.log('=====>>',Object.keys(this.inputValueObj).length);
       
    }
    @track invoiceData = [];

    handleNext() {
        //this.loaded = !this.loaded;
       // loaded = true;
        let selectedRecords = this.template.querySelector("lightning-datatable").getSelectedRows();
        //console.log('selectedRecords===>>', selectedRecords);
        if (selectedRecords != null && typeof selectedRecords !== 'undefined' && selectedRecords.length > 0 && selectedRecords.length === 1) {

            console.log('selectedRecords=137==>>', selectedRecords);

            for (const key in selectedRecords) {
                let objData = selectedRecords[key];
                if (selectedRecords[key].hasOwnProperty('purchaseOrderByCustomer')) {
                    let dateData = new Date(selectedRecords[key]['creationDate']);
                    console.log('====>>',dateData);                 
                    this.invoiceData.push({
                       // "Case__c": this.recordId,
                        //"CS_PO_Number_Order__c": selectedRecords[key]['purchaseOrderByCustomer'] ? selectedRecords[key]['purchaseOrderByCustomer'] : 0,
                        "CS_PO_Number__c": selectedRecords[key]['purchaseOrderByCustomer'] ? selectedRecords[key]['purchaseOrderByCustomer'] : 0,
                        "CS_SAP_Invoice_Number__c" : selectedRecords[key]['billingDocument'] ? selectedRecords[key]['billingDocument'] : '',
                        "CS_Creation_Date__c" : selectedRecords[key]['creationDate'] ? Date.parse(selectedRecords[key]['creationDate']) : null,
                        "CS_Invoice_Date__c" : selectedRecords[key]['billingDocumentDate'] ? Date.parse(selectedRecords[key]['billingDocumentDate']) : null,
                        "CS_Currency__c" : selectedRecords[key]['transactionCurrency'] ? selectedRecords[key]['transactionCurrency'] : '0',
                        "CS_Net_Value__c" : selectedRecords[key]['totalNetAmount'] ? Number(selectedRecords[key]['totalNetAmount']) : 0
                    });
                }

                
            }

            if (this.invoiceData != null && typeof this.invoiceData !== 'undefined' && this.invoiceData.length > 0) {

                let result = [...this.invoiceData];
                let caseid = this.recordId;
                const nextEvent = new CustomEvent('next', {
                    detail: {
                        result,
                        caseid
                    }
                });
                this.dispatchEvent(nextEvent);


               /*  insertInvoiceData({
                        lstObject: this.invoiceData
                    }).then((result) => {
                        console.log('in Next ====>>', result);
                        this.showTosterMsg('Selected Invoices has been Created in Salesforce', 'Invoice Created','success');
                        const nextEvent = new CustomEvent('next', {
                            detail: {
                                result
                            }
                        });
                        this.dispatchEvent(nextEvent);
                    })
                    .catch((error) => {
                        this.showTosterMsg(JSON.stringify(error), 'Error While Creating Invoice Records','error');
                        console.log('in Next ==error==>>', error);
                    }); */

            }
            //console.log('invoiceData======>>', this.invoiceData)
        } else if(selectedRecords && selectedRecords.length>1)
        {
            this.showTosterMsg('Please select only one Row form the Data Table', 'Error','error');
        }
        else{
            this.showTosterMsg('Please select Data from the Given Table', 'Error','error');
        }
    }

    showTosterMsg(message, title,variant) {
        const event = new ShowToastEvent({
            title: title,
            message: message,
            variant : variant
        });
        this.dispatchEvent(event);
    }

    formatDate(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();
    
        if (month.length < 2) 
            month = '0' + month;
        if (day.length < 2) 
            day = '0' + day;
    
        return [year, month, day].join('-');
    }

    callApexGenricMethod(typeOfField,fieldValue,SAPCustomerId){
        
        if(typeOfField && typeOfField==='billingDocument' && fieldValue)
        {
            
            return InvoiceDetailsByInvoiceNumber({"invoiceNumber":fieldValue,"sapCustomerId":SAPCustomerId});
        }
        else if(typeOfField && typeOfField==='purchaseOrderByCustomer' && fieldValue)
        {
            
            return InvoiceDetailsByPONumber({"poNumber":fieldValue,"sapCustomerId":SAPCustomerId});
        }
        else{
            
            return null;
        }
    }

}