/**
*  Created Date                                   : 27-Mar-21
*  Created By                                     : Bhushan K 
*  Requirement Reference                          : CCM-808
*  Description			                          : To display SAP retrieve data for All Deliveries 
                                                     Tab on Account 360 view.
*****  MODIFICATION HISTORY
Version Number		Date		Last Modified By		Description/User Story Number

**/
import { LightningElement, api, track, wire} from 'lwc';
import getAccountDetailsList from '@salesforce/apex/CS_Delivery_LWCController.getAccDetails';
import { getRecord } from 'lightning/uiRecordApi';

export default class CS_DeliveryCmp extends LightningElement {
    @track columns = [
    { label: 'Created date', fieldName: 'formattedCreationDate',type: 'date',},
    { label: 'Delivery number', fieldName: 'deliveryDocument',wrapText: true },
    { label: 'Ship to customer Code Number', fieldName: 'shipToParty',wrapText: true },
    { label: 'Delivery type', fieldName: 'formattedDeliveryDocumentType',wrapText: true },
    { label: 'Ship To Name', fieldName: 'addressID',wrapText: true },
    { label: 'Carrier', fieldName: 'businessPartnerName1',wrapText: true },
    { label: 'Proof Of Delivery', fieldName: 'formattedOverallProofOfDeliveryStatus',wrapText: true },
    { label: 'Good Movement Date', fieldName: 'formattedActualGoodsMovementDate',type: 'date' },
    ];
    @api recordId;
    @track deliveryLists;
    @track error; 
    connectedCallback() {
       
        getAccountDetailsList({
            RecordId: this.recordId
            })
            .then((result) => {
                this.deliveryLists = result;
            })
            .catch((error) => {
                this.error = error;
            }
            );
    }
}