/**
*  Created Date                                   : 27-Mar-21
*  Created By                                     : Pratigyan 
*  Requirement Reference                          : CCM-643
*  Description			                          : To dispay SAP retrieve data for Pending Deliveries Tab on Account 360 view.

*****  MODIFICATION HISTORY
Version Number		Date		Last Modified By		Description/User Story Number

**/

import { LightningElement, api, track, wire} from 'lwc';
import getAccountDetailsList from '@salesforce/apex/CS_Invoice_LwcController.getAccountDetails';
import { getRecord } from 'lightning/uiRecordApi';

export default class CS_InvoiceTab extends LightningElement {
    @track columns = [
    { label: 'Invoice Number', fieldName: 'billingDocument' },
    { label: 'Invoice Type', fieldName: 'formattedBillingDocumentType' },
    { label: 'Created date', fieldName: 'formattedCreationDate',type: 'date',},
    { label: 'Net value', fieldName: 'totalNetAmount' },
    { label: 'Status', fieldName: 'formattedOverallBillingStatus'},
    { label: 'ResolutionDate', fieldName: 'resolutionDate'}
    ];
    @api recordId;
    @track invoiceLists;
    @track error; 
    
    connectedCallback() {
        getAccountDetailsList({
            accountRecordId: this.recordId
            })
           
            .then((result) => {
                this.invoiceLists = result;
                console.log('this.invoiceLists==> '+this.invoiceLists);
                console.log('this.invoiceLists==> '+result.errorMessage);
                
            })
            .catch((error) => {
                this.error = error;
                console.log('this.error==> '+this.error);
                
            }
            );
    }
}