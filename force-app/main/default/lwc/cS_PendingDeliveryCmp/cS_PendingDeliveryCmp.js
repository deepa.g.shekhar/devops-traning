/**
*  Created Date                                   : 05-Apr-21
*  Created By                                     : Pratigyan 
*  Requirement Reference                          : CCM-1110
*  Description			                          : To dispay SAP retrieve data for Pending Deliveries Tab on Account 360 view.

*****  MODIFICATION HISTORY
Version Number		Date		Last Modified By		Description/User Story Number

**/

import { LightningElement, api, track, wire } from 'lwc';
import getAccountDetailsList from '@salesforce/apex/CS_PendingDelivery_LwcController.getDeliveryRecordDetails';
import { getRecord } from 'lightning/uiRecordApi';

export default class CS_PendingDeliveryCmp extends LightningElement {

    @track columns = [
        { label: 'Order Id', fieldName: 'salesOrder' },
        { label: 'Order Date', fieldName: 'formattedCreationDate',type: 'date' },
        { label: 'P.O. Id', fieldName: 'purchaseOrderByCustomer' },
        { label: 'P.O. Name', fieldName: 'customerPurchaseOrderType' },
       /* { label: 'Product', fieldName: 'material' },
        { label: 'Product name', fieldName: 'salesOrderItemText' },
        { label: 'Volume', fieldName: 'itemVolume' },
        { label: 'U.M.', fieldName: 'itemVolumeUnit' },
        { label: 'Quantity', fieldName: 'requestedQuantity' },*/
        ];

    @api recordId;
    @track deliveryList;
    @track error; 
    
    connectedCallback() {
        getAccountDetailsList({
            RecordId: this.recordId
            })
           
            .then((result) => {
                //this.results = result;
                this.deliveryList = result;
                console.log('result==>'+result);
                
            })
            .catch((error) => {
                console.log('error====>', error);
                this.error = error;
            }
            );
    }
}