/**
*  Created Date                                   : 14-Apr-21
*  Created By                                     : Spoorthi B R / Indu Narasimhamurthy
*  Requirement Reference                          : CCM-793
*  Description			                          : JS to display Order and Return Order.

*****  MODIFICATION HISTORY
Version Number		Date		Last Modified By		Description/User Story Number

**/
import { LightningElement, api, track} from 'lwc';
import getMergeList from '@salesforce/apex/CS_MergeOrder_LWCController.getAccountDetails';

export default class CS_OrderTabCmp extends LightningElement {
    @track columns = [    
        { label: 'Created Date', fieldName: 'formattedCreationDate', type: 'date' }, 
        { label: 'Order Number', fieldName: 'salesOrder'},        
        { label: 'PO Number', fieldName: 'purchaseOrderByCustomer' }, 
        { label: 'Order Type', fieldName: 'formattedSalesOrderType' },
        { label: 'Amount/Net Price', fieldName: 'totalNetAmount' },
        { label: 'Status', fieldName: 'formattedOverallSDProcessStatus' },
        { label: 'Delivery Date', fieldName: 'formattedDeliveryDate', type: 'date' },
        { label: 'Blocked Order', fieldName: 'formattedDeliveryBlockReason' },
        { label: 'Blocked invoice', fieldName: 'formattedHeaderBillingBlockReason' },
        { label: 'Reference Order', fieldName: 'referenceSDDocument' },      
    ];
    @api recordId;
    @track mergeOrderLists;
    @track error; 
    passClick(event){
        console.log(event.target.value);
        this.template.querySelector('c-generic-Spinner').handleClick(event.target.value);
        }   
        
    connectedCallback() {
      getMergeList({
        RecordId: this.recordId
              })
             
              .then((result) => {
                  this.mergeOrderLists = result;
                  console.log('order List', this.mergeOrderLists);
              })
              .catch((error) => {
                  this.error = error;
              }
              )
      }       
}