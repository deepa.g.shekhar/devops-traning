import {
    LightningElement,
    api,
    track
} from 'lwc';
import getInvoiceLineItemDetail from '@salesforce/apex/CS_SAPSFInvoiceLineItemsResponse.getLineItems';
import insertInvoiceDataToGetData from '@salesforce/apex/CS_LinkInvoiceToCaseHandler.insertInvoice';

// import insertInvoiceData from '@salesforce/apex/CS_SAPSFInvoiceDetails.insertInvoice';
const columns = [{
        label: 'SAP Invoice Number',
        fieldName: 'billingDocument',
        type: 'text',
        wrapText: true,
        initialWidth : 175
    },
    {
        label: 'SAP Order Number',
        fieldName: 'orderID',
        type: 'text',
        wrapText: true,
        initialWidth : 175
    },
    {
        label: 'PO Order Number',
        fieldName: 'purchaseOrderByCustomer',
        type: 'text',
        wrapText: true,
        initialWidth : 175
    },
    {
        label: 'Delivery Number',
        fieldName: '',
        type: 'text',
        wrapText: true,
        initialWidth : 175
    },
    {
        label: 'Invoice Date',
        fieldName: 'billingDocumentDate',
        type: "date-local",
        typeAttributes: {
            month: "2-digit",
            day: "2-digit"
        },
        wrapText: true,
        initialWidth : 100
    },
    {
        label: 'Created Date',
        fieldName: 'creationDate',
        type: "date-local",
        typeAttributes: {
            month: "2-digit",
            day: "2-digit"
        },
        wrapText: true,
        initialWidth : 150
    },
    {
        label: 'Currency',
        fieldName: 'transactionCurrency',
        type: 'text',
        wrapText: true,
        initialWidth : 150
    },
    {
        label: 'SAP Invoice Item Number',
        fieldName: 'billingDocumentItem',
        type: 'text',
        wrapText: true,
        initialWidth : 220
    },
    {
        label: 'Quantity',
        fieldName: 'billingQuantity',
        type: 'number',
        editable: true,
        wrapText: true,
        initialWidth : 150
    },
    {
        label: 'Net Value',
        fieldName: 'netAmount',
        type: 'text',
        wrapText: true,
        initialWidth : 150
    }
    
];
export default class CS_invoiceLineItemLinkToInvoice extends LightningElement {

    @api sapInvoiceNumber;
    loaded = false;
    @api caseId;
    @api invoiceData;
    isDisable=true;
    @track invoiceLineItemData = [];
    draftValues = [];
    columns = columns;
    error;
    listofBillingDocumentItem = [];
    draftedInvoiceLineItemValue = {};
    connectedCallback() {
        
       // this.loading = false;

        console.log('connectedCallback=======>>', this.sapInvoiceNumber);
        getInvoiceLineItemDetail({
                'Invoice': this.sapInvoiceNumber
            })
            .then(result => {

                if (result) {
                    let arr = [...JSON.parse(result).d.results];

                    arr.forEach(currentItem => {

                        for (const key in currentItem) {

                            if ((key === 'creationDate' || key === 'billingDocumentDate') && currentItem[key]) {
                                // console.log('key===67=>>', key);
                                let DateDate = Number(currentItem[key].replace('/Date(', '').replace(')/', ''));
                                currentItem[key] = new Date(DateDate)
                                // console.log('====DateDate=====>', DateDate);
                            }
                        }

                    });

                   // this.listofBillingDocumentItem = arr.map(currentItem => currentItem.BillingDocumentItem);
                    //console.log('arr=======>>', this.listofBillingDocumentItem);

                    this.invoiceLineItemData = [...arr];
                    console.log('invoiceLineItemData=======>>', this.invoiceLineItemData);
                    this.loaded = true;
                }

            })
            .catch(error => {
                this.error = error;
                this.loaded = true;
                console.log(error);
                // Error logic...
            });
    }
    handleSave(event) {
        let selectedRecords = this.template.querySelector("lightning-datatable").getSelectedRows();
        console.log('selectedRecords===>>', selectedRecords);
        if (selectedRecords) {
            
            let ClaimLineItem = [];
            let invoiceLineItem = [];
            let caseList = [];
            let draftedObjectKeys = [...Object.keys(this.draftedInvoiceLineItemValue)];

            for (const key in selectedRecords) {

                if (selectedRecords[key] && this.draftedInvoiceLineItemValue && Object.hasOwnProperty.call(this.draftedInvoiceLineItemValue, selectedRecords[key]['billingDocumentItem'])) {

                    const element = selectedRecords[key];
                    if(element['referenceSDDocumentCategory'] === 'J'){
                        element['referenceSDDocumentCategory'] =  element['referenceSDDocument'];
                    }
                    else if (element['referenceSDDocumentCategory'] !== 'J'){
                        element['referenceSDDocumentCategory'] =  '';
                    }
                    
                    ClaimLineItem.push({
                        'CS_Case__c': this.caseId,
                        'CS_Quantity__c': Number(this.draftedInvoiceLineItemValue[element['billingDocumentItem']]),
                        'CS_Net_Value__c': Number(element['netAmount']),
                        "CS_Plant__c": element['plant'] ? element['plant']: null,
                        "CS_Plant_Description__c": element['plant'] ?  element['plant'] + '-' + element['plantCity'] +  '-' +  element['plantRegion'] : null,
                        "CS_SAP_Reference_Delivery__c": element['referenceSDDocumentCategory'],
                        "CS_SAP_Order_Number__c": element['orderID'] ? element['orderID'] : null,
                        'CS_Invoice_Item_Number__c': element['billingDocumentItem'] ? element['billingDocumentItem'] : null
                    });
                    invoiceLineItem.push({
                        'Invoice__r': {
                            'CS_SAP_Invoice_Number__c': this.sapInvoiceNumber
                        },
                        'CS_SAP_Invoice_Item_Number__c': element['billingDocumentItem'] ? element['billingDocumentItem'] : null,
                            'CS_Quantity__c': element['billingQuantity'] ? Number(element['billingQuantity']) : 0 ,
                            'CS_SAP_Invoice_Number__c':element["billingDocument"] ? element["billingDocument"] : null,
                            'CS_Net_Value__c': element['netAmount'] ? Number(element['netAmount']) : 0,
                            "CS_SAP_Order_Number__c": element['orderID'] ? element['orderID'] : null,
                            "CS_PO_Number__c": element['purchaseOrderByCustomer'] ? element['purchaseOrderByCustomer']: null,
                            "CS_Invoice_Date__c": element['billingDocumentDate'] ? Date.parse(element['billingDocumentDate']):null,
                            "CS_Creation_Date__c": element['creationDate'] ? Date.parse(element['creationDate']) : null,
                            "CS_Currency__c": element['transactionCurrency'] ?  element['transactionCurrency'] : '0',
                            "CS_Net_Value__c": element['netAmount'] ? Number(element['netAmount']) : 0,
                            "CS_Plant__c": element['plant'] ? element['plant']: null,
                            "CS_Plant_Description__c": element['plant'] ?  element['plant'] + '-' + element['plantCity'] +  '-' +  element['plantRegion'] : null,
                            "CS_SAP_Reference_Delivery__c": element['referenceSDDocumentCategory']
                             
                    });

                } else {
                    const element = selectedRecords[key];
                    
                    
                    if(element['referenceSDDocumentCategory'] === 'J'){
                        element['referenceSDDocumentCategory'] =  element['referenceSDDocument'];
                    }
                    else if (element['referenceSDDocumentCategory'] !== 'J'){
                        element['referenceSDDocumentCategory'] =  null;
                    }
                    

                    invoiceLineItem.push({
                        'Invoice__r': {
                            'CS_SAP_Invoice_Number__c': this.sapInvoiceNumber
                        },
                        'CS_SAP_Invoice_Item_Number__c': element['billingDocumentItem'] ? element['billingDocumentItem'] : null,
                            'CS_Quantity__c': element['billingQuantity'] ? Number(element['billingQuantity']) : 0 ,
                            'CS_SAP_Invoice_Number__c':element["billingDocument"] ? element["billingDocument"] : null,
                            'CS_Net_Value__c': element['netAmount'] ? Number(element['netAmount']) : 0,
                            "CS_SAP_Order_Number__c": element['orderID'] ? element['orderID'] : null,
                            "CS_PO_Number__c": element['purchaseOrderByCustomer'] ? element['purchaseOrderByCustomer']: null,
                            "CS_Invoice_Date__c": element['billingDocumentDate'] ? Date.parse(element['billingDocumentDate']):null,
                            "CS_Creation_Date__c": element['creationDate'] ? Date.parse(element['creationDate']) : null,
                            "CS_Currency__c": element['transactionCurrency'] ?  element['transactionCurrency'] : '0',
                            "CS_Net_Value__c": element['netAmount'] ? Number(element['netAmount']) : 0,
                            "CS_Plant__c": element['plant'] ? element['plant']: null,
                            "CS_Plant_Description__c": element['plant'] ?  element['plant'] + '-' + element['plantCity'] +  '-' +  element['plantRegion'] : null,
                            "CS_SAP_Reference_Delivery__c": element['referenceSDDocumentCategory']
                    });

                }

            }
            console.log('lstCase===>>'+this.caseId);
            caseList.push({
                'CS_Invoice__r': {
                    'CS_SAP_Invoice_Number__c': this.sapInvoiceNumber
                },
                Id: this.caseId
            });
            console.log('ClaimLineItem===>>', ClaimLineItem);
            console.log('invoiceLineItem===>>', invoiceLineItem);

            console.log('this.invoiceData==>>'+JSON.parse(JSON.stringify(this.invoiceData)));
            if (this.invoiceData) {
                console.log('invoiceDate===>>',JSON.parse(JSON.stringify(this.invoiceData)));
                insertInvoiceDataToGetData({
                        "StrInvoiceData": JSON.stringify(this.invoiceData),
                       "strInvoiceLineItemData": JSON.stringify(invoiceLineItem),
                       "strClaimItemData": JSON.stringify(ClaimLineItem),
                       "strCaseData" : JSON.stringify(caseList)
                    }).then((result) => {
                        console.log('in Next ====>>', result);

                        const nextEvent = new CustomEvent('submitRecords');
                        this.dispatchEvent(nextEvent);

                        

                        // this.showTosterMsg('Selected Invoices has been Created in Salesforce', 'Invoice Created','success');
                        /*  const nextEvent = new CustomEvent('next', {
                             detail: {
                                 result
                             }
                         });
                         this.dispatchEvent(nextEvent); */
                    })
                    .catch((error) => {
                        this.error = error;
                        // this.showTosterMsg(JSON.stringify(error), 'Error While Creating Invoice Records','error');
                        // console.log('in Next ==error==>>', error);
                    });
            }

        }

    }
    handleCellChange(event) {
        let eachdraftValuesArr = event.detail.draftValues;
        let newObj = {};
        eachdraftValuesArr.forEach(function (item) {
            console.log(item);
            newObj[item.billingDocumentItem] = item.billingQuantity;
        });
        this.draftedInvoiceLineItemValue = Object.assign(this.draftedInvoiceLineItemValue, newObj);
        //  this.draftedInvoiceLineItemValue = {...newObj};
        console.log('in handleRowAction====>>', this.draftedInvoiceLineItemValue);
    }

    handleCancel(event){
        const nextEvent = new CustomEvent('submitRecords');
        this.dispatchEvent(nextEvent);
    }

    handleRowSelected(event){
        let selectedRows=event.detail.selectedRows;
        if(selectedRows && selectedRows.length>0)
        {
            this.isDisable = false;
        }else{
            this.isDisable = true;
        }

    }


}