/**
*  Created Date                                   : 14-Apr-21
*  Created By                                     : Spoorthi B R
*  Requirement Reference                          : CCM-793
*  Description			                          : Unit test code to merge Order and Return Order code.

*****  MODIFICATION HISTORY
Version Number		Date		Last Modified By		Description/User Story Number

**/
@isTest

public class CS_MergeOrder_LWCControllerTest {
    public static  String acc;    
    @testSetup
    static void createTestData() {
        integer i;
        List<Account> accountInsert = new List<Account>();
        accountInsert = CS_TestUtility.createAccount(); 
        for(i=0; i<accountInsert.size(); i++){
            if(accountInsert[i].CS_Partner_Function__c=='Sold To' && accountInsert[i].CS_SAP_Customer_Id__c!=' ' ){
                acc=accountInsert[i].Id;
                
            }
        } 
    }
    
    @isTest
    public static void getAccountDetailsTestPositive1() {
        Account acc = [SELECT Id,CS_SAP_Customer_Id__c,CS_Partner_Function__c FROM account WHERE CS_SAP_Customer_Id__c = '12345678' LIMIT 1];
        String recordId = acc.Id;        
        test.startTest();       
        String body1 = '{ "d": { "results": [ { "CustomerReturn": "string", "CustomerReturnType": "string", "SalesOrganization": "string", "DistributionChannel": "string", "OrganizationDivision": "string", "SalesGroup": "string", "SalesOffice": "string", "SalesDistrict": "string", "SoldToParty": "string", "CreationDate": "/Date(1492041600000)/", "RequestedDeliveryDate": "/Date(1492041600000)/" } ] } }';
        CS_MockHttpResponseGenerator mock1 = new CS_MockHttpResponseGenerator(body1,200,'OK');
        String body = '{ "d": { "results": [ { "CustomerReturn": "string", "CustomerReturnType": "string", "CreationDate": "/Date(1492041600000)/", "CreatedByUser": "string", "PurchaseOrderByCustomer": "string", "CustomerPurchaseOrderType": "string", "CustomerPurchaseOrderDate": "/Date(1492041600000)/", "CustomerReturnDate": "/Date(1492041600000)/", "RequestedDeliveryDate": "/Date(1492041600000)/", "HeaderBillingBlockReason": "string", "DeliveryBlockReason": "string", "OverallSDProcessStatus": "string", "FormattedHeaderBillingBlockReason": "", "FormattedOverallSDProcessStatus": "", "FormattedCustomerReturnType": "", "FormattedDeliveryBlockReason":"" } ] } }';
        CS_MockHttpResponseGenerator mock = new CS_MockHttpResponseGenerator(body,200,'OK');
        DateTime dt = system.now().addMonths(Integer.valueOf(Label.CS_Creationdate));
        String dtString = String.valueOf(dt);
        dtString = dtString.replace(' ','T');       
        string endPointUrl= 'callout:SAP_SHIFT/API_SALES_ORDER_SRV/A_SalesOrder?$filter=SoldToParty+eq+'+'\''+'12345678'+'\'+and+CreationDate+gt+datetime\''+dtString+'\'&$format=json';
        system.debug('end point url' +endPointUrl);
        string endPointUrl1 = 'callout:SAP_SHIFT/API_CUSTOMER_RETURN_SRV/A_CustomerReturn?$filter=SoldToParty+eq+'+'\''+'12345678'+'\'+and+CreationDate+gt+datetime\''+dtString+'\'&$format=json';
        Map<String, HttpCalloutMock> endPoint2TestResp = new Map<String,HttpCalloutMock>();
        endPoint2TestResp.put(endPointUrl1,mock);  
        endPoint2TestResp.put(endPointUrl,mock1);
        HttpCalloutMock multiCalloutMock = new CS_MultiMockHttpResponseGenerator(endPoint2TestResp);
        Test.setMock(HttpCalloutMock.class, multiCalloutMock);        
        CS_MergeOrder_LWCController.getAccountDetails(recordId);   
        test.stopTest();
    }    
    @isTest
    public static void getAccountDetailsTestPositive2() {
        Account acc = [Select Id,CS_SAP_Customer_Id__c,CS_Partner_Function__c from account where CS_SAP_Customer_Id__c = '12345678' LIMIT 1];
        String recordId = acc.Id;        
        test.startTest();       
        String body1 = '';
        CS_MockHttpResponseGenerator mock1 = new CS_MockHttpResponseGenerator(body1,500,'ERROR');
        String body = '';
        CS_MockHttpResponseGenerator mock = new CS_MockHttpResponseGenerator(body,500,'ERROR');
        DateTime dt = system.now().addMonths(Integer.valueOf(Label.CS_Creationdate));
        String dtString = String.valueOf(dt);
        dtString = dtString.replace(' ','T');       
        string endPointUrl= 'callout:SAP_SHIFT/API_SALES_ORDER_SRV/A_SalesOrder?$filter=SoldToParty+eq+'+'\''+'12345678'+'\'+and+CreationDate+gt+datetime\''+dtString+'\'&$format=json';
        string endPointUrl1 = 'callout:SAP_SHIFT/API_CUSTOMER_RETURN_SRV/A_CustomerReturn?$filter=SoldToParty+eq+'+'\''+'12345678'+'\'+and+CreationDate+gt+datetime\''+dtString+'\'&$format=json';
        Map<String, HttpCalloutMock> endPoint2TestResp = new Map<String,HttpCalloutMock>();
        endPoint2TestResp.put(endPointUrl1,mock);  
        endPoint2TestResp.put(endPointUrl,mock1);
        HttpCalloutMock multiCalloutMock = new CS_MultiMockHttpResponseGenerator(endPoint2TestResp);
        Test.setMock(HttpCalloutMock.class, multiCalloutMock);        
        CS_MergeOrder_LWCController.getAccountDetails(recordId);          
        test.stopTest();
    }   
    
    @isTest
    public static void getAccountDetailsTestPositive3() {
        Account acc = [Select Id,CS_SAP_Customer_Id__c,CS_Partner_Function__c from account where CS_SAP_Customer_Id__c = '12345678' LIMIT 1];
        String recordId = acc.Id; 
        test.startTest();
        Map<String, HttpCalloutMock> endPoint2TestResp = new Map<String,HttpCalloutMock>();
        String body1 = '{ "d": { "results": [ { "CustomerReturn": "string", "CustomerReturnType": "string", "SalesOrganization": "string", "DistributionChannel": "string", "OrganizationDivision": "string", "SalesGroup": "string", "SalesOffice": "string", "SalesDistrict": "string", "SoldToParty": "string", "CreationDate": "/Date(1492041600000)/", "RequestedDeliveryDate": "/Date(1492041600000)/" } ] } }';
        CS_MockHttpResponseGenerator mock1 = new CS_MockHttpResponseGenerator(body1,200,'OK');
        String body = '';
        CS_MockHttpResponseGenerator mock = new CS_MockHttpResponseGenerator(body,500,'ERROR');        
        DateTime dt = system.now().addMonths(Integer.valueOf(Label.CS_Creationdate));
        String dtString = String.valueOf(dt);
        dtString = dtString.replace(' ','T');       
        string endPointUrl= 'callout:SAP_SHIFT/API_SALES_ORDER_SRV/A_SalesOrder?$filter=SoldToParty+eq+'+'\''+'12345678'+'\'+and+CreationDate+gt+datetime\''+dtString+'\'&$format=json';
        string endPointUrl1 = 'callout:SAP_SHIFT/API_CUSTOMER_RETURN_SRV/A_CustomerReturn?$filter=SoldToParty+eq+'+'\''+'12345678'+'\'+and+CreationDate+gt+datetime\''+dtString+'\'&$format=json';
        endPoint2TestResp.put(endPointUrl1,mock);  
        endPoint2TestResp.put(endPointUrl,mock1);
        HttpCalloutMock multiCalloutMock = new CS_MultiMockHttpResponseGenerator(endPoint2TestResp);
        Test.setMock(HttpCalloutMock.class, multiCalloutMock);
        CS_MergeOrder_LWCController.getAccountDetails(recordId);   
        test.stopTest();
    }
    
    @isTest
    public static void getAccountDetailsTestPositive4() {
        Account acc = [Select Id,CS_SAP_Customer_Id__c,CS_Partner_Function__c from account where CS_SAP_Customer_Id__c = '12345678' LIMIT 1];
        String recordId = acc.Id; 
        test.startTest();
        Map<String, HttpCalloutMock> endPoint2TestResp = new Map<String,HttpCalloutMock>();
        String body = '{ "d": { "__count": "string", "results": [ { "SalesOrder": "string", "CustomerPaymentTerms": "string", "PaymentMethod": "string", "FixedValueDate": "/Date(1492041600000)/", "AssignmentReference": "string", "ReferenceSDDocument": "string", "ReferenceSDDocumentCategory": "string", "AccountingDocExternalReference": "string", "CustomerAccountAssignmentGroup": "string", "OverallTotalDeliveryStatus": "string", "OverallSDDocumentRejectionSts": "string", "to_Item": { "results": [ { "SDProcessStatus": "string", "DeliveryStatus": "string", "OrderRelatedBillingStatus": "string", "to_Partner": { "results": [ { "Supplier": "string", "Personnel": "string", "ContactPerson": "string" } ] }, "to_PricingElement": { "results": [ { "TransactionCurrency": "string", "ConditionControl": "string", "ConditionInactiveReason": "string", "ConditionClass": "string", "PrcgProcedureCounterForHeader": "string", "FactorForConditionBasisValue": 3.14, "StructureCondition": "string", "PeriodFactorForCndnBasisValue": 3.14, "PricingScaleBasis": "string", "VariantCondition": "string" } ] }, "to_ScheduleLine": { "results": [ { "CorrectedQtyInOrderQtyUnit": "0", "DelivBlockReasonForSchedLine": "string" } ] }, "to_Text": { "results": [ { "LongTextID": "string", "LongText": "string" } ] } } ] }, "to_Partner": { "results": [ { "ContactPerson": "string" } ] }, "to_PaymentPlanItemDetails": { "results": [ { "MaximumToBeAuthorizedAmount": "0", "PaytPlnForAuthorizationItem": "string", "PaytPlnItmForAuthorizationItem": "string" } ] }, "to_PricingElement": { "results": [ { "CndnIsRelevantForIntcoBilling": true, "ConditionIsManuallyChanged": true, "ConditionIsForConfiguration": true, "VariantCondition": "string" } ] }, "to_Text": { "results": [ { "LongTextID": "string", "LongText": "string" } ] } } ] } }';
        CS_MockHttpResponseGenerator mock = new CS_MockHttpResponseGenerator(body, 200,'OK'); 
        String body1 = '';
        DateTime dt = system.now().addMonths(Integer.valueOf(Label.CS_Creationdate));
        String dtString = String.valueOf(dt);
        dtString = dtString.replace(' ','T');       
        string endPointUrl= 'callout:SAP_SHIFT/API_SALES_ORDER_SRV/A_SalesOrder?$filter=SoldToParty+eq+'+'\''+'12345678'+'\'+and+CreationDate+gt+datetime\''+dtString+'\'&$format=json';
        string endPointUrl1 = 'callout:SAP_SHIFT/API_CUSTOMER_RETURN_SRV/A_CustomerReturn?$filter=SoldToParty+eq+'+'\''+'12345678'+'\'+and+CreationDate+gt+datetime\''+dtString+'\'&$format=json';
        CS_MockHttpResponseGenerator mock1 = new CS_MockHttpResponseGenerator(body1,500,'NOT OK'); 
        endPoint2TestResp.put(endPointUrl1,mock);  
        endPoint2TestResp.put(endPointUrl,mock1);
        HttpCalloutMock multiCalloutMock = new CS_MultiMockHttpResponseGenerator(endPoint2TestResp);
        Test.setMock(HttpCalloutMock.class, multiCalloutMock);
        CS_MergeOrder_LWCController.getAccountDetails(recordId);   
        test.stopTest();
    }
    @Istest
    public static void getAccountDetailsTestNegative(){ 
        CS_MergeOrder_LWCController.getAccountDetails(''); 
     }
}