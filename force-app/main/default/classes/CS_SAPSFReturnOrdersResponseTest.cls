/**
*  Created Date                                   : 09-April-21
*  Created By                                     : Narmada
*  Requirement Reference                          :  
*  Description Of Class                           : Test class for return orders LWC controller and return order response data classes

*****  MODIFICATION HISTORY
Version Number		Date		Last Modified By		Description/User Story Number

**/

@isTest
public class CS_SAPSFReturnOrdersResponseTest {
    
    public static FINAL String attributeName ='SAP_Customer_Return_Order_Service';
    public static CS_SAPSFReturnOrdersParser returnOrdersResponse=new CS_SAPSFReturnOrdersParser();
    public static  List<Account> accountInsert = new List<Account>();
    public static  String acc1;
    public static  String acc;
    public static  String acc2;
    public static  String acc3;
    
    @testSetup
    static void createTestData(){
        List<CS_SAP_Picklist_Mapping__c> mapInsert = new List<CS_SAP_Picklist_Mapping__c>();
        mapInsert = CS_TestUtility.createSAPPickListMapping(); 
        accountInsert = CS_TestUtility.createAccount(); 
        
    }  
    
    /**
*  Created Date                                   : 09-April-21
*  Created By                                     : Narmada
*  Requirement Reference                          :  
*  Description Of Method                          : Text method for CS_ReturnOrderLwcController and for CS_SAPSFReturnOrderResponseData catch exception

**/
    @isTest
    public static void getAccountDetailsTestPositive(){
        Integer i;        
        accountInsert = CS_TestUtility.createAccount();         
        for(i=0; i<accountInsert.size(); i++){
            if(accountInsert[i].CS_Partner_Function__c=='Sold To' && accountInsert[i].CS_SAP_Customer_Id__c!=null ){
                acc1=accountInsert[i].Id;
                
            }
            if(accountInsert[i].CS_Partner_Function__c=='Ship To'){
                acc2=accountInsert[i].Id;  
                
            }
            if(accountInsert[i].CS_SAP_Customer_Id__c==null){
                acc3=accountInsert[i].Id;  
                
            }
        }
               
        
        CS_Integration_Attributes__mdt integrationTest = new CS_Integration_Attributes__mdt();
        integrationTest = CS_TestUtility.fetchIntegrationAttributeRecord(attributeName);
        Test.startTest();
        String body = '{""d"":[""results"":{"__metadata":{"id":"","uri":"","type":"API_BILLING_DOCUMENT_SRV.A_BillingDocumentType"},"BillingDocument":"7001007147","SDDocumentCategory":"M","BillingDocumentCategory":"L","BillingDocumentType":"ZF2","CreationDate":"","CreationTime":"PT00H17M18S","LastChangeDate":null,"LastChangeDateTime":"","LogicalSystem":"SHACLNT100","SalesOrganization":"ES02","DistributionChannel":"10","Division":"C0","BillingDocumentDate":"","BillingDocumentIsCancelled":false,"CancelledBillingDocument":"","ForeignTrade":"","IsExportDelivery":"","BillingDocCombinationCriteria":"","ManualInvoiceMaintIsRelevant":false,"IsIntrastatReportingRelevant":false,"IsIntrastatReportingExcluded":false,"TotalNetAmount":"700.66","TransactionCurrency":"EUR","StatisticsCurrency":"EUR","TaxAmount":"28.03","TotalGrossAmount":"728.69","CustomerPriceGroup":"YM","PriceListType":"ZI","TaxDepartureCountry":"ES","VATRegistration":"ESB50871284","VATRegistrationOrigin":"B","VATRegistrationCountry":"ES","HierarchyTypePricing":"A","IsEUTriangularDeal":false,"SDPricingProcedure":"ZESA01","ShippingCondition":"ZH","PayerParty":"100035392","ContractAccount":"","CustomerPaymentTerms":"I060","PaymentMethod":"","PaymentReference":"7001007147","FixedValueDate":null,"AdditionalValueDays":"0","SEPAMandate":"","CompanyCode":"ES02","FiscalYear":"0000","AccountingDocument":"","CustomerAccountAssignmentGroup":"01","AccountingExchangeRateIsSet":false,"AbsltAccountingExchangeRate":"1.00000","AcctgExchangeRateIsIndrctQtan":false,"ExchangeRateDate":"","ExchangeRateType":"","DocumentReferenceID":"7001007147","AssignmentReference":"1822004270","DunningArea":"","DunningBlockingReason":"","DunningKey":"","InternalFinancialDocument":"","IsRelevantForAccrual":false,"SoldToParty":"100028060","PartnerCompany":"","PurchaseOrderByCustomer":"1822004270","CustomerGroup":"IB","Country":"ES","CityCode":"","SalesDistrict":"ES0001","Region":"29","County":"","CreditControlArea":"ES01","CustomerRebateAgreement":"","SalesDocumentCondition":"0000071420","OverallSDProcessStatus":"C","OverallBillingStatus":"A","AccountingPostingStatus":"C","AccountingTransferStatus":"C","BillingIssueType":"","InvoiceListStatus":"","OvrlItmGeneralIncompletionSts":"","OverallPricingIncompletionSts":"","BillingDocumentListType":"ZLR","BillingDocumentListDate":null}]}}';
        CS_MockHttpResponseGenerator mock = new CS_MockHttpResponseGenerator(body,200,'OK');
        Test.setMock(HttpCalloutMock.class, mock);
        CS_ReturnOrder_LwcController.getAccountDetails(acc1);
        CS_ReturnOrder_LwcController.getAccountDetails(acc2);
        CS_ReturnOrder_LwcController.getAccountDetails(acc3);
        CS_ReturnOrder_LwcController.getAccountDetails('');
        CS_ReturnOrder_LwcController.getAccountDetails('abc');        
        Test.stopTest();
        System.assertEquals(returnOrdersResponse.isSuccess=false, returnOrdersResponse.isSuccess);
        
    }
    
    /**
    *  Created Date                                   : 09-April-21
    *  Created By                                     : Narmada
    *  Requirement Reference                          :  
    *  Description Of Method                          : Test method for bad response body.
    **/
    @isTest
    public static void getReturnOrdersBySAPCustomerIdTestNegative1(){        
        CS_Integration_Attributes__mdt integrationTest = new CS_Integration_Attributes__mdt();
        integrationTest = CS_TestUtility.fetchIntegrationAttributeRecord(attributeName);
        Test.startTest();
        String body = '';
        CS_MockHttpResponseGenerator mock = new CS_MockHttpResponseGenerator(body,500,'OK');
        Test.setMock(HttpCalloutMock.class, mock);
        CS_SAPSFReturnOrderResponseData.getReturnOrdersBySAPCustomerId('12345');
        Test.stopTest();
        System.assertEquals(returnOrdersResponse.isSuccess=false, returnOrdersResponse.isSuccess);        
    }
    
    /**
    *  Created Date                                   : 09-April-21
    *  Created By                                     : Narmada
    *  Requirement Reference                          :  
    *  Description Of Method                          : Test method for passing correct response body.
    **/
    @isTest
    public static void getReturnOrdersBySAPCustomerIdTestPositive(){        
        CS_Integration_Attributes__mdt integrationTest = new CS_Integration_Attributes__mdt();
        integrationTest = CS_TestUtility.fetchIntegrationAttributeRecord(attributeName);
        Test.startTest();
        String body = '{ "d": { "results": [ { "CustomerReturn": "string", "CustomerReturnType": "string", "CreationDate": "/Date(1492041600000)/", "CreatedByUser": "string", "PurchaseOrderByCustomer": "string", "CustomerPurchaseOrderType": "string", "CustomerPurchaseOrderDate": "/Date(1492041600000)/", "CustomerReturnDate": "/Date(1492041600000)/", "RequestedDeliveryDate": "/Date(1492041600000)/", "HeaderBillingBlockReason": "string", "DeliveryBlockReason": "string", "OverallSDProcessStatus": "string", "FormattedHeaderBillingBlockReason": "", "FormattedOverallSDProcessStatus": "", "FormattedCustomerReturnType": "", "FormattedDeliveryBlockReason":"" } ] } }';
        CS_MockHttpResponseGenerator mock = new CS_MockHttpResponseGenerator(body,200,'OK');
        Test.setMock(HttpCalloutMock.class, mock);
        CS_SAPSFReturnOrderResponseData.getReturnOrdersBySAPCustomerId('123456');
        CS_SAPSFReturnOrderResponseData.getReturnOrdersBySAPCustomerId(' ');        
        Test.stopTest();
        System.assertEquals(returnOrdersResponse.isSuccess=true, returnOrdersResponse.isSuccess);        
    }
    
    /**
    *  Created Date                                   : 09-April-21
    *  Created By                                     : Narmada
    *  Requirement Reference                          :  
    *  Description Of Method                          : Test method for passing empty response body.
    **/ 
    @isTest
    public static void getReturnOrdersBySAPCustomerIdTestNegative2(){        
        CS_Integration_Attributes__mdt integrationTest = new CS_Integration_Attributes__mdt();
        integrationTest = CS_TestUtility.fetchIntegrationAttributeRecord(attributeName);
        Test.startTest();
        String body = '';
        CS_MockHttpResponseGenerator mock = new CS_MockHttpResponseGenerator(body,200,'OK');
        Test.setMock(HttpCalloutMock.class, mock);
        CS_SAPSFReturnOrderResponseData.getReturnOrdersBySAPCustomerId('12345');        
        Test.stopTest();
        System.assertEquals(returnOrdersResponse.isSuccess=false, returnOrdersResponse.isSuccess);        
    }
}