/**
*  Created Date                                   : 20-Apr-21
*  Created By                                     :	Pratigyan
*  Requirement Reference                          : CCM - 373
*  Description			                          : To unit test if the case Status AND EVENT is published based 
													on the response from iPaaS.
*****  MODIFICATION HISTORY
Version Number		Date		Last Modified By		Description/User Story Number

**/
@isTest
public class CS_InvoiceNotificationTriggerHelperTest {
    public static  String caseId;    
    @testSetup  
    /**
*  Created Date                                   : 20-Apr-21
*  Created By                                     :	Pratigyan
*  Requirement Reference                          : CCM - 373
*  Description			                          : To unit test if the case Status AND EVENT is published based 
													on the response from iPaaS.
*****  MODIFICATION HISTORY
Version Number		Date		Last Modified By		Description/User Story Number

**/
    
    Static void createTestData() {
        Integer i;
        List<Case> caseInsert = new List<Case>();        
        caseInsert = CS_TestUtility.createCase1();
        for(i=0; i<caseInsert.size(); i++) {
            if(caseInsert[i].CS_Invoice__r.CS_SAP_Invoice_Number__c == '7001061002'){
                caseId = caseInsert[i].Id;                
            }
        }
        OrderItem oiUpdate = CS_TestUtility.createOrderItem();
        oiUpdate.CS_Case__c = caseId;
        Update oiUpdate;
        
    } 
    @isTest
    public static void getEventDataPositive() {  
        System.Test.startTest();
        Case c1 = [SELECT id, status FROM Case LIMIT 1];
        string cId = String.ValueOf(c1.Id);
        string s = '{"CaseDetails": {"CaseID": "'+cId+'","CaseStatus": "Open","SAPInvoiceId": "12500","InvoiceType": "ZL2","SystemName": "SHIFT"}}';
        CS_Invoice_Notification_PE__e test1 = new CS_Invoice_Notification_PE__e();
        test1.CS_Case_Details__c = s;
        Database.SaveResult results = EventBus.publish(test1);
        System.Test.stopTest();
        system.assertEquals(true, results.isSuccess());
    }   
    @isTest
    public static void getEventDataNegative() {  
        System.Test.startTest();
        Case c1 = [SELECT id, status FROM Case LIMIT 1];
        string cId = String.ValueOf(c1.Id);
        string s = '{"CaseDetails": {"CaseID": "'+00123+'","CaseStatus": "Open","SAPInvoiceId": "12500","InvoiceType": "ZL2","SystemName": "SHIFT"}}';
        CS_Invoice_Notification_PE__e test1 = new CS_Invoice_Notification_PE__e();
        test1.CS_Case_Details__c = s;
        Database.SaveResult results = EventBus.publish(test1);
        System.Test.stopTest();
        system.assertEquals(true, results.isSuccess());
    }
}