/**
*  Created Date                                   : 25-Mar-2021
*  Created By                                     : Narmada
*  Requirement Reference                          : CCM 44
*  Description Of Method                          : Json Parsing for invoice response body

*****  MODIFICATION HISTORY
Version Number		Date		Last Modified By		Description/User Story Number
01                 25-Mar-2021                           CCM 44
**/

public class CS_InvoiceWrapper {
   	@AuraEnabled public InvoiceItem d;
   	@AuraEnabled public Boolean isSuccess;
   	@AuraEnabled public String errorMessage; 
	public class InvoiceItem {        
		@AuraEnabled public List<InvoiceResult> results;        
	}
    
	public class InvoiceResult {
         @AuraEnabled public String FormattedBillingDocumentType;
		 @AuraEnabled public String BillingDocument;
         @AuraEnabled public String SDDocumentCategory;
         @AuraEnabled public String BillingDocumentCategory;
         @AuraEnabled public String BillingDocumentType;
         @AuraEnabled public String CreationDate;
         @AuraEnabled public DateTime FormattedCreationDate;
         @AuraEnabled public String CreationTime;
         @AuraEnabled public String LastChangeDate;
         @AuraEnabled public String LastChangeDateTime;
         @AuraEnabled public String LogicalSystem;
         @AuraEnabled public String SalesOrganization;
         @AuraEnabled public String DistributionChannel;
         @AuraEnabled public String Division;
         @AuraEnabled public String BillingDocumentDate;
         @AuraEnabled public Boolean BillingDocumentIsCancelled;
		 @AuraEnabled public String CancelledBillingDocument;
		 @AuraEnabled public String ForeignTrade;
		 @AuraEnabled public String IsExportDelivery;
		 @AuraEnabled public String BillingDocCombinationCriteria;
		 @AuraEnabled public Boolean ManualInvoiceMaintIsRelevant;
		 @AuraEnabled public Boolean IsIntrastatReportingRelevant;
		 @AuraEnabled public Boolean IsIntrastatReportingExcluded;
		 @AuraEnabled public String TotalNetAmount;
		 @AuraEnabled public String TransactionCurrency;
		 @AuraEnabled public String StatisticsCurrency;
		 @AuraEnabled public String TaxAmount;
		 @AuraEnabled public String TotalGrossAmount;
		 @AuraEnabled public String CustomerPriceGroup;
		 @AuraEnabled public String PriceListType;
		 @AuraEnabled public String TaxDepartureCountry;
		 @AuraEnabled public String VATRegistration;
		 @AuraEnabled public String VATRegistrationOrigin;
		 @AuraEnabled public String VATRegistrationCountry;
    	 @AuraEnabled public String HierarchyTypePricing;
		 @AuraEnabled public String CustomerTaxClassification1;
		 @AuraEnabled public String CustomerTaxClassification2;
		 @AuraEnabled public String CustomerTaxClassification3;
		 @AuraEnabled public String CustomerTaxClassification4;
		 @AuraEnabled public String CustomerTaxClassification5;
		 @AuraEnabled public String CustomerTaxClassification6;
		 @AuraEnabled public String CustomerTaxClassification7;
		 @AuraEnabled public String CustomerTaxClassification8;
		 @AuraEnabled public String CustomerTaxClassification9;
		 @AuraEnabled public Boolean IsEUTriangularDeal;
		 @AuraEnabled public String SDPricingProcedure;
		 @AuraEnabled public String ShippingCondition;
		 @AuraEnabled public String IncotermsVersion;
		 @AuraEnabled public String IncotermsClassification;
		 @AuraEnabled public String IncotermsTransferLocation;
		 @AuraEnabled public String IncotermsLocation1;
		 @AuraEnabled public String IncotermsLocation2;
		 @AuraEnabled public String PayerParty;
		 @AuraEnabled public String ContractAccount;
		 @AuraEnabled public String CustomerPaymentTerms;
		 @AuraEnabled public String PaymentMethod;
		 @AuraEnabled public String PaymentReference;
		 @AuraEnabled public String FixedValueDate;
		 @AuraEnabled public String AdditionalValueDays;
		 @AuraEnabled public String SEPAMandate;
		 @AuraEnabled public String CompanyCode;
		 @AuraEnabled public String FiscalYear;
		 @AuraEnabled public String AccountingDocument;
		 @AuraEnabled public String CustomerAccountAssignmentGroup;
		 @AuraEnabled public Boolean AccountingExchangeRateIsSet;
		 @AuraEnabled public String AbsltAccountingExchangeRate;
		 @AuraEnabled public Boolean AcctgExchangeRateIsIndrctQtan;
		 @AuraEnabled public String ExchangeRateDate;
		 @AuraEnabled public String ExchangeRateType;
		 @AuraEnabled public String DocumentReferenceID;
		 @AuraEnabled public String AssignmentReference;
		 @AuraEnabled public String DunningArea;
		 @AuraEnabled public String DunningBlockingReason;
		 @AuraEnabled public String DunningKey;
		 @AuraEnabled public String InternalFinancialDocument;
		 @AuraEnabled public Boolean IsRelevantForAccrual;
		 @AuraEnabled public String SoldToParty;
		 @AuraEnabled public String PartnerCompany;
		 @AuraEnabled public String PurchaseOrderByCustomer;
		 @AuraEnabled public String CustomerGroup;
		 @AuraEnabled public String Country;
		 @AuraEnabled public String CityCode;
		 @AuraEnabled public String SalesDistrict;
		 @AuraEnabled public String Region;
		 @AuraEnabled public String County;
		 @AuraEnabled public String CreditControlArea;
		 @AuraEnabled public String CustomerRebateAgreement;
		 @AuraEnabled public String SalesDocumentCondition;
		 @AuraEnabled public String OverallSDProcessStatus;
		 @AuraEnabled public String OverallBillingStatus;
		 @AuraEnabled public String AccountingPostingStatus;
		 @AuraEnabled public String AccountingTransferStatus;
		 @AuraEnabled public String BillingIssueType;
		 @AuraEnabled public String InvoiceListStatus;
		 @AuraEnabled public String OvrlItmGeneralIncompletionSts;
		 @AuraEnabled public String OverallPricingIncompletionSts;
		 @AuraEnabled public String InvoiceClearingStatus;
		 @AuraEnabled public String BillingDocumentListType;
		 @AuraEnabled public String BillingDocumentListDate;
        
		
	}
    
@AuraEnabled(cacheable=true)
	public static CS_InvoiceWrapper parse(String json) {
     
		return (CS_InvoiceWrapper) System.JSON.deserialize(json, CS_InvoiceWrapper.class);
	}
}