/**
*  Created Date                                   : 12-Apr-21
*  Created By                                     : Pratigyan
*  Requirement Reference                          : CCM - 373
*  Description			                          : This interface is used to pdate case details in Salesforce from SAP. 
													Corresponds to I-01 interface from SAP → CDM

*****  MODIFICATION HISTORY
Version Number		Date		Last Modified By		Description/User Story Number

**/
public with sharing class CS_InvoiceNotificationTriggerHandler implements CS_TriggerInterface {
       /**
*  Created Date                                   : 12-April-21
*  Created By                                     : Pratigyan
*  Requirement Reference                          :  
*  Description Of Method                          : To Handle the invoice notification request trigger
**/  
    public void beforeInsert(List<SObject> newRecordList){
        
    }
    public void afterInsert(List<SObject> newRecordList,Map<Id, SObject> newRecordsMap){
        
        List<CS_Invoice_Notification_PE__e> invoicelist = new List<CS_Invoice_Notification_PE__e>();
        invoicelist = (List<CS_Invoice_Notification_PE__e>)newRecordList;
        CS_InvoiceNotificationTriggerHelper.getEventData(invoicelist);
       }
    
    public void beforeUpdate(Map<Id, SObject> newRecordsMap, Map<Id, SObject> oldRecordsMap){}
    public void afterUpdate(Map<Id, SObject> newRecordsMap, Map<Id, SObject> oldRecordsMap){}
    public void beforeDelete(List<SObject> oldRecordList,Map<Id, SObject> oldRecordsMap){}
    public void afterDelete(Map<Id, SObject> oldRecordsMap){}
    public void afterUndelete(List<SObject> newRecordList,Map<Id, SObject> newRecordsMap){}    

}