/**
*  Created Date                                   : 14-Apr-21
*  Created By                                     : Bajrang
*  Requirement Reference                          : CCM - 333
*  Description			                          : 
*****  MODIFICATION HISTORY
Version Number		Date		Last Modified By		Description/User Story Number

**/
public class CS_CaseNotificationTriggerHelper {
    /**
*  Created Date            : 20-Apr-2021
*  Created By              : Bajrang
*  Requirement Reference   : CCM - 333
*  Description Of Method   : Update case and related Order Products based on the response from iPaaS
**/  
    public static void getEventData(List<CS_Case_Notification_PE__e> csList){
        try {
            if(csList != NULL) {
                CS_SAPSFCaseNotificationParser caseResponse= new CS_SAPSFCaseNotificationParser();
                CS_SAPSFCaseNotificationParser orderResponse= new CS_SAPSFCaseNotificationParser();
                CS_SAPSFCaseNotificationParser orderItemResponse= new CS_SAPSFCaseNotificationParser();
                List<CS_SAPSFCaseNotificationParser> orderItemResponseList = new List<CS_SAPSFCaseNotificationParser>();
                List<CS_SAPSFCaseNotificationParser.OrderLineItemDetails> orderItemListListWrap = new List<CS_SAPSFCaseNotificationParser.OrderLineItemDetails>();
                List<Case> caseList =new List<Case>();
                List<Case> listOfCaseUpdate = new List<Case>();
                List<Order> listOfOrderUpdate = new List<Order>();
                List<Order> orderList = new List<Order>();
                List<OrderItem> listOfOrderItemUpdate = new List<OrderItem>();
                List<CS_Invoice__c> listOfInvUpdate = new List<CS_Invoice__c>();
                Set<Id> caseId=new Set<Id>();
                map<Id,String> ordListMap= new map<Id,String>();
                List<OrderItem> oList = new List<OrderItem>();
                List<OrderItem> oiUpdateList = new List<OrderItem>();
                
                for(CS_Case_Notification_PE__e caseNotification : csList){
                    String caseParse = '';
                    CS_SAPSFCaseNotificationParser.CaseDetails CaseDetailswrap;
                    
                    if(String.isNotBlank(caseNotification.CS_Case_Details__c)){
                        caseParse = caseNotification.CS_Case_Details__c;
                        caseResponse = CS_SAPSFCaseNotificationParser.parse(caseParse);
                        if(caseResponse != null && caseResponse.caseDetails != null){
                            CaseDetailswrap = caseResponse.caseDetails;
                        }                	
                    }
                    
                    else
                    {
                        throw new CS_CustomException(CS_CustomException.CS_INVALID_CASE_ID);
                    }
                    
                    String orderParse = '';
                    CS_SAPSFCaseNotificationParser.OrderDetails OrderDetailsWrap;
                    if(String.isNotBlank(caseNotification.CS_Order_Details__c)){
                        orderParse = caseNotification.CS_Order_Details__c;
                        orderResponse = CS_SAPSFCaseNotificationParser.parse(orderParse);
                        if(orderResponse != null && orderResponse.orderDetails != null){
                            OrderDetailsWrap = orderResponse.orderDetails;
                            
                        }
                        
                        else
                        {
                            throw new CS_CustomException(CS_CustomException.CS_ORDERRESPONSE);
                        }
                    }
                    
                    else
                    {
                        throw new CS_CustomException(CS_CustomException.CS_ORDERRESPONSE);
                    }
                    
                    String orderItemParse = ''; 
                    if(String.isNotBlank(caseNotification.CS_Order_Line_Items__c)){
                        orderItemParse = caseNotification.CS_Order_Line_Items__c;
                        orderItemResponse = CS_SAPSFCaseNotificationParser.parse(orderItemParse);
                        orderItemResponseList.add(orderItemResponse);
                        orderItemListListWrap.addAll(orderItemResponse.OrderLineItems.OrderLineItemDetails);              	
                    }
                    
                    else
                    {
                        throw new CS_CustomException(CS_CustomException.CS_ORDERPRODUCTRESPONSE);
                    }
                    String myIdPrefix = String.valueOf(CaseDetailswrap.CaseID).substring(0,3);
                    if(String.isNotBlank(CaseDetailswrap.Action) && CaseDetailswrap.Action == 'Update'){
                        if(String.isNotBlank(CaseDetailswrap.CaseID)&& ((CaseDetailswrap.CaseID.length() == 18)
                                                                        || (CaseDetailswrap.CaseID.length() == 15))
                           && (myIdPrefix == '500')){
                               if(OrderDetailsWrap.ReturnOrderType == CS_ConstantClass.RETURN_ORDER_TYPE || OrderDetailsWrap.ReturnOrderType == CS_ConstantClass.RETURN_ORDER_TYPE1){
                                   caseId.add(CaseDetailswrap.CaseID);
                               }
                               
                               else
                               {
                                   throw new CS_CustomException(CS_CustomException.CS_CASE_ID_RETURNORDERTYPE);
                               }
                           }
                        
                        else
                        {
                            throw new CS_CustomException(CS_CustomException.CS_INVALID_CASE_ID);
                        }
                    }
                }
                if(caseId != NULL) { 
                    caseList = [SELECT Id, Type, Status, CS_Invoice__r.CS_SAP_Invoice_Number__c, CS_Invoice__r.Id FROM Case WHERE ID IN : caseId]; 
                    Map<Id, List<OrderItem>> mapOrderItem = new Map<Id, List<OrderItem>>();
                    Map<String, String> retOrdListMap = new Map<String, STring>();
                    for(OrderItem oiList : [SELECT Id, CS_Case__c, CS_Order_Line_Status__c, CS_SAP_Invoice_Line_Id__c FROM OrderItem WHERE CS_Case__c in: caseId]) {
                        if(mapOrderItem.containsKey(oiList.CS_Case__c)){  
                            List<orderItem> ord_item = mapOrderItem.get(oiList.CS_Case__c);
                            ord_item.add(oiList);
                            mapOrderItem.put(oiList.CS_Case__c, ord_item);
                        } else {
                            mapOrderItem.put(oiList.CS_Case__c, new List<OrderItem> {oiList});
                        }
                    }
                    Map<Id, Id> mapOrderUpdate = new Map<Id, Id>();
                    orderList=[SELECT Id,Status,CS_Case_ID__c,CS_Return_Order_Type__c,CS_SAP_Net_Price__c,CS_SAP_Order_Number__c FROM Order Where CS_Case_ID__c IN :caseId];
                    if(orderList.isEmpty())
                    {
                        throw new CS_CustomException(CS_CustomException.CS_ORDERRESPONSE);
                    }
                    else{
                        for(Order orderData :orderList)
                        {
                            mapOrderUpdate.put(orderData.CS_Case_ID__c,orderData.Id);                
                        }
                    }       
                    Order ord =new Order();
                    if(mapOrderUpdate.keySet().contains(caseResponse.CaseDetails.CaseID)){
                        ord.Id=mapOrderUpdate.get(Id.valueOf(caseResponse.CaseDetails.CaseID));
                        ord.CS_SAP_Net_Price__c=Decimal.valueOf(orderResponse.orderDetails.SAPNetPrice);
                        ord.CS_SAP_Order_Number__c=orderResponse.orderDetails.SapOrderId;
                        ord.Status=orderResponse.orderDetails.OrderStatus;
                        listOfOrderUpdate.add(ord);
                    }
                    else
                    {
                        caseResponse.errorMessage=Label.CS_INVALID_CASE_ID;
                    }
                    for(CS_SAPSFCaseNotificationParser.OrderLineItemDetails wrapList : orderItemListListWrap) {
                        ordListMap.put(wrapList.SourceSystemOrderLIneId, wrapList.RejectionReason);
                    }                
                    oList = [SELECT Id, CS_Case__c, CS_Order_Line_Status__c,CS_Rejection_Reason__c, CS_SAP_Invoice_Line_Id__c FROM OrderItem WHERE Id in: OrdListMap.keyset() AND CS_Case__c IN: caseId];
                    for(OrderItem oi : oList) {
                        orderItem o = new OrderItem();
                        o.Id = oi.Id;
                        o.CS_Rejection_Reason__c = OrdListMap.get(oi.Id);
                        oiUpdateList.add(o);
                    } 
                    
                }
                else{
                    caseResponse.errorMessage = CS_CustomException.CS_CASE_ID_BLANK;
                }
                
                Update listOfOrderUpdate;
                Update oiUpdateList;
                
            }
        }
        catch (Exception ex) {
            ExceptionLogHandler.createErrorRecord(ex, 'CS_CaseNotificationTriggerHelper');
        }
    }
}