/**
*  Created Date                                   : 22-Feb-21
*  Created By                                     : Udaybhan Singh Kushwah
*  Requirement Reference                          : To be updated 
*  Description Of Method                          : This Method is to Create Exception log records.

*****  MODIFICATION HISTORY
Version Number		Date		Last Modified By		Description/User Story Number

**/
public class ExceptionLogHandler {
    
    /**
    *  Created Date            : 22-Feb-21
    *  Created By              : Udaybhan Singh Kushwah
    *  Requirement Reference   : To be updated 
    *  Description Of Method   : This Method to Create Exception log records.
    **/
    public static void createErrorRecord(Exception objException, String functionalityName){
        //check if the exception object is not null
        //check if the functionalityName is not null/blank/empty
        if(objException != null && String.isNotBlank(functionalityName)){
            CS_Exception_Log__c errorRecord = new CS_Exception_Log__c();
            errorRecord.CS_Functionality__c = functionalityName;
            errorRecord.CS_Exception_Message__c = objException.getMessage();
            errorRecord.CS_Exception_Type__c = objException.getTypeName();
            errorRecord.CS_Exception_Line_Number__c = objException.getLineNumber();
            errorRecord.CS_Exception_Stack_Trace__c = objException.getStackTraceString();        
            try{
                database.insert(errorRecord);   
            }
            catch(Exception ex){
                System.debug('exception');
            }
        }          
    }
}