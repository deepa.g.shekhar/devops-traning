/**
*  Created Date                                   : 05-April-21
*  Created By                                     : Udaybhan Singh Kushwah
*  Requirement Reference                          : To be updated 
*  Description Of Method                          : Test Class for Get SoldToId from Case.

*****  MODIFICATION HISTORY
Version Number		Date		Last Modified By		Description/User Story Number

**/
@isTest
public class CS_LinkInvoiceToCaseHandlerTest {
    
    public static testmethod void UnitTest(){
        
       string invoiceDate = JSON.serialize(CS_TestUtility.createInvoice(), true);
       string invoiceLineDate = JSON.serialize(CS_TestUtility.createInvoiceLineItem(),true);
        list<case> caseDate = CS_TestUtility.createCase1();
        string caseId = caseDate[0].id;
        string claimItem =  JSON.serialize( new list<CS_Claim_Line_Item__c>{new CS_Claim_Line_Item__c(CS_Case__c=caseId,CS_Quantity__c='10',CS_Net_Value__c=20)},true);
        CS_LinkInvoiceToCaseHandler.insertInvoice(invoiceDate, invoiceLineDate, claimItem, json.serialize(caseDate,true));
        CS_LinkInvoiceToCaseHandler.getCase(caseId);
    }
}