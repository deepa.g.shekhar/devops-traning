/**
*  Created Date                                   : 02-April-2021
*  Created By                                     : Indu
*  Requirement Reference                          : To be updated 
*  Description Of Method                          : Json Parsing for all order response body

*****  MODIFICATION HISTORY
Version Number		Date		Last Modified By		Description/User Story Number
01                 02-April-2021                            CCM-124
**/

public with sharing class CS_OrderParser {
    
    @AuraEnabled public OrderItem d;
    @AuraEnabled public Boolean isSuccess;
    @AuraEnabled public String errorMessage;
    @AuraEnabled public Boolean checkForSuccess;
    @AuraEnabled public String checkForError;
    @AuraEnabled public String checkForReturnError;
    
    public class OrderItem {        
        @AuraEnabled public List<OrderList> results;
    }
    
    public class OrderList {
        @AuraEnabled public String creationDate;
        @AuraEnabled public DateTime formattedCreationDate;
        @AuraEnabled public String salesOrder;
        @AuraEnabled public String purchaseOrderByCustomer;
        @AuraEnabled public String salesOrderType;
        @AuraEnabled public String formattedSalesOrderType;
        @AuraEnabled public String totalNetAmount;
        @AuraEnabled public String overallSDProcessStatus;
        @AuraEnabled public String formattedOverallSDProcessStatus;
        @AuraEnabled public String requestedDeliveryDate;
        @AuraEnabled public DateTime formattedDeliveryDate;
        @AuraEnabled public String deliveryBlockReason;
        @AuraEnabled public String formattedDeliveryBlockReason;
        @AuraEnabled public String headerBillingBlockReason;
        @AuraEnabled public String formattedHeaderBillingBlockReason;
        @AuraEnabled public String referenceSDDocument;
        @AuraEnabled public String createdByUser;            
    }
    
    @AuraEnabled(cacheable=true)
    public static CS_OrderParser parse(String json) {
        return (CS_OrderParser) System.JSON.deserialize(json, CS_OrderParser.class);
    }
}