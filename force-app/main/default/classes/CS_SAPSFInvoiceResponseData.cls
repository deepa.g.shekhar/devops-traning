/**
*  Created Date                                   : 25-Mar-2021 
*  Created By                                     : Narmada
*  Requirement Reference                          : CCM 44
*  Description Of Method                          : To get the invoice wrapper body

*****  MODIFICATION HISTORY
Version Number		Date		Last Modified By		Description/User Story Number
01                 25-Mar-2021                            44
**/

public With Sharing class CS_SAPSFInvoiceResponseData {
    
    public static String invoiceResponseBody;
    public static CS_SAPSFInvoiceParser invoiceItemsResponse;
    public static FINAL String attributeName = 'SAP_Billing_Document_Service';
    public static String entityType = 'Invoice';
    public static String billingDocumentType='Billing Type';
    public static String billingStatus='OverallBillingStatus';
    public static DateTime FormattedBillingDocumentDate;
    public static String userLanguage = UserInfo.getLanguage();
    public static string language;
    public static List<CS_SAP_Picklist_Mapping__c> invoicekeyValue = new List<CS_SAP_Picklist_Mapping__c>();
    public static Map<String,Map<String,String>> invoiceKeyValueMap = new Map<String,Map<String,String>>();
    public static Map<String,String> subInvoiceKeyValueMap;
    
    /**
*  Created Date                                   : 25-Mar-2021 
*  Created By                                     : Narmada
*  Requirement Reference                          : CCM 44
*  Description Of Method                          : To get the response from http and to parse the json response and to get the wrapper response
**/
    @AuraEnabled(cacheable=true)
    public static CS_SAPSFInvoiceParser getInvoiceDetailsByInvoiceNumber(String invoiceNumber, String sapCustomerId){
        try{
            invoiceItemsResponse=new CS_SAPSFInvoiceParser();
            if(String.isNotBlank(sapCustomerId) && String.isNotBlank(invoiceNumber)){
                invoiceItemsResponse.isSuccess=true;
                
                HTTPCalloutFramework.HttpCalloutWrapper wrapper=new HTTPCalloutFramework.HttpCalloutWrapper();
                
                Map<String,String> mapHeader=new Map<String,String>();
                mapHeader.put('accept','application/json');
                mapHeader.put('Content-Type','application/json');
                
                Map<String,String> mapURL=new Map<String,String>();
                
                String endpointURL='?$filter=SoldToParty+eq+\''+sapCustomerId+'\'&$filter=BillingDocument+eq+\''+invoiceNumber+'\'&$format=json&$top=1';
                
                wrapper.integrationAttributeName=attributeName;
                wrapper.mapOfHttpHeaders=mapHeader;
                wrapper.mapOfURLParameters =mapURL;
                wrapper.endPointParamString=endpointURL;
                
                
                
                HTTPResponse response=HTTPCalloutFramework.sendHTTPGETRequest(wrapper);
                
                invoiceResponseBody=response.getBody();
                if(response.getStatusCode()==200){
                    if(String.isNotBlank(response.getBody())){                        
                        invoiceItemsResponse = CS_SAPSFInvoiceParser.parse(invoiceResponseBody);
                        
                        for(CS_SAPSFInvoiceParser.InvoiceResult result:invoiceItemsResponse.d.results){
                            
                            Long longtimedate = long.valueOf( result.creationDate.substringBetween('(', ')') );
                            result.formattedCreationDate = DateTime.newInstance(longtimedate);
                            
                        }
                        invoiceItemsResponse.isSuccess = true;   
                    }
                    else{
                        invoiceItemsResponse.isSuccess=false; 
                        invoiceItemsResponse.errorMessage = Label.CS_InvoiceResponse;
                    }
                }
                else{
                    invoiceItemsResponse.isSuccess=false;   
                    invoiceItemsResponse.errorMessage=Label.CS_SAPException;
                }
                
                
            } // When Sap Customer Id not equals to null
            
            else { // When SAP Customer Id is blank
                invoiceItemsResponse.errorMessage = Label.CS_NullSAPId;
                invoiceItemsResponse.isSuccess = false;
            }            
        }
        catch(Exception ex){
            ExceptionLogHandler.createErrorRecord(ex, 'CS_SAPSFInvoiceResponseData');
        }         
        return invoiceItemsResponse;
    }
    
    
    /**
*  Created Date                                   : 25-Mar-2021 
*  Created By                                     : Narmada
*  Requirement Reference                          : CCM 44
*  Description Of Method                          : To get the response from http and to parse the json response and to get the wrapper response
**/
    @AuraEnabled(cacheable=true)
    public static CS_SAPSFInvoiceParser getInvoiceDetailsByPONumber(String poNumber, String sapCustomerId){
        try{
            invoiceItemsResponse=new CS_SAPSFInvoiceParser();
            if(String.isNotBlank(sapCustomerId) && String.isNotBlank(poNumber)){
                invoiceItemsResponse.isSuccess=true;
                
                HTTPCalloutFramework.HttpCalloutWrapper wrapper=new HTTPCalloutFramework.HttpCalloutWrapper();
                
                Map<String,String> mapHeader=new Map<String,String>();
                Mapheader.put('accept','application/json');
                Mapheader.put('Content-Type','application/json');
                
                Map<String,String> mapURL=new Map<String,String>();
                
                String endpointURL='?$filter=SoldToParty+eq+\''+sapCustomerId+'\'&$filter=PurchaseOrderByCustomer+eq+\''+poNumber+'\'&$format=json&$top=1';
                
                wrapper.integrationAttributeName=attributeName;
                wrapper.mapOfHttpHeaders=mapHeader;
                wrapper.mapOfURLParameters =mapURL;
                wrapper.endPointParamString=endpointURL;
                
                
                
                HTTPResponse response=HTTPCalloutFramework.sendHTTPGETRequest(wrapper);
                
                invoiceResponseBody=response.getBody();
                if(response.getStatusCode()==200){
                    if(String.isNotBlank(response.getBody())){                        
                        invoiceItemsResponse = CS_SAPSFInvoiceParser.parse(invoiceResponseBody);
                        
                        for(CS_SAPSFInvoiceParser.InvoiceResult result:invoiceItemsResponse.d.results){
                            
                            Long longtimedate = long.valueOf( result.creationDate.substringBetween('(', ')') );
                            result.formattedCreationDate = DateTime.newInstance(longtimedate);
                            
                        }
                        invoiceItemsResponse.isSuccess = true;   
                    }
                    else{
                        invoiceItemsResponse.isSuccess=false; 
                        invoiceItemsResponse.errorMessage = Label.CS_InvoiceResponse;
                    }
                }
                else{
                    invoiceItemsResponse.isSuccess=false;   
                    invoiceItemsResponse.errorMessage=Label.CS_SAPException;
                }
                
                
            } // When Sap Customer Id not equals to null
            
            else { // When SAP Customer Id is blank
                invoiceItemsResponse.errorMessage = Label.CS_NullSAPId; 
                invoiceItemsResponse.isSuccess = false;
            }            
        }
        catch(Exception ex){
            ExceptionLogHandler.createErrorRecord(ex, 'CS_SAPSFInvoiceResponseData');
        }         
        return invoiceItemsResponse;
    }
    
    /**
*  Created Date                                   : 25-Mar-2021 
*  Created By                                     : Narmada
*  Requirement Reference                          : CCM 44
*  Description Of Method                          : To get the response from http and to parse the json response and to get the wrapper response
**/
    @AuraEnabled(cacheable=true)
    public static CS_SAPSFInvoiceParser getInvoiceItems(String sapCustomerId){
        try{
            invoiceItemsResponse=new CS_SAPSFInvoiceParser();
            for(PicklistEntry value: User.LanguageLocalekey.getDescribe().getPicklistValues()) {
                if(value.getValue() == userLanguage) {
                    language = value.getValue();                    
                }                
            } 
            DateTime dt = system.now().addMonths(Integer.valueOf(Label.CS_Creationdate));
            String dtString = String.valueOf(dt);
            dtString = dtString.replace(' ','T');
            HTTPCalloutFramework.HttpCalloutWrapper wrapper=new HTTPCalloutFramework.HttpCalloutWrapper();
            
            Map<String,String> mapHeader=new Map<String,String>();
            Mapheader.put('accept','application/json');
            Mapheader.put('Content-Type','application/json');
            
            Map<String,String> mapURL=new Map<String,String>();
            
            String endpointURL='?$filter=SoldToParty+eq+\''+sapCustomerId+'\'+and+CreationDate+gt+datetime\''+dtString+'\'&$format=json';
            
            wrapper.integrationAttributeName=attributeName;
            wrapper.mapOfHttpHeaders=mapHeader;
            wrapper.mapOfURLParameters =mapURL;
            wrapper.endPointParamString=endpointURL;
            
            
            
            HTTPResponse response=HTTPCalloutFramework.sendHTTPGETRequest(wrapper);
            
            invoiceResponseBody=response.getBody();
            if(response.getStatusCode()==200){
                if(String.isNotBlank(response.getBody())){                        
                    invoiceItemsResponse = CS_SAPSFInvoiceParser.parse(invoiceResponseBody);
                    invoicekeyValue =[Select CS_Property_Name__c,CS_Key__c,CS_Value__c,CS_Entity__c,CS_Language__c 
                                      From CS_SAP_Picklist_Mapping__c 
                                      where CS_Entity__c =: entityType 
                                      AND CS_Language__c =: language];
                    
                    for (CS_SAP_Picklist_Mapping__c kVal : invoicekeyValue ){
                        subInvoiceKeyValueMap=new Map<String,String>();
                        if(invoiceKeyValueMap.keySet().contains(kVal.CS_Property_Name__c)){                            
                            subInvoiceKeyValueMap = invoiceKeyValueMap.get(kVal.CS_Property_Name__c);
                            subInvoiceKeyValueMap.put(kVal.CS_Key__c,kVal.CS_Value__c);
                            invoiceKeyValueMap.put(kVal.CS_Property_Name__c,subInvoiceKeyValueMap);
                        }
                        else{
                            subInvoiceKeyValueMap.put(kVal.CS_Key__c,kVal.CS_Value__c);
                            invoiceKeyValueMap.put(kVal.CS_Property_Name__c,subInvoiceKeyValueMap); 
                        }                        
                    }
                    for(CS_SAPSFInvoiceParser.InvoiceResult result:invoiceItemsResponse.d.results){
                        if(String.isNotBlank(result.creationDate)) { 
                            Long longtimedate = long.valueOf( result.creationDate.substringBetween('(', ')') );
                            result.formattedCreationDate = DateTime.newInstance(longtimedate);
                        }
                        if(String.isNotBlank(result.billingDocumentDate)) {
                            Long longtimeBillingDocumentDate = long.valueOf( result.billingDocumentDate.substringBetween('(', ')') );
                            formattedBillingDocumentDate = DateTime.newInstance(longtimeBillingDocumentDate);
                        } 
                        result.resolutionDate=result.customerPaymentTerms +'-'+ formattedBillingDocumentDate;
                        if(String.isNotBlank(result.billingDocumentType)){
                            if(invoiceKeyValueMap.keySet().contains(billingDocumentType)){                                
                                subInvoiceKeyValueMap = invoiceKeyValueMap.get(billingDocumentType);
                                if(subInvoiceKeyValueMap.containsKey(result.BillingDocumentType)){                                    
                                    result.formattedBillingDocumentType=result.billingDocumentType +' - '+ subInvoiceKeyValueMap.get(result.billingDocumentType);                                    
                                }
                            }
                        }
                        if(String.isNotBlank(result.overallBillingStatus)){
                            if(invoiceKeyValueMap.keySet().contains(billingStatus)){                                
                                subInvoiceKeyValueMap = invoiceKeyValueMap.get(billingStatus);
                                if(subInvoiceKeyValueMap.containsKey(result.overallBillingStatus)){                                    
                                    result.formattedOverallBillingStatus=result.overallBillingStatus +' - '+ subInvoiceKeyValueMap.get(result.overallBillingStatus);                                    
                                }
                            }
                        }
                    }
                    invoiceItemsResponse.isSuccess = true;   
                }
                else{
                    invoiceItemsResponse.isSuccess=false; 
                    invoiceItemsResponse.errorMessage = Label.CS_InvoiceResponse;
                }
            }
            else{
                invoiceItemsResponse.isSuccess=false;   
                invoiceItemsResponse.errorMessage=Label.CS_SAPException;
            }
            
        }
        catch(Exception ex){
            ExceptionLogHandler.createErrorRecord(ex, 'CS_SAPSFInvoiceResponseData');
        }         
        return invoiceItemsResponse;
    }
}