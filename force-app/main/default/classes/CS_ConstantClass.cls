public class CS_ConstantClass {
    
    public static FINAL String ACCOUNT_SOLD_TO = 'Sold To';
    public static FINAL String ACCOUNT_SHIP_TO = 'Ship To';
    public static FINAL String PICKLIST_MAPPING = 'Orders';
    public static FINAL String RETURN_ORDER_TYPE = 'ZREA';
    public static FINAL String RETURN_ORDER_TYPE1 = 'ZREB';
    public static FINAL String INVOICE_CASE_STATUS = 'Closed';
    public static FINAL String INVOICE_DOA_STATUS = 'Approved';
    public static FINAL String INVOICE_TYPE = 'ZG2';
    public static FINAL String INVOICE_TYPE1 = 'ZG3';
    public static FINAL String INVOICE_TYPE2 = 'ZGFR';
    public static FINAL String INVOICE_TYPE3 = 'ZL2';
    public static FINAL String INVOICE_TYPE4 = 'ZDFR';
    public static FINAL String INVOICE_IDPREFIX = '500';
    public static FINAL String GLOBAL_ENTITLEMENT_FOR_REQUEST_NAME = 'Global Entitlement Request';    
    public static FINAL String CASE_REQUEST = 'Request';
       
}