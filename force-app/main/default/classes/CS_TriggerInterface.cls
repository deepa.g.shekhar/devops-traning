public interface CS_TriggerInterface {
    void beforeInsert(List<SObject> newRecordList);
    void afterInsert(List<SObject> newRecordList,Map<Id, SObject> newRecordsMap);
    void beforeUpdate(Map<Id, SObject> newRecordsMap, Map<Id, SObject> oldRecordsMap);
    void afterUpdate(Map<Id, SObject> newRecordsMap, Map<Id, SObject> oldRecordsMap);
    void beforeDelete(List<SObject> oldRecordList,Map<Id, SObject> oldRecordsMap);
    void afterDelete(Map<Id, SObject> oldRecordsMap);
    void afterUndelete(List<SObject> newRecordList,Map<Id, SObject> newRecordsMap);
}