/**
*  Created Date                                   : 05-Apr-21
*  Created By                                     : Harshita Gupta
*  Requirement Reference                          : To be updated 
*  Description			                          : Utility Class 

*****  MODIFICATION HISTORY
Version Number		Date		Last Modified By		Description/User Story Number

**/
@isTest
public class CS_TestUtility {
    
    /**
*  Created Date            : 5-Apr-2021
*  Created By              : Harshita Gupta
*  Requirement Reference   : To be updated 
*  Description Of Method   : Method for the creating Account
**/
    @isTest
    public static List<Account> createAccount(){
        List<Account> allaccount = new List<Account>();
        Account acc = new Account(Name = 'Account1', CS_SAP_Customer_Id__c = '12345678',CS_Partner_Function__c = 'Sold To');
        allaccount.add(acc);
        Account acc1 = new Account(Name = 'Account2',CS_Partner_Function__c = 'Ship To', ParentId = acc.Id);
        allaccount.add(acc1);
        Account acc2 = new Account(Name = 'Account3', CS_SAP_Customer_Id__c = ' ',CS_Partner_Function__c = 'Sold To');
        allaccount.add(acc2);
        Account acc3 = new Account(Name = 'Account5', CS_SAP_Customer_Id__c = '1234678',CS_Partner_Function__c = 'Ship To');
        allaccount.add(acc3);
        Account acc4 = new Account(Name = 'Account4');
        allaccount.add(acc4);
        insert allaccount;
        return allaccount;
    }
    /**
*  Created Date            : 15-Apr-2021
*  Created By              : Harshita Gupta
*  Requirement Reference   : To be updated 
*  Description Of Method   : Method for the creating Invoice
**/
    @isTest
    public static List<CS_Invoice__c> createInvoice(){
        List<CS_Invoice__c> allInvoice = new List<CS_Invoice__c>(); 
        CS_Invoice__c   inv = new CS_Invoice__c(CS_SAP_Invoice_Number__c = '7001061002');
        allInvoice.add(inv);
        
        return allInvoice;
    }
    /**
*  Created Date            : 15-Apr-2021
*  Created By              : Harshita Gupta
*  Requirement Reference   : To be updated 
*  Description Of Method   : Method for the creating Invoice line items.
**/
    @isTest
    public static List<CS_Invoice_Line_Item__c> createInvoiceLineItem(){
        List<CS_Invoice_Line_Item__c> allInvoiceLineItem = new List<CS_Invoice_Line_Item__c>(); 
        CS_Invoice_Line_Item__c   invLineItem = new CS_Invoice_Line_Item__c(Invoice__r =new CS_Invoice__c(CS_SAP_Invoice_Number__c='7001061002'));
        allInvoiceLineItem.add(invLineItem);
        
        return allInvoiceLineItem;
    }
    /**
*  Created Date            : 15-Apr-2021
*  Created By              : Harshita Gupta
*  Requirement Reference   : To be updated 
*  Description Of Method   : Method for the creating Case.
**/
    @isTest
    public static list<case> createCase1(){
        Case sobj = new Case(
            Status = 'Open',                                
            Priority = 'Medium',                                          
            Origin = 'Email',                               
            Subject = 'Damage packaging of my parcel',      
            Description = 'Damage packaging of my parcel',  
            SuppliedEmail = 'j.thompson@gmail.com'         
        );
        insert sobj;
        return new list<case>{new case(id=sobj.id , CS_Invoice__r=new CS_Invoice__c(CS_SAP_Invoice_Number__c='7001061002'))};
            }
    
    /**
*  Created Date            : 12-Apr-2021
*  Created By              : Narmada
*  Requirement Reference   : To be updated 
*  Description Of Method   : method to create SAP Picklist mapping records 
**/
    public static list<CS_SAP_Picklist_Mapping__c> createSAPPickListMapping(){
        String language;
        String userLanguage = UserInfo.getLanguage();
        for(PicklistEntry value: User.LanguageLocalekey.getDescribe().getPicklistValues()) {
            if(value.getValue() == userLanguage) {
                language = value.getValue();
                
            }
        }
        List<CS_SAP_Picklist_Mapping__c> mapping=new List<CS_SAP_Picklist_Mapping__c>();
        CS_SAP_Picklist_Mapping__c map1=new CS_SAP_Picklist_Mapping__c(CS_Property_Name__c='DeliveryBlockReason',CS_Key__c='ZD',CS_Value__c='',CS_Language__c=language,CS_Entity__c='Order');
        mapping.add(map1);
        CS_SAP_Picklist_Mapping__c map2=new CS_SAP_Picklist_Mapping__c(CS_Property_Name__c='CustomerReturnType',CS_Key__c='ZREA',CS_Value__c='',CS_Language__c=language,CS_Entity__c='Order');
        mapping.add(map2);
        CS_SAP_Picklist_Mapping__c map3=new CS_SAP_Picklist_Mapping__c(CS_Property_Name__c='HeaderBillingBlockReason',CS_Key__c='',CS_Value__c='',CS_Language__c=language,CS_Entity__c='Order');
        mapping.add(map3);
        CS_SAP_Picklist_Mapping__c map4=new CS_SAP_Picklist_Mapping__c(CS_Property_Name__c='OverallSDProcessStatus',CS_Key__c='C',CS_Value__c='',CS_Language__c=language,CS_Entity__c='Order');
        mapping.add(map4);
        CS_SAP_Picklist_Mapping__c map5=new CS_SAP_Picklist_Mapping__c(CS_Property_Name__c='DeliveryBlockReason',CS_Key__c=' ',CS_Value__c='',CS_Language__c=language,CS_Entity__c='Order');
        mapping.add(map5);
        CS_SAP_Picklist_Mapping__c map6=new CS_SAP_Picklist_Mapping__c(CS_Property_Name__c='OverallProofOfDeliveryStatus',CS_Key__c='C',CS_Value__c='Quantities were verified and confirmed',CS_Language__c=language,CS_Entity__c='Delivery');
        mapping.add(map6);
        CS_SAP_Picklist_Mapping__c map7 = new CS_SAP_Picklist_Mapping__c(CS_Property_Name__c='DeliveryDocumentType',CS_Key__c='ZLRA',CS_Value__c='',CS_Language__c=language,CS_Entity__c='Delivery');
        mapping.add(map7);
        CS_SAP_Picklist_Mapping__c map8=new CS_SAP_Picklist_Mapping__c(CS_Property_Name__c='SalesOrderType',CS_Key__c='ZREA',CS_Value__c='',CS_Language__c=language,CS_Entity__c='Order');
        mapping.add(map8);
        CS_SAP_Picklist_Mapping__c map9=new CS_SAP_Picklist_Mapping__c(CS_Property_Name__c='Billing Type',CS_Key__c='ZF2',CS_Value__c='',CS_Language__c=language,CS_Entity__c='Invoice');
        mapping.add(map9);
        CS_SAP_Picklist_Mapping__c map10=new CS_SAP_Picklist_Mapping__c(CS_Property_Name__c='OverallBillingStatus',CS_Key__c='A',CS_Value__c='Completed',CS_Language__c=language,CS_Entity__c='Invoice');
        mapping.add(map10);
        CS_SAP_Picklist_Mapping__c map11=new CS_SAP_Picklist_Mapping__c(CS_Property_Name__c='OverallBillingStatus',CS_Key__c='A',CS_Value__c='Completed',CS_Language__c=language,CS_Entity__c='Invoice');
        mapping.add(map11);
        insert mapping;
        return mapping;
    }
    
    /**
*  Created Date            : 5-Apr-2021
*  Created By              : Harshita Gupta
*  Requirement Reference   : To be updated 
*  Description Of Method   : method for fetching Integration Attribute metadata
**/    
    public static CS_Integration_Attributes__mdt fetchIntegrationAttributeRecord(String IntegrationAttribute){
        CS_Integration_Attributes__mdt attributeMetadataTest = new CS_Integration_Attributes__mdt();
        if(String.isNotBlank(IntegrationAttribute)){
            attributeMetadataTest = [SELECT ID, CS_Endpoint_URL__c, CS_Method_Type__c, CS_Timeout__c, CS_Name__c , CS_System_Name__c
                                     FROM CS_Integration_Attributes__mdt
                                     WHERE DeveloperName =: IntegrationAttribute
                                     LIMIT 1];
            
        }
        return attributeMetadataTest;
    }
    
    /**
*  Created Date            : 20-Apr-2021
*  Created By              : Spoorthi B R
*  Requirement Reference   : CCM - 365 
*  Description Of Method   : Method for the creating Product.
**/
    
    public static Product2 createProduct() {
        Product2 prodCreate = new Product2();
        prodCreate.Name = 'Test Product';
        insert prodCreate;
        return prodCreate;
    } 
    
    /**
*  Created Date            : 20-Apr-2021
*  Created By              : Spoorthi B R
*  Requirement Reference   : CCM - 365 
*  Description Of Method   : Method for the creating Price Book.
**/
    
    public static Pricebook2 createPriceBook() {
        Pricebook2 priceBookCreate = new Pricebook2();        
        priceBookCreate.IsActive = TRUE;       
        priceBookCreate.Id = Test.getStandardPricebookId();           
        return priceBookCreate;
    }
    
    /**
*  Created Date            : 20-Apr-2021
*  Created By              : Spoorthi B R
*  Requirement Reference   : CCM - 365
*  Description Of Method   : Method for the creating Price Book Entry.
**/
    
    public static PricebookEntry createPricebookEntry() {
        Pricebook2 pbId = CS_TestUtility.createPriceBook(); 
        pbId.Description = 'Make it standard';
        Update pbId;
        Product2 prod = CS_TestUtility.createProduct();
        Id standardPB = pbId.Id;
        PricebookEntry createStdPriceBookEntry = new PricebookEntry();
        createStdPriceBookEntry.IsActive = TRUE;
        createStdPriceBookEntry.UnitPrice = 20;
        createStdPriceBookEntry.Pricebook2Id = standardPB;
        createStdPriceBookEntry.Product2Id = prod.Id;
        createStdPriceBookEntry.UseStandardPrice = FALSE;
        Insert createStdPriceBookEntry;
        return createStdPriceBookEntry;        
    }
    
    /**
*  Created Date            : 20-Apr-2021
*  Created By              : Spoorthi B R
*  Requirement Reference   : CCM - 365 
*  Description Of Method   : Method for the creating Contract.
**/
    
    public static Contract createContract() {
        Id acc;    
        integer i;
        List<Account> accountInsert = new List<Account>();
        accountInsert = CS_TestUtility.createAccount(); 
        for(i=0; i<accountInsert.size(); i++){
            if(accountInsert[i].CS_Partner_Function__c=='Sold To' && accountInsert[i].CS_SAP_Customer_Id__c!=' ' ){
                acc=accountInsert[i].Id;
                
            }
        }
        Contract createContract = new Contract();
        createContract.AccountId = acc;
        createContract.Status = 'Draft';
        createContract.StartDate = system.today() - 2;
        createContract.ContractTerm = 4;        
        insert createContract;
        createContract.Status = 'Activated';
        Update createContract;        
        return createContract;
    }
    
    /**
*  Created Date            : 20-Apr-2021
*  Created By              : Spoorthi B R
*  Requirement Reference   : CCM - 365
*  Description Of Method   : Method for the creating Order.
**/
    
    public static Order createOrder() {
        Id acc;    
        integer i;
        List<Account> accountInsert = new List<Account>();
        accountInsert = CS_TestUtility.createAccount(); 
        for(i=0; i<accountInsert.size(); i++){
            if(accountInsert[i].CS_Partner_Function__c=='Sold To' && accountInsert[i].CS_SAP_Customer_Id__c!=' ' ){
                acc=accountInsert[i].Id;
                
            }
        }
        contract con = CS_TestUtility.createContract();
        Pricebook2 pb = CS_TestUtility.createPriceBook();
        Order createOrder = new Order();
        createOrder.AccountId = con.AccountId;
        createOrder.ContractId = con.Id;
        createOrder.EffectiveDate = system.today() + 2;
        createOrder.Status = 'Open'; 
        createOrder.CS_Order_Date__c = system.today();        
        createOrder.Pricebook2Id = pb.Id;
        insert createOrder;
        return createOrder;
        
    }
    
    /**
*  Created Date            : 20-Apr-2021
*  Created By              : Spoorthi B R
*  Requirement Reference   : CCM - 365
*  Description Of Method   : Method for the creating Order Item.
**/
    
    public static orderItem createOrderItem() {
        order oId = CS_TestUtility.createOrder();
        PriceBookEntry pbEntry = CS_TestUtility.createPricebookEntry();
        product2 prod = CS_TestUtility.createProduct();
        orderItem oiInsert = new OrderItem();
        oiInsert.CS_Order_Line_Status__c = 'REJECTED';
        oiInsert.CS_SAP_Invoice_Line_Id__c = 'Credit Invoice';
        oiInsert.OrderId = oId.Id;
        oiInsert.PricebookEntryId = pbEntry.Id;
        oiInsert.Product2Id = prod.Id;
        oiInsert.UnitPrice = 2;
        oiInsert.CS_Quantity__c = 10;
        oiInsert.Quantity = 10;
        insert oiInsert;        
        return oiInsert;       
    }
    
    public static Account createAccount1() {
        Account acc = new Account();
        acc.BillingCountry = 'Test Country';
        acc.CS_SAP_Customer_Id__c = '1234567';
        //acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Hospital').getRecordTypeId();
        acc.Name = 'TestAcc';
        insert acc;
        return acc;
    }
    
    public static Order createOrder1() {
        Account acc = createAccount1();
        Order ord = new Order();
        ord.RecordTypeId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Return Order').getRecordTypeId();
        ord.CS_Return_Order_Type__c = 'ZERB';
        ord.CS_SAP_Order_Number__c = '123456';
        ord.AccountId = acc.Id;
        ord.Status = 'Open';
        ord.EffectiveDate = system.Today();
        insert ord;
        return ord;
    } 
    
    public static Case createCaseInsertion() {
        Account acc = createAccount1();
        Order ord = createOrder1();
        case newCase = new Case();
        newCase.CS_Case_Created_From_SAP__c = true;
        newCase.CS_Initial_Order__c = ord.Id;
        newCase.AccountId = acc.Id;
        insert newCase;
        return newCase;
    }
    
    public static Case createCaseRequest() {    
    	Case newCase = new Case(
            AccountId = createAccount1().Id,
            Subject = 'Test Request Case',
            Type = CS_ConstantClass.CASE_REQUEST
        );
        insert newCase;
        return newCase;
    }
    
}