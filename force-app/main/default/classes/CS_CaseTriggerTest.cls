/**
* Created Date : 14-April-21
* Created By : Indu 
* Requirement Reference :
* Description Of Method : Test class On Case Trigger 

***** MODIFICATION HISTORY
Version Number  Date  Last Modified By   Description/User   Story Number
CCM-357
**/
@isTest
public class CS_CaseTriggerTest {
    public static testmethod void testData(){
        CS_TestUtility.createCaseInsertion();
        system.assertEquals(1, CS_CaseTriggerHelper.eventReturnCase.size(), 'Event is published');
    }   
    
    /*@isTest
    public static void testAssignEntitlementAndCloseMilestone() {
        Account currentAcc = CS_TestUtility.createAccount1();
        // Query Entitlement Process and then create a related Entitlement
        List<SlaProcess> epL = [SELECT Id FROM SlaProcess WHERE Name = :'Request EP'];
        Entitlement e = new Entitlement(AccountId = currentAcc.Id, StartDate = System.Today(), SlaProcessId = epL[0].Id, Name = CS_ConstantClass.GLOBAL_ENTITLEMENT_FOR_REQUEST_NAME);
        insert e;
        // Create case after entitlement is inserted so the trigger can find it
        Case currentCase = CS_TestUtility.createCaseRequest();
        // Query case to make sure the beforeInsert method was called and the entitlement was linked to the case
        Case queriedCase = [SELECT Id, EntitlementId FROM Case WHERE Type = :CS_ConstantClass.CASE_REQUEST LIMIT 1][0];
        System.assertEquals(e.Id, queriedCase.EntitlementId);
        // Create case without Type and then update the case to type=request to make sure the beforeUpdate method was called and attach the entitlement when the type is set to request
        Case currentCase2 = CS_TestUtility.createCaseInsertion();
        currentCase2.Type = CS_ConstantClass.CASE_REQUEST;
        update currentCase2;
        // Close the cases to verify that the beforeUpdate method was called and the milestones were completed
        currentCase.Status = 'Closed';
        currentCase2.Status = 'Closed';
        update currentCase;
        update currentCase2;
        List<CaseMilestone> cmList = [SELECT Id, CompletionDate, isCompleted FROM CaseMilestone WHERE CaseId = :currentCase.Id OR CaseId = :currentCase2.Id LIMIT 2];
        System.assertEquals(true, cmList[0].isCompleted);
        System.assertEquals(true, cmList[1].isCompleted);
    }*/
}