/**
*  Created Date                                    : 06-Apr-21
*  Created By                                      : Harshita Gupta
*  Requirement Reference                           : To be updated 
*  Description Of Method                           : Parse the JSON of the Delivery Items

*****  MODIFICATION HISTORY
Version Number		Date		Last Modified By		Description/User Story Number
**/
public class CS_SAPSFDeliveryParser {
    @AuraEnabled public  Boolean isSuccess;
    @AuraEnabled public  String errorMessage;
    
    @AuraEnabled public DeliveryWrapper d;
    
    public class DeliveryWrapper {
        @AuraEnabled public List<DeliveryItems> results;
    }
    
    public class DeliveryItems {
       @AuraEnabled public DateTime formattedCreationDate;
       @AuraEnabled public DateTime formattedActualGoodsMovementDate;
       @AuraEnabled public String creationDate;
       @AuraEnabled public String creationTime;
       @AuraEnabled public String deliveryDocument;
       @AuraEnabled public String deliveryDocumentType;
       @AuraEnabled public String formattedDeliveryDocumentType;
       @AuraEnabled public String shipToParty;
       @AuraEnabled public String actualGoodsMovementDate;
       @AuraEnabled public String overallProofOfDeliveryStatus;
       @AuraEnabled public String formattedOverallProofOfDeliveryStatus;
       @AuraEnabled public String soldToParty;
       public To_DeliveryDocumentPartner to_DeliveryDocumentPartner;
       
    }
    public class To_DeliveryDocumentPartner {
		@AuraEnabled public List<Results> results;
	}
    public class Results {
		@AuraEnabled public String addressID;
		@AuraEnabled public To_Address to_Address;
    }
   
    public class To_Address {
       @AuraEnabled public String addressID;
       @AuraEnabled public String businessPartnerName1;
        
    }
    public static CS_SAPSFDeliveryParser parse(String json) {
        //System.debug('Succes Result ------>'+isSuccess);
        return (CS_SAPSFDeliveryParser) System.JSON.deserialize(json, CS_SAPSFDeliveryParser.class);
    }
}