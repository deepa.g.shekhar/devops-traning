/**
*  Created Date                                   : 07-April-21
*  Created By                                     : Harshita Gupta
*  Requirement Reference                          : To be updated 
*  Description			                          : Test class for Delivery Response Data

*****  MODIFICATION HISTORY
Version Number		Date		Last Modified By		Description/User Story Number

**/
@isTest
public class CS_SAPSFDeliveryResponseTest {
    public static FINAL String attributeName = 'SAP_All_Delivery_Document_Service';
    public static  String acc;
    public static  String acc1;
    public static  String acc2;
    public static  String acc3;
    
    @testSetup
    static void createTestData(){
        Integer i;
        List<CS_SAP_Picklist_Mapping__c> mapInsert = new List<CS_SAP_Picklist_Mapping__c>();
        mapInsert = CS_TestUtility.createSAPPickListMapping(); 
        
        List<Account> accountInsert = new List<Account>();
        accountInsert = CS_TestUtility.createAccount(); 
        for(i=0; i<accountInsert.size(); i++){
            if(accountInsert[i].CS_Partner_Function__c=='Sold To' && accountInsert[i].CS_SAP_Customer_Id__c!=' ' ){
                acc1=accountInsert[i].Id;
                
            }
            if(accountInsert[i].CS_Partner_Function__c=='Ship To'){
                acc2=accountInsert[i].Id;  
                
            }
            if(accountInsert[i].CS_SAP_Customer_Id__c==' '){
                acc3=accountInsert[i].Id;  
                
            }
        }
        
    }




@isTest
public static void getDeliveryItemsResponseTestPositive(){
    CS_Integration_Attributes__mdt integrationTest = new CS_Integration_Attributes__mdt();
    integrationTest = CS_TestUtility.fetchIntegrationAttributeRecord(attributeName);
    Test.startTest();
    String body = '{ "d": { "results": [ { "BillingDocumentDate": "/Date(1492098664000)/", "CreationDate": "/Date(1492098664000)/","ActualGoodsMovementDate": "/Date(1492098654000)/", "DeliveryDocument": "string", "DeliveryDocumentType": "string", "OverallProofOfDeliveryStatus": "string", "OrderID": "string", "PickedItemsLocation": "string", "ShipToParty": "string", "SoldToParty": "string", "WarehouseStagingArea": "string", "to_DeliveryDocumentPartner": { "results": [ { "SDDocumentItem": "string", "Supplier": "string", "to_Address": { "StreetSuffixName": "string", "TaxJurisdiction": "string", "TransportZone": "string" } } ] }, "to_DeliveryDocumentItem": { "results": [ { "AlternateProductNumber": "string", "WarehouseStockCategory": "string", "WarehouseStorageBin": "string", "to_DocumentFlow": { "results": [ { "SubsequentDocumentCategory": "string", "TransferOrderInWrhsMgmtIsConfd": "Unknown Type: boolean,null" } ] }, "to_SerialDeliveryItem": { "MaintenanceItemObjectList": 0, "SDDocumentCategory": "string", "to_MaintenanceItemObject": { "results": [ { "MaintObjectLocAcctAssgmtNmbr": "string", "Material": "string", "SerialNumber": "string" } ] } } } ] } } ] } }';
    CS_MockHttpResponseGenerator mock = new CS_MockHttpResponseGenerator(body,200,'OK');
    Test.setMock(HttpCalloutMock.class, mock);
    CS_SAPSFDeliveryResponse.getDeliveryItems('7001007147');
    Test.stopTest();
}
@isTest
public static void getDeliveryItemsResponseTestWrongBody(){
    CS_Integration_Attributes__mdt integrationTest = new CS_Integration_Attributes__mdt();
    integrationTest = CS_TestUtility.fetchIntegrationAttributeRecord(attributeName);
    Test.startTest();
    String body = '{​​"d":{​​"results":[{​​"__metadata":{​​"id":"","uri":"","type":"API_BILLING_DOCUMENT_SRV.A_BillingDocumentItemType"}​​,"BillingDocument":"7001135055","BillingDocumentItem":"10","SalesDocumentItemCategory":"ZTAN","SalesDocumentItemType":"","CreatedByUser":"ASYS-CTRL-M","CreationDate":"","CreationTime":"PT00H12M16S","OrganizationDivision":"C0","Division":"A2","SalesOffice":"","Material":"387015","Batch":"8S727","BillingDocumentItemText":"CLEXANE 60 MG. 10 JERIN. H SD","ServicesRenderedDate":"","BillingQuantity":"6.000","BillingQuantityUnit":"UNT","BillingQuantityInBaseUnit":"6.000","BaseUnit":"UNT","ItemGrossWeight":"986.000","ItemNetWeight":"986.000","ItemWeightUnit":"G","ItemVolume":"9846.000","ItemVolumeUnit":"CCM","BillToPartyCountry":"ES","BillToPartyRegion":"29","NetAmount":"75.81","TransactionCurrency":"EUR","GrossAmount":"0.00","PricingDate":"","AbsltPriceDetnExchangeRate":"1.00000","PriceDetnExchRateIsIndrctQtan":false,"PriceDetnExchangeRateDate":"","PricingScaleQuantityInBaseUnit":"0.000","TaxAmount":"3.03","CostAmount":"83.88","Subtotal1Amount":"303.24","OrderID":"","ReferenceSDDocument":"5001129600","ReferenceSDDocumentItem":"10","ReferenceSDDocumentCategory":"J","SalesDocument":"1001129514","SalesDocumentItem":"10","SalesSDDocumentCategory":"C","SalesGroup":"","AdditionalCustomerGroup1":"","AdditionalCustomerGroup2":"","AdditionalCustomerGroup3":"2","AdditionalCustomerGroup4":"","AdditionalCustomerGroup5":"","SDDocumentReason":"","SalesOrderSalesOrganization":"ES02","SalesOrderDistributionChannel":"10","ShippingPoint":"ESB1","to_BillingDocument":{​"__deferred":{​​"uri":""}​​}​​​}​​]}​​}​​';        
    CS_MockHttpResponseGenerator mock = new CS_MockHttpResponseGenerator(body,500,'ERROR');
    Test.setMock(HttpCalloutMock.class, mock);        
    
    CS_SAPSFDeliveryResponse.getDeliveryItems(acc1);
    Test.stopTest();
}
@isTest
public static void getDeliveryItemsResponseTestFailure(){
    CS_Integration_Attributes__mdt integrationTest = new CS_Integration_Attributes__mdt();
    integrationTest = CS_TestUtility.fetchIntegrationAttributeRecord(attributeName);
    Test.startTest();
    String body1 = '';
    CS_MockHttpResponseGenerator mock1 = new CS_MockHttpResponseGenerator(body1,500,'Error');
    Test.setMock(HttpCalloutMock.class, mock1);
    CS_SAPSFDeliveryResponse.getDeliveryItems(acc3);
    Test.stopTest();
}
@isTest
public static void getDeliveryItemsErrorResponseTest(){
    CS_Integration_Attributes__mdt integrationTest = new CS_Integration_Attributes__mdt();
    integrationTest = CS_TestUtility.fetchIntegrationAttributeRecord(attributeName);
    Test.startTest();
    String body = '{ "d": { "results": [ { "BillingDocumentDate": "/Date(1492098664000)/", "CreationDate": "/Date(1492098664000)/", "DeliveryDocument": "string", "DeliveryDocumentType": "string", "OrderID": "string", "PickedItemsLocation": "string", "ShipToParty": "string", "SoldToParty": "string", "WarehouseStagingArea": "string", "to_DeliveryDocumentPartner": { "results": [ { "SDDocumentItem": "string", "Supplier": "string", "to_Address": { "StreetSuffixName": "string", "TaxJurisdiction": "string", "TransportZone": "string" } } ] }, "to_DeliveryDocumentItem": { "results": [ { "AlternateProductNumber": "string", "WarehouseStockCategory": "string", "WarehouseStorageBin": "string", "to_DocumentFlow": { "results": [ { "SubsequentDocumentCategory": "string", "TransferOrderInWrhsMgmtIsConfd": "Unknown Type: boolean,null" } ] }, "to_SerialDeliveryItem": { "MaintenanceItemObjectList": 0, "SDDocumentCategory": "string", "to_MaintenanceItemObject": { "results": [ { "MaintObjectLocAcctAssgmtNmbr": "string", "Material": "string", "SerialNumber": "string" } ] } } } ] } } ] } }';
    CS_MockHttpResponseGenerator mock = new CS_MockHttpResponseGenerator(body,500,'ERROR');
    Test.setMock(HttpCalloutMock.class, mock);
    CS_SAPSFDeliveryResponse.getDeliveryItems('7001007147');
    Test.stopTest();
}
@isTest
public static void getDeliveryItemsEmptyBodyResponseTest(){
    CS_Integration_Attributes__mdt integrationTest = new CS_Integration_Attributes__mdt();
    integrationTest = CS_TestUtility.fetchIntegrationAttributeRecord(attributeName);
    Test.startTest();
    String body = '';
    CS_MockHttpResponseGenerator mock = new CS_MockHttpResponseGenerator(body,200,'OK');
    Test.setMock(HttpCalloutMock.class, mock);
    CS_SAPSFDeliveryResponse.getDeliveryItems('7001007147');
    Test.stopTest();
}
@isTest
public static void getDeliveryItemsNullSAPID(){
    CS_SAPSFDeliveryResponse.getDeliveryItems(null);
    
}
}