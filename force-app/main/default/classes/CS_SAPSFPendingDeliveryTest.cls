/**
*  Created Date                                    : 7-Apr-21
*  Created By                                      : Bajrang
*  Requirement Reference                           : Test Class for CS_SAPSFPendingDeliveryData 
*  Description Of Method                           : Integration of Pending delivery items

*****  MODIFICATION HISTORY
Version Number		Date		Last Modified By		Description/User Story Number
**/

@isTest
public class CS_SAPSFPendingDeliveryTest {
    
    public static FINAL String attributeName = 'SAP_All_Order_Service';
    
    @testSetup
    static void createTestData(){
        List<Account> accountInsert = new List<Account>();
        accountInsert = CS_TestUtility.createAccount(); 
        
    }    
    /**
*  Created Date            : 7-Apr-2021
*  Created By              : Bajrang
*  Requirement Reference   : To be updated 
*  Description Of Method   : Test method for the getDelivery method
**/
    @isTest
    public static void wrongBodyResponseTest(){
        CS_Integration_Attributes__mdt integrationTest = new CS_Integration_Attributes__mdt();
        integrationTest = CS_TestUtility.fetchIntegrationAttributeRecord(attributeName);
        Test.startTest();
        String body = '{​​"d":{​​"results":[{​​"__metadata":{​​"id":"","uri":"","type":"API_BILLING_DOCUMENT_SRV.A_BillingDocumentItemType"}​​,"BillingDocument":"7001135055","BillingDocumentItem":"10","SalesDocumentItemCategory":"ZTAN","SalesDocumentItemType":"","CreatedByUser":"ASYS-CTRL-M","CreationDate":"","CreationTime":"PT00H12M16S","OrganizationDivision":"C0","Division":"A2","SalesOffice":"","Material":"387015","Batch":"8S727","BillingDocumentItemText":"CLEXANE 60 MG. 10 JERIN. H SD","ServicesRenderedDate":"","BillingQuantity":"6.000","BillingQuantityUnit":"UNT","BillingQuantityInBaseUnit":"6.000","BaseUnit":"UNT","ItemGrossWeight":"986.000","ItemNetWeight":"986.000","ItemWeightUnit":"G","ItemVolume":"9846.000","ItemVolumeUnit":"CCM","BillToPartyCountry":"ES","BillToPartyRegion":"29","NetAmount":"75.81","TransactionCurrency":"EUR","GrossAmount":"0.00","PricingDate":"","AbsltPriceDetnExchangeRate":"1.00000","PriceDetnExchRateIsIndrctQtan":false,"PriceDetnExchangeRateDate":"","PricingScaleQuantityInBaseUnit":"0.000","TaxAmount":"3.03","CostAmount":"83.88","Subtotal1Amount":"303.24","OrderID":"","ReferenceSDDocument":"5001129600","ReferenceSDDocumentItem":"10","ReferenceSDDocumentCategory":"J","SalesDocument":"1001129514","SalesDocumentItem":"10","SalesSDDocumentCategory":"C","SalesGroup":"","AdditionalCustomerGroup1":"","AdditionalCustomerGroup2":"","AdditionalCustomerGroup3":"2","AdditionalCustomerGroup4":"","AdditionalCustomerGroup5":"","SDDocumentReason":"","SalesOrderSalesOrganization":"ES02","SalesOrderDistributionChannel":"10","ShippingPoint":"ESB1","to_BillingDocument":{​"__deferred":{​​"uri":""}​​}​​​}​​]}​​}​​';        
        CS_MockHttpResponseGenerator mock = new CS_MockHttpResponseGenerator(body,200,'OK');
        Test.setMock(HttpCalloutMock.class, mock);
        
        
        CS_SAPSFPendingDeliveryResponse.getDeliveryItems('7001007147');
        Test.stopTest();
    }
    @isTest
    public static void rightBodyResponseTest(){
        CS_Integration_Attributes__mdt integrationTest = new CS_Integration_Attributes__mdt();
        integrationTest = CS_TestUtility.fetchIntegrationAttributeRecord(attributeName);
        Test.startTest();
        String body = '{ "d": { "results": [ { "BillingDocument": "string", "BillingDocumentItem": "string", "CreationDate": "/Date(1492041600000)/", "AdditionalMaterialGroup1": "string", "AdditionalMaterialGroup2": "string", "AdditionalMaterialGroup3": "string", "AdditionalMaterialGroup4": "string", "BillingDocumentItemText": "string", "ServicesRenderedDate": "/Date(1492041600000)/", "BillingQuantity": "0", "BillingQuantityUnit": "string", "BillingQuantityInBaseUnit": "0", "BaseUnit": "string", "ItemGrossWeight": "0", "ItemNetWeight": "0", "ItemWeightUnit": "string", "ItemVolume": "0", "ItemVolumeUnit": "string", "BillToPartyCountry": "string", "BillToPartyRegion": "string", "BillingPlanRule": "string", "NetAmount": "0", "TransactionCurrency": "string", "to_BillingDocument": { "BillingDocument": "string", "PriceListType": "string", "TaxDepartureCountry": "string", "InvoiceClearingStatus": "string", "BillingDocumentListType": "string", "BillingDocumentListDate": "/Date(1492041600000)/", "to_Item": { "results": [ null ] }, "to_Partner": { "results": [ { "BillingDocument": "string", "PartnerFunction": "string", "Customer": "string", "Supplier": "string", "Personnel": "string", "ContactPerson": "string" } ] }, "to_PricingElement": { "results": [ { "ConditionIsManuallyChanged": true, "ConditionIsForConfiguration": true, "VariantCondition": "string" } ] }, "to_Text": { "results": [ { "LongTextID": "string", "LongText": "string" } ] } }, "to_ItemText": { "results": [ { "BillingDocument": "string", "BillingDocumentItem": "string", "Language": "string", "LongTextID": "string", "LongText": "string" } ] }, "to_Partner": { "results": [ { "ContactPerson": "string", "to_BillingDocument": { "BillingDocumentListType": "string", "BillingDocumentListDate": "/Date(1492041600000)/", "to_Item": { "results": [ null ] }, "to_Partner": { "results": [ { "BillingDocument": "string", "PartnerFunction": "string", "Customer": "string", "Supplier": "string", "Personnel": "string", "ContactPerson": "string" } ] }, "to_PricingElement": { "results": [ { "ConditionIsManuallyChanged": true, "ConditionIsForConfiguration": true, "VariantCondition": "string" } ] }, "to_Text": { "results": [ { "BillingDocument": "string", "Language": "string", "LongTextID": "string", "LongText": "string" } ] } } } ] }, "to_PricingElement": { "results": [ { "ConditionScaleBasisCurrency": "string", "CndnIsRelevantForIntcoBilling": true, "ConditionIsManuallyChanged": true, "ConditionIsForConfiguration": true, "VariantCondition": "string", "to_BillingDocument": { "BillingDocumentListType": "string", "BillingDocumentListDate": "/Date(1492041600000)/", "to_Item": { "results": [ null ] }, "to_Partner": { "results": [ { "Personnel": "string", "ContactPerson": "string" } ] }, "to_PricingElement": { "results": [ { "CndnIsRelevantForIntcoBilling": true, "ConditionIsManuallyChanged": true, "ConditionIsForConfiguration": true, "VariantCondition": "string" } ] }, "to_Text": { "results": [ { "LongText": "string" } ] } } } ] } } ] } }';
        CS_MockHttpResponseGenerator mock = new CS_MockHttpResponseGenerator(body,200,'OK');
        Test.setMock(HttpCalloutMock.class, mock);
        
        
        CS_SAPSFPendingDeliveryResponse.getDeliveryItems('7001007147');
        Test.stopTest();
    }
    
    @isTest
    public static void failureResponseTest(){
        CS_Integration_Attributes__mdt integrationTest = new CS_Integration_Attributes__mdt();
        integrationTest = CS_TestUtility.fetchIntegrationAttributeRecord(attributeName);
        Test.startTest();
        String body1 = '';
        CS_MockHttpResponseGenerator mock1 = new CS_MockHttpResponseGenerator(body1,500,'Error');
        Test.setMock(HttpCalloutMock.class, mock1);
        CS_SAPSFPendingDeliveryResponse.getDeliveryItems('7001007147');
        Test.stopTest();
    }
    
    
}