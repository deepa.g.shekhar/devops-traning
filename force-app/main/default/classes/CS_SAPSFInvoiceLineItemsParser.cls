/**
*  Created Date                                    : 24-Mar-21
*  Created By                                      : Harshita Gupta
*  Requirement Reference                           : To be updated 
*  Description Of Method                           : Parse the JSON of the Invoice LineItems


*****  MODIFICATION HISTORY
Version Number		Date		Last Modified By		Description/User Story Number
**/
public class CS_SAPSFInvoiceLineItemsParser {
    public InvoiceLineItemsWrapper d;
    @AuraEnabled public Boolean isSuccess;
    @AuraEnabled public String errorMessage;
    
    public class InvoiceLineItemsWrapper {
        public List<LineItems> results;
    }
    
    public class LineItems {
        public String billingDocument;
        public String orderID;
        public String creationDate;
        public DateTime formattedCreationDate;
        public String transactionCurrency;
        public String billingDocumentItem;
        public String billingQuantity;
        public String netAmount;
        public String costCenter;
        public String plant;
        public String plantCity;
        public String plantRegion;
        public String referenceSDDocument;
        public String referenceSDDocumentCategory;
        public String billingDocumentItemInPartSgmt;
        public To_BillingDocument to_BillingDocument;
    }
    public class To_BillingDocument {
        public String purchaseOrderByCustomer;
        public String billingDocumentDate;
    }
    
    
    
    /**
	*  Created Date            : 24-Mar-2021
	*  Created By              : Harshita Gupta
	*  Requirement Reference   : To be updated 
	*  Description Of Method   : Deserialize JSON received from the response
	**/
    public static CS_SAPSFInvoiceLineItemsParser parseInvoiceLineItems(String json) {
        return (CS_SAPSFInvoiceLineItemsParser) System.JSON.deserialize(json, CS_SAPSFInvoiceLineItemsParser.class);
    }
    
}