/**
*  Created Date                                    : 24-Mar-21
*  Created By                                      : Harshita Gupta
*  Requirement Reference                           : To be updated 
*  Description Of Method                           : Integration of Invoice Line Items

*****  MODIFICATION HISTORY
Version Number		Date		Last Modified By		Description/User Story Number
**/
public class CS_SAPSFInvoiceLineItemsDetails {
    public static String invoiceLineItemDetails;
    public static CS_InvoiceLineItemsParserWrapper itemsResponse;
    public static FINAL String attributeName = 'SAP_Billing_Document_Item_Service';
    
    /**
*  Created Date            : 24-Mar-2021
*  Created By              : Harshita Gupta
*  Requirement Reference   : To be updated 
*  Description Of Method   : Get Invoice Line Items Details
**/
    @AuraEnabled
    public static string getLineItems(String Invoice){
        try{
            if(String.isNotBlank(Invoice)){
                String actualDate = '/Date(1530403200000)/';
                Long longtime = long.valueOf( actualDate.substringBetween('(', ')') );
                DateTime FormattedDateTime = DateTime.newInstance(longtime);  
                
                HTTPCalloutFramework.HttpCalloutWrapper callout = new HTTPCalloutFramework.HttpCalloutWrapper();
                CS_Integration_Attributes__mdt attributeMetadata = new CS_Integration_Attributes__mdt();
                Map<String,String> mapHeader = new Map<String,String>();
                attributeMetadata = [SELECT ID, CS_Endpoint_URL__c, CS_Method_Type__c, CS_Timeout__c, CS_Name__c , CS_System_Name__c
                                     FROM CS_Integration_Attributes__mdt
                                     WHERE DeveloperName =: attributeName
                                     LIMIT 1];
                if(attributeMetadata != null){
                    mapHeader.put('accept','application/json');
                    mapHeader.put('Content-Type','application/json');
                    String URL = '?$format=json';
                    callout.mapOfURLParameters = new Map<String, String>();
                    callout.endPointParamString = URL;
                    callout.integrationAttributeName = attributeName;
                    callout.endPointParamString = attributeMetadata.CS_Endpoint_URL__c.replace('{BillingDocument}',Invoice) + URL;
                    callout.mapOfHttpHeaders=mapHeader;
                    HTTPResponse response = HTTPCalloutFramework.sendHTTPGETRequest(callout);
                    invoiceLineItemDetails = response.getBody();
                    if(response.getStatusCode()==200){
                        System.debug('---->>>'+response.getBody());
                        if(String.isNotBlank(response.getBody())){
                            
                            itemsResponse = CS_InvoiceLineItemsParserWrapper.parseInvoiceLineItems(invoiceLineItemDetails);
                            System.debug(itemsResponse.d.results);
                            for(CS_InvoiceLineItemsParserWrapper.LineItems responseData : itemsResponse.d.results){
                                Long longtimedate = long.valueOf( responseData.CreationDate.substringBetween('(', ')') );
                                responseData.FormattedCreationDate = DateTime.newInstance(longtimedate);
                            }
                            itemsResponse.isSuccess = true;   
                        }
                        else{
                            itemsResponse.isSuccess = false;   
                            itemsResponse.errorMessage = 'Getting empty response body';
                        }
                        
                    }
                }
            }
        }
        catch(Exception ex){
            ExceptionLogHandler.createErrorRecord(ex, 'CS_SAPSFInvoiceLineItemsDetails');
        } 
        
        return json.serialize(itemsResponse);
    }
    
}