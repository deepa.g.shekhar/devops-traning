public class CS_CustomException extends Exception {
    public static FINAL String CS_INVALID_CASE_ID = 'Case id is invalid';
    public static FINAL String CS_CASE_ID_ISNULL = 'Case Id should not be blank';
    public static FINAL String CS_CASE_ID_ISCLOSED = 'Case Id should not be already Closed';
    public static FINAL String CS_CASE_ID_BLANK = 'Case does not exist';
    public static FINAL String CS_CASE_ID_RETURNORDERTYPE = 'Return order is not matching';
    public static FINAL String CS_ORDERRESPONSE = 'Order Response is null';
    public static FINAL String CS_CASERESPONSE = 'Case Response is null';
    public static FINAL String CS_ORDERPRODUCTRESPONSE = 'Order Product Response is null';
    public static FINAL String CS_SAPNULLRESPONSE = 'No response from SAP';
    public static FINAL String CS_SAPFETCHERROR = 'An error occured while fetching data from SAP. Please contact your system administrator if the error persists';
    public static FINAL String CS_ISACCNULL = 'Account ID not found';      
}