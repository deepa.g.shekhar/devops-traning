/**
*  Created Date                                   : 14-April-21
*  Created By                                     : Bajrang
*  Requirement Reference                          :  
*  Description Of Method                          : To Handle the case notification trigger

*****  MODIFICATION HISTORY
Version Number		Date		Last Modified By		Description/User Story Number

**/
public class CS_CaseNotificationTriggerHandler implements CS_TriggerInterface{
    
    /**
*  Created Date                                   : 14-April-21
*  Created By                                     : Bajrang
*  Requirement Reference                          :  
*  Description Of Method                          : To Handle the case notification trigger

*****  MODIFICATION HISTORY
Version Number		Date		Last Modified By		Description/User Story Number

**/
    public void beforeInsert(List<SObject> newRecordList){
        
    }
    public void afterInsert(List<SObject> newRecordList,Map<Id, SObject> newRecordsMap){
        
        system.debug('inside Handler');
        List<CS_Case_Notification_PE__e> caseNotification = New List<CS_Case_Notification_PE__e>();
        caseNotification =  (List<CS_Case_Notification_PE__e>)newRecordList;
        SYSTEM.debug('NEW RECORD LIST-->'+newRecordList);
        if(caseNotification != NUll) {
            //added for IICS test
            Case testCase = new Case();
            testCase.Subject = 'PE TEST For CaseNotification';
            testCase.Description = caseNotification[0].CS_Case_Details__c;
            insert testCase;
           	CS_CaseNotificationTriggerHelper.getEventData(caseNotification);
            //CS_CaseNotificationTriggerHelper.getCaseData(caseNotification);
        }
        
    }
    public void beforeUpdate(Map<Id, SObject> newRecordsMap, Map<Id, SObject> oldRecordsMap){}
    public void afterUpdate(Map<Id, SObject> newRecordsMap, Map<Id, SObject> oldRecordsMap){
        
    }
    public void beforeDelete(List<SObject> oldRecordList,Map<Id, SObject> oldRecordsMap){}
    public void afterDelete(Map<Id, SObject> oldRecordsMap){}
    public void afterUndelete(List<SObject> newRecordList,Map<Id, SObject> newRecordsMap){}    

}