/**
*  Created Date                                   : 02-Mar-21
*  Created By                                     : Prachi Dhaundiyal
*  Requirement Reference                          : To be updated 
*  Description:			                          : Generic Framework for sending HTTP Requests from Salesforce

*****  MODIFICATION HISTORY
Version Number		Date		Last Modified By		Description/User Story Number

**/
public class HTTPCalloutFramework {
    
    /* VARIABLE DECLARATION STARTS */
	static FINAL String namedCredential = 'callout:';
    static HttpCalloutWrapper instHttpCalloutWrapper;
    static HTTPRequest requestDetails;
    static HTTPResponse responseDetails;
    static CS_Integration_Attributes__mdt attributeMetadata;
	/* VARIABLE DECLARATION ENDS */
    
    /**
    *  Created Date            : 02-Mar-2021
    *  Created By              : Prachi Dhaundiyal
    *  Requirement Reference   : To be updated 
    *  Description Of Class    : Inner class for wrapping request details
    **/
    public class HttpCalloutWrapper{
        public String integrationAttributeName;   
        public String endPointParamString;
        public String requestBody;
        public Blob blobBody;         
        public Map<String, String> mapOfHttpHeaders;
        public Map<String, String> mapOfURLParameters;          
        public List<URLParameterWrapper> urlParameters;
    } 

    /**
    *  Created Date            : 02-Mar-2021
    *  Created By              : Prachi Dhaundiyal
    *  Requirement Reference   : To be updated 
    *  Description Of Class    : Inner class for URL parameter details
    **/
    public class URLParameterWrapper{
    	public String key;
        public String value;
        public String operator;
    }
    
    /**
    *  Created Date            : 02-Mar-2021
    *  Created By              : Prachi Dhaundiyal
    *  Requirement Reference   : To be updated 
    *  Description Of Method   : Initialize HTTPCalloutWrapper instance
    **/
	static void initializeHttpCalloutWrapper(HttpCalloutWrapper objHTTPCalloutWrapper){
        instHttpCalloutWrapper = objHTTPCalloutWrapper;	        
	}
    
    /**
    *  Created Date            : 02-Mar-2021
    *  Created By              : Prachi Dhaundiyal
    *  Requirement Reference   : To be updated 
    *  Description Of Method   : Generate HTTP header for the request
    **/
    static void generateHTTPHeader(){    
        //Check if header map is sent in the wrapper, else throw exception
        if(instHttpCalloutWrapper.mapOfHttpHeaders != null && !instHttpCalloutWrapper.mapOfHttpHeaders.isEmpty()){
            for(String keys: instHttpCalloutWrapper.mapOfHttpHeaders.keySet()){
                //Check if map contains correct key, value pairs, else throw exception
                if(String.isNotBlank(instHttpCalloutWrapper.mapOfHttpHeaders.get(keys))){
                    requestDetails.setHeader(keys, instHttpCalloutWrapper.mapOfHttpHeaders.get(keys));      
                }                    
                else{
                    throw new HTTPCalloutException(HTTPCalloutException.HEADER_NOT_FOUND);
                }                    
            }  
        }
        else{
            throw new HTTPCalloutException(HTTPCalloutException.HEADER_NOT_FOUND);
        }                                           
    }
    
    /**
    *  Created Date            : 02-Mar-2021
    *  Created By              : Prachi Dhaundiyal
    *  Requirement Reference   : To be updated 
    *  Description Of Method   : Generate URL for the request
    **/
    static void generateURLParameters(){
		String tempURLParamString = '';
        //Check if URL map is sent in the wrapper, else throw exception
       	if(instHttpCalloutWrapper.mapOfURLParameters != null && !instHttpCalloutWrapper.mapOfURLParameters.isEmpty()){
            for(String keys: instHttpCalloutWrapper.mapOfURLParameters.keySet()){
                //Check if map contains correct key, value pairs, else throw exception
                if(String.isNotBlank(instHttpCalloutWrapper.mapOfURLParameters.get(keys))){
                    tempURLParamString += keys + '=' + instHttpCalloutWrapper.mapOfURLParameters.get(keys) + '&';   
                }                    
                else{
                    throw new HTTPCalloutException(HTTPCalloutException.ENDPOINT_URL_NOT_FOUND);
                }
            }                        
            if(String.isNotBlank(tempURLParamString)){
                //remove the last occurence of & from the query string
                tempURLParamString = tempURLParamString.removeEnd('&');
                //replace all whitespaces with + 
                tempURLParamString = tempURLParamString.replaceAll(' ', '+');
                instHttpCalloutWrapper.endPointParamString = tempURLParamString;                                
            }
        }                             
    }
	
    /**
    *  Created Date            : 02-Mar-2021
    *  Created By              : Prachi Dhaundiyal
    *  Requirement Reference   : To be updated 
    *  Description Of Method   : Set the endpoint URL
    **/
	static void setEndpointURL(){
        String url = '';
        //Check if relevant interface details are present in the integration attribute metadata, else throw exception
		if(String.isNotBlank(attributeMetadata.CS_System_Name__c) && String.isNotBlank(attributeMetadata.CS_Endpoint_URL__c)){
            if(attributeMetadata.CS_Endpoint_URL__c.contains('/to_Item')){
                url = namedCredential + attributeMetadata.CS_System_Name__c;                
            }
            else{
               
            	url = namedCredential + attributeMetadata.CS_System_Name__c + attributeMetadata.CS_Endpoint_URL__c;  
                
            }			
			if(String.isNotBlank(instHttpCalloutWrapper.endPointParamString))
				url += instHttpCalloutWrapper.endPointParamString;
            System.debug('---url' +url);
			requestDetails.setEndpoint(url);	
		}	
        else{
            throw new HTTPCalloutException(HTTPCalloutException.ENDPOINT_URL_NOT_FOUND);
        } 
	}
	
    /**
    *  Created Date            : 02-Mar-2021
    *  Created By              : Prachi Dhaundiyal
    *  Requirement Reference   : To be updated 
    *  Description Of Method   : Set method type (GET, POST, DELETE etc)
    **/
	static void setMethodType(){
        //Check if method type is set in the integration attribute metadata, else throw exception
		if(String.isNotBlank(attributeMetadata.CS_Method_Type__c))
			requestDetails.setMethod(attributeMetadata.CS_Method_Type__c);		
        else{
            throw new HTTPCalloutException(HTTPCalloutException.METHOD_TYPE_NOT_FOUND);
        } 
	}	
	
    /**
    *  Created Date            : 02-Mar-2021
    *  Created By              : Prachi Dhaundiyal
    *  Requirement Reference   : To be updated 
    *  Description Of Method   : Set timeout for the request
    **/
	static void setTimeout(){
        //set request timeout if it is set in integration attribute metadata
		if(attributeMetadata.CS_Timeout__c != null)
			requestDetails.setTimeout(Integer.valueOf(attributeMetadata.CS_Timeout__c));		
	}
	
    /**
    *  Created Date            : 02-Mar-2021
    *  Created By              : Prachi Dhaundiyal
    *  Requirement Reference   : To be updated 
    *  Description Of Method   : Set body for the request
    **/
	static void setRequestBody(){
        //Check if request body (either string or blob) is sent in the wrapper, else throw exception
		if(String.isNotBlank(instHttpCalloutWrapper.requestBody))
			requestDetails.setBody(instHttpCalloutWrapper.requestBody);
		else if(instHttpCalloutWrapper.blobBody != null)
			requestDetails.setBodyAsBlob(instHttpCalloutWrapper.blobBody);	
        else{
            throw new HTTPCalloutException(HTTPCalloutException.BODY_NOT_FOUND);
        }
	}	
        
    /**
    *  Created Date            : 02-Mar-2021
    *  Created By              : Prachi Dhaundiyal
    *  Requirement Reference   : To be updated 
    *  Description Of Method   : Fetch integration attributes for the interface, depending upon interface name
    **/
	static void getAttributeMetadata(){
		attributeMetadata = new CS_Integration_Attributes__mdt();
        try{
            //Check if integration attribute name is sent in the wrapper, else throw exception
            if(String.isNotBlank(instHttpCalloutWrapper.integrationAttributeName)){
                attributeMetadata = [SELECT ID, CS_Endpoint_URL__c, CS_Method_Type__c, CS_Timeout__c, CS_Name__c , CS_System_Name__c
                                     FROM CS_Integration_Attributes__mdt
                                     WHERE DeveloperName =: instHttpCalloutWrapper.integrationAttributeName
                                     LIMIT 1];
            }    
            else{
                throw new HTTPCalloutException(HTTPCalloutException.INTEGRATION_ATTRIBUTE_NOT_FOUND);
            }            
        }
        catch(Exception ex){
            throw new HTTPCalloutException(HTTPCalloutException.INTEGRATION_ATTRIBUTE_NOT_FOUND);
        }            
	}
	
    /**
    *  Created Date            : 02-Mar-2021
    *  Created By              : Prachi Dhaundiyal
    *  Requirement Reference   : To be updated 
    *  Description Of Method   : Generate HTTP request and call required methods
    **/
	static void generateHTTPRequest(){
        //Call required methods to setup http request
		if(instHttpCalloutWrapper != null){
			getAttributeMetadata();	
			generateURLParameters();			
			generateHTTPHeader();
			setEndpointURL();
			setMethodType();
			setTimeout();				
		}	
	}
	
    /**
    *  Created Date            : 02-Mar-2021
    *  Created By              : Prachi Dhaundiyal
    *  Requirement Reference   : To be updated 
    *  Description Of Method   : Send HTTP GET Request
    **/
    public static HTTPResponse sendHTTPGETRequest(HttpCalloutWrapper objHTTPCalloutWrapper){
        try{
            Http http = new Http();
            requestDetails = new HTTPRequest();
            responseDetails = new HTTPResponse();
            if(objHTTPCalloutWrapper != null){
                initializeHttpCalloutWrapper(objHTTPCalloutWrapper);
                
                generateHTTPRequest();	               
                responseDetails = http.send(requestDetails);
                system.debug('sendHTTPGETRequest -- Endpoint Details --> ' + requestDetails.getEndpoint());
                system.debug('sendHTTPGETRequest -- Response Body --> ' + responseDetails.getBody());
                system.debug('sendHTTPGETRequest -- Status --> ' + responseDetails.getStatus());
                system.debug('sendHTTPGETRequest -- Status Code --> ' + responseDetails.getStatusCode());                
            }
            else
                throw new HTTPCalloutException(HTTPCalloutException.WRAPPER_NOT_FOUND);	
        }
        catch(Exception ex){
            ExceptionLogHandler.createErrorRecord(ex, ex.getMessage());
        }        
        return responseDetails;
    }

	/**
    *  Created Date            : 02-Mar-2021
    *  Created By              : Prachi Dhaundiyal
    *  Requirement Reference   : To be updated 
    *  Description Of Method   : Send HTTP POST Request
    **/    
    public static HTTPResponse sendHTTPPOSTRequest(HttpCalloutWrapper objHTTPCalloutWrapper){
        try{
            Http http = new Http();
            requestDetails = new HTTPRequest();
            responseDetails = new HTTPResponse();
            if(objHTTPCalloutWrapper != null){
                initializeHttpCalloutWrapper(objHTTPCalloutWrapper);
                generateHTTPRequest();	
                setRequestBody();
                responseDetails = http.send(requestDetails);
                system.debug('sendHTTPGETRequest -- Endpoint Details --> ' + requestDetails.getEndpoint());
                system.debug('sendHTTPGETRequest -- Response Body --> ' + responseDetails.getBody());
                system.debug('sendHTTPGETRequest -- Status --> ' + responseDetails.getStatus());
                system.debug('sendHTTPGETRequest -- Status Code --> ' + responseDetails.getStatusCode());                
            }
            else
                throw new HTTPCalloutException(HTTPCalloutException.WRAPPER_NOT_FOUND);	
        }
        catch(Exception ex){
            ExceptionLogHandler.createErrorRecord(ex, ex.getMessage());
        }        
        return responseDetails;
    }
    
}