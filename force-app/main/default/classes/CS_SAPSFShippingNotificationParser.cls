/**
*  Created Date                                   : 20-Apr-21
*  Created By                                     : Spoorthi B R
*  Requirement Reference                          : CCM - 365
*  Description			                          : Parse the JSON of the Shipping Notification.
*****  MODIFICATION HISTORY
Version Number		Date		Last Modified By		Description/User Story Number

**/
public class CS_SAPSFShippingNotificationParser {
    @AuraEnabled public OrderDetails OrderDetails;
    @AuraEnabled public CaseDetails CaseDetails;
    @AuraEnabled public OrderLineItems orderLineItems;
    
    public class OrderDetails {
        @AuraEnabled public String ReferenceInvoice;
    }
    public class CaseDetails {
        @AuraEnabled public String CaseID;
        @AuraEnabled public String CaseStatus;
    }
    public class OrderLineItems{
        public list<OrderLineItemDetails> OrderLineItemDetails;
    }
    public class OrderLineItemDetails {
         @AuraEnabled public String OrderLineStatus;
         @AuraEnabled public String ProductId;
         @AuraEnabled public String ReferenceInvoiceLineId;
         @AuraEnabled public String SourceSystemOrderLIneId;
         @AuraEnabled public String ReferenceDelivery;
    }    
/**
*  Created Date            : 20-Apr-2021
*  Created By              : Spoorthi B R
*  Requirement Reference   : CCM - 365
*  Description Of Method   : Method to parse the json.
**/
    public static CS_SAPSFShippingNotificationParser parse(String jSon) {
        return (CS_SAPSFShippingNotificationParser) System.JSON.deserialize(jSon, CS_SAPSFShippingNotificationParser.class);
    }
    
}