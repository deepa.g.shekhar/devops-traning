/**
*  Created Date                                   : 08-April-21
*  Created By                                     : Harshita Gupta
*  Requirement Reference                          : To be updated 
*  Description			                          : Test class for Delievery LWC Controller

*****  MODIFICATION HISTORY
Version Number		Date		Last Modified By		Description/User Story Number

**/

@isTest
public class CS_Delivery_LWCControllerTest {
    public static  String acc;
    public static  String acc1;
    public static  String acc2;
    public static  String acc3;
    public static  String acc4;
    public static  String acc5;
    
    @isTest
    static void getAccDetailsTestPositive(){
        Integer i;
        
        List<Account> accountInsert = new List<Account>();
        accountInsert = CS_TestUtility.createAccount(); 
        for(i=0; i<accountInsert.size(); i++){
            if(accountInsert[i].CS_Partner_Function__c=='Sold To' && accountInsert[i].CS_SAP_Customer_Id__c!=' ' ){
                acc1=accountInsert[i].Id;
                
            }
            if(accountInsert[i].CS_Partner_Function__c=='Ship To' && accountInsert[i].ParentId != null){
                acc2=accountInsert[i].Id;  
                
            }
            if(accountInsert[i].CS_SAP_Customer_Id__c==' '){
                acc3=accountInsert[i].Id;  
                
            }
            if(accountInsert[i].CS_Partner_Function__c=='Ship To'){
                acc4=accountInsert[i].Id;  
                
            }
            
            if(accountInsert[i].Name == 'Account4'){
                acc5 = accountInsert[i].Id;  
                
            }
        }
        System.debug(acc4);
        CS_Delivery_LWCController.getAccDetails(acc1);
        CS_Delivery_LWCController.getAccDetails(acc2);
        CS_Delivery_LWCController.getAccDetails(acc3);
        CS_Delivery_LWCController.getAccDetails(acc4);
        CS_Delivery_LWCController.getAccDetails('');
        CS_Delivery_LWCController.getAccDetails('dummyacc');
    }
}