public class HTTPCalloutException extends Exception {
    public static FINAL String HEADER_NOT_FOUND = 'HTTP Header is missing values or is empty';
    public static FINAL String METHOD_TYPE_NOT_FOUND = 'HTTP Method Type not defined';
    public static FINAL String ENDPOINT_URL_NOT_FOUND = 'Endpoint URL is missing values or is empty';
    public static FINAL String WRAPPER_NOT_FOUND = 'HTTPCalloutWrapper is empty or null';
    public static FINAL String BODY_NOT_FOUND = 'Request Body is empty or null';
    public static FINAL String INTEGRATION_ATTRIBUTE_NOT_FOUND = 'Unable to find the integration details in CS_Integration_Attribute__mdt metadata.';
}