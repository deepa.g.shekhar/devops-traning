/**
*  Created Date                                   : 20-Apr-21
*  Created By                                     : Spoorthi B R
*  Requirement Reference                          : CCM - 365
*  Description			                          : To unit test if the case Status and Order Product is updated based 
													on the response from iPaaS.

*****  MODIFICATION HISTORY
Version Number		Date		Last Modified By		Description/User Story Number

**/
@IsTest
public class CS_ShippingNotificationTriggerHelperTest {
/** 
*  Created Date            : 20-Apr-2021
*  Created By              : Spoorthi B R
*  Requirement Reference   : CCM - 365
*  Description Of Method   : Publish the Shipping Notification events and test if Case and related Order products are updated
**/
    public static  String caseId;    
    @testSetup     
    Static void createTestData() {
        Integer i;
        List<Case> caseInsert = new List<Case>();        
        caseInsert = CS_TestUtility.createCase1();
        for(i=0; i<caseInsert.size(); i++) {
            if(caseInsert[i].CS_Invoice__r.CS_SAP_Invoice_Number__c == '7001061002'){
                caseId = caseInsert[i].Id;                
            }
        }
        OrderItem oiUpdate = CS_TestUtility.createOrderItem();
        oiUpdate.CS_Case__c = caseId;
        Update oiUpdate;
        
    }
    
    @isTest
    public static void getEventDataPositive() {  
        System.Test.startTest();
        Case c1 = [SELECT id, status FROM Case LIMIT 1];
        string cId = String.ValueOf(c1.Id);  
        OrderItem oi = [SELECT id FROM orderItem LIMIT 1];       
        string oId = String.valueOf(oi.Id);
        String s = '{"ReturnOrder": {"CaseID": "'+cId+'","CaseStatus": "Closed","ReferenceInvoice": "123454321","ReferenceDelivery": "12345","SystemName": "SHIFT","ReturnOrderLine":[{"ReferenceInvoiceLineId": "Credit Invoice","OrderLineStatus": "REJECTED","SourceSystemOrderLineID": "'+oId+'"},{"ReferenceInvoiceLineId": "Check","OrderLineStatus": "PARTIALLY SHIPPED","SourceSystemOrderLineID": "'+oId+'"}]}}';
        CS_Shipping_Notification_PE__e test = new CS_Shipping_Notification_PE__e();
        test.CS_Case_Details__c = s;
        Database.SaveResult results = EventBus.publish(test);
        System.Test.stopTest();
        system.assertEquals(true, results.isSuccess());
    }    
    
    @isTest
    public static void getEventDataNegative() {  
        System.Test.startTest();
        Case c1 = [SELECT id, status FROM Case LIMIT 1];
        string cId = String.ValueOf(c1.Id);  
        String s = '{"ReturnOrder": {"CaseID": "'+'cId'+'","CaseStatus": "Closed","ReferenceInvoice": "123454321","ReferenceDelivery": "12345","SystemName": "SHIFT","ReturnOrderLine":[{"ReferenceInvoiceLineId": "Credit Invoice","OrderLineStatus": "REJECTED"},{"ReferenceInvoiceLineId": "Check","OrderLineStatus": "PARTIALLY SHIPPED"}]}}';
        CS_Shipping_Notification_PE__e test = new CS_Shipping_Notification_PE__e();
        test.CS_Case_Details__c = s;
        Database.SaveResult results = EventBus.publish(test);
        System.Test.stopTest();
        system.assertEquals(true, results.isSuccess());
    }  
    
    @isTest
    public static void getEventDataNegative1() {  
        System.Test.startTest();
        Case c1 = [SELECT id, status FROM Case LIMIT 1];
        string cId = String.ValueOf(c1.Id);  
        String s = '{"ReturnOrder": {"CaseID": "'+''+'","CaseStatus": "Closed","ReferenceInvoice": "123454321","ReferenceDelivery": "12345","SystemName": "SHIFT","ReturnOrderLine":[{"ReferenceInvoiceLineId": "Credit Invoice","OrderLineStatus": "REJECTED"},{"ReferenceInvoiceLineId": "Check","OrderLineStatus": "PARTIALLY SHIPPED"}]}}';
        CS_Shipping_Notification_PE__e test = new CS_Shipping_Notification_PE__e();
        test.CS_Case_Details__c = s;
        Database.SaveResult results = EventBus.publish(test);
        System.Test.stopTest();
        system.assertEquals(true, results.isSuccess());
    } 
    
    @isTest
    public static void getEventDataNegative2() {  
        System.Test.startTest();
        Case c1 = [SELECT id, status FROM Case LIMIT 1];
        string cId = String.ValueOf(c1.Id);  
        String s = '{"ReturnOrder": {"CaseID": "'+'5000r000005YeNIBZ9'+'","CaseStatus": "Closed","ReferenceInvoice": "123454321","ReferenceDelivery": "12345","SystemName": "SHIFT","ReturnOrderLine":[{"ReferenceInvoiceLineId": "Credit Invoice","OrderLineStatus": "REJECTED"},{"ReferenceInvoiceLineId": "Check","OrderLineStatus": "PARTIALLY SHIPPED"}]}}';
        CS_Shipping_Notification_PE__e test = new CS_Shipping_Notification_PE__e();
        test.CS_Case_Details__c = s;
        Database.SaveResult results = EventBus.publish(test);
        System.Test.stopTest();
        system.assertEquals(true, results.isSuccess());
    }
}