/**
*  Created Date                                   : 12-Apr-21
*  Created By                                     : Pratigyan
*  Requirement Reference                          : CCM - 373
*  Description			                          : This interface is used to pdate case details in Salesforce from SAP. 
													Corresponds to I-01 interface from SAP → CDM

*****  MODIFICATION HISTORY
Version Number		Date		Last Modified By		Description/User Story Number

**/
public with sharing class CS_InvoiceNotificationTriggerHelper {
    public static String invoiceResponseBody;
    public static CS_SAPSFUpdateCaseParser invoicewrapperResponse;    
    
    	public static void getEventData(List<CS_Invoice_Notification_PE__e> res) {
        try {
        if(res != NULL) {
        invoicewrapperResponse = new CS_SAPSFUpdateCaseParser();
        List<CS_SAPSFUpdateCaseParser> invoicewrapperResponseList = new List<CS_SAPSFUpdateCaseParser>();
        Set<Id> caseIds = new Set<Id>();             
        List<Case> caseUpdate = new List<Case>();  
        
            for(CS_Invoice_Notification_PE__e parseRes : res) {
            string r = parseRes.CS_Case_Details__c;
            invoicewrapperResponse = CS_SAPSFUpdateCaseParser.parse(r);  
                system.debug('wrapper Update -- >'+invoicewrapperResponse);
            String myIdPrefix = String.valueOf(invoicewrapperResponse.caseDetails.CaseID).substring(0,3);
            if((String.isNotBlank(invoicewrapperResponse.caseDetails.CaseID)) 
              && ((invoicewrapperResponse.caseDetails.CaseID.length() == 18)
              || (invoicewrapperResponse.caseDetails.CaseID.length() == 15))
              && (myIdPrefix == '500')) {
                invoicewrapperResponseList.add(invoicewrapperResponse);
               caseIds.add(Id.valueOf(invoicewrapperResponse.caseDetails.CaseID));
              } else {                
                  throw new CS_CustomException(CS_CustomException.CS_INVALID_CASE_ID); 
              }
          }
          
          if(caseIds != NULL) {       
              Map<Id, Case> caseStatusMap = new Map<Id, Case>([SELECT id,CS_Case_Created_From_SAP__c,CS_DOA_Status__c,status,
                                                               CS_Credit_Note_Number__c,CS_Debit_Note_Number__c,type FROM Case where ID in: caseIds]);
               
           for(CS_SAPSFUpdateCaseParser wrap : invoicewrapperResponseList) {
               if(caseStatusMap.containsKey(Id.valueOf(wrap.caseDetails.CaseID))) {
                Case c = new Case();
                c.Id = Id.valueOf(wrap.caseDetails.CaseID); 
                c.Status = CS_ConstantClass.INVOICE_CASE_STATUS;   
				c.CS_DOA_Status__c = CS_ConstantClass.INVOICE_DOA_STATUS;
                   
                 if(wrap.caseDetails.invoicetype==CS_ConstantClass.INVOICE_TYPE || wrap.caseDetails.invoicetype==CS_ConstantClass.INVOICE_TYPE1 || wrap.caseDetails.invoicetype==CS_ConstantClass.INVOICE_TYPE2){ 
              c.CS_Credit_Note_Number__c=wrap.caseDetails.SAPInvoiceId;
              }else if (wrap.caseDetails.invoicetype==CS_ConstantClass.INVOICE_TYPE3 || wrap.caseDetails.invoicetype==CS_ConstantClass.INVOICE_TYPE4) {
                      c.CS_Debit_Note_Number__c=wrap.caseDetails.SAPInvoiceId;
              
              }
                caseUpdate.add(c);   
               } else {
                   throw new CS_CustomException(CS_CustomException.CS_INVALID_CASE_ID); 
               }
             }          
              
            
       				 }
             Update caseUpdate;
       			 } 
             
        }
            
		catch (Exception ex) {
         			   ExceptionLogHandler.createErrorRecord(ex, 'CS_InvoiceNotificationTriggerHelper');
            	}
}
}