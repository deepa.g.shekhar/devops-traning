public class CS_OrderItemTriggerHandler implements CS_TriggerInterface {
       public void BeforeInsert(List<SObject> newRecordList){     
    }
    public void AfterInsert(List<SObject> newRecordList,Map<Id, SObject> newRecordsMap){ 
        CS_OrderItemTriggerHelper.publishReturnOrder((List<OrderItem>)newRecordList);
    }
    public void BeforeUpdate(Map<Id, SObject> newRecordsMap, Map<Id, SObject> oldRecordsMap){}
    public void AfterUpdate(Map<Id, SObject> newRecordsMap, Map<Id, SObject> oldRecordsMap){
        
    }
    public void BeforeDelete(List<SObject> oldRecordList,Map<Id, SObject> oldRecordsMap){}
    public void AfterDelete(Map<Id, SObject> oldRecordsMap){}
    public void AfterUndelete(List<SObject> newRecordList,Map<Id, SObject> newRecordsMap){}  

}