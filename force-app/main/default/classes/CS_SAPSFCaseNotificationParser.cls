/**
*  Created Date                                    : 14-Apr-21
*  Created By                                      : Bajrang
*  Requirement Reference                           : 
*  Description Of Method                           : Parse the JSON of the CaseNotification


*****  MODIFICATION HISTORY
Version Number		Date		Last Modified By		Description/User Story Number
**/
public class CS_SAPSFCaseNotificationParser {
    @AuraEnabled public String errorMessage;
    public CaseDetails  caseDetails;
    public OrderDetails orderDetails;
    public OrderLineItems orderLineItems;
    public CustomerDetails customerDetails;
    
    public class CustomerDetails {
        public String CustomerID;
    }
    
    public class CaseDetails{
        public String CaseID;
        public String CountryCode;
        public String CaseStatus;
        public String Action;
        public String SystemName;
        public string CurrencyCode;
        public string SAPNotes;
    }
    
    public class OrderDetails{
        public String ReferenceInvoice;
        public String ReturnOrderType;
        public String OrderStatus;
        public String SAPNetPrice;
        public String SapOrderId;
        public String OrderDate;

    }
    
    public class OrderLineItems{
        public list<OrderLineItemDetails> OrderLineItemDetails;
    }
    public class OrderLineItemDetails{
        public String SourceSystemOrderLIneId;
        public String  ReferenceInvoiceLineId;
        public String  OrderLineStatus;
        public String  SAPNetPrice;
        public String  RejectionReason;
        public String  SAPOrderLineId;
    }
    
    /**
*  Created Date            : 14-Apr-2021
*  Created By              : Bajrang
*  Requirement Reference   : 
*  Description Of Method   : Deserialize JSON received from the CaseNotification Publisher
**/
    public static CS_SAPSFCaseNotificationParser parse(String Json) {
        return (CS_SAPSFCaseNotificationParser) System.JSON.deserialize(Json, CS_SAPSFCaseNotificationParser.class);
    }
    
}