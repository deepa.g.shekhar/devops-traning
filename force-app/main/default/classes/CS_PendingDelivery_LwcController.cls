/**
*  Created Date                                   : 05-Apr-21
*  Created By                                     : Bajrang
*  Requirement Reference                          :  
*  Description Of Method                          : Delivery LWC Controller

*****  MODIFICATION HISTORY
Version Number		Date		Last Modified By		Description/User Story Number

**/
public class CS_PendingDelivery_LwcController {
    /**
*  Created Date                                   : 05-Apr-21
*  Created By                                     : Bajrang
*  Requirement Reference                          : To be updated 
*  Description Of Method                          : To get the SAP Account details form salesforce
*/
    @AuraEnabled(cacheable=true)    
    public static CS_SAPSFPendingDeliveryParser getDeliveryRecordDetails(String RecordId) {        
        String SapCustomerId;
        CS_SAPSFPendingDeliveryParser responseWrapper;
        try{
            if(String.isNotBlank(RecordId)){
                Account accountDetails=new Account();
                accountDetails = [SELECT CS_Partner_Function__c, CS_SAP_Customer_Id__c, ParentId, Parent.CS_Partner_Function__c, 
                                  Parent.CS_SAP_Customer_Id__c 
                                  from Account 
                                  where Id =: RecordId];
                if(accountDetails != null){
                    if(accountDetails.CS_Partner_Function__c.Contains(CS_ConstantClass.ACCOUNT_SOLD_TO)){
                        SapCustomerId=accountDetails.CS_SAP_Customer_Id__c;                        
                    }
                    else if(accountDetails.CS_Partner_Function__c.Contains(CS_ConstantClass.ACCOUNT_SHIP_TO)){
                        if(accountDetails.ParentId!=null){
                            SapCustomerId=accountDetails.Parent.CS_SAP_Customer_Id__c;    
                        }                                                
                    }
                }
                else{
                    responseWrapper.isSuccess = false;
                    responseWrapper.errorMessage = Label.CS_AccountNotFound;                    
                }
            }
            else if(String.isBlank(RecordId)){
                responseWrapper.isSuccess=false;
                responseWrapper.errorMessage = Label.CS_AccountIDNull;
            }
        }
        catch(Exception ex){
            responseWrapper.isSuccess=false;
            responseWrapper.errorMessage = Label.CS_SAPException; 
            ExceptionLogHandler.createErrorRecord(ex, 'CS_PendingDeliveryLwcController');
        }
        responseWrapper = CS_SAPSFPendingDeliveryResponse.getDeliveryItems(SapCustomerId);
        return responseWrapper;
        
    }
}