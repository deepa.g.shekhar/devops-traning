/**
*  Created Date                                   : 05-April-21
*  Created By                                     : Narmada
*  Requirement Reference                          : To be updated 
*  Description			                          : Test class for Invoice Response Data

*****  MODIFICATION HISTORY
Version Number		Date		Last Modified By		Description/User Story Number

**/

@isTest
public class CS_SAPSFInvoiceResponseDataTest {
    
    public static FINAL String attributeName ='SAP_Billing_Document_Service';
    public static CS_SAPSFInvoiceParser invoiceItemsResponse=new CS_SAPSFInvoiceParser();
    public static  String acc1;
    public static  String acc;
    public static  String acc2;
    public static  String acc3;
    public static  List<Account> accountInsert = new List<Account>();
    
    @testSetup
    public static void createTestData(){
        List<CS_SAP_Picklist_Mapping__c> mapInsert = new List<CS_SAP_Picklist_Mapping__c>();
        mapInsert = CS_TestUtility.createSAPPickListMapping(); 
        accountInsert = CS_TestUtility.createAccount(); 
        
    } 
    @isTest
    public static void getAccountDetailsTestPositive(){
        Integer i;
        
        accountInsert = CS_TestUtility.createAccount(); 
        
        for(i=0; i<accountInsert.size(); i++){
            if(accountInsert[i].CS_Partner_Function__c=='Sold To' && accountInsert[i].CS_SAP_Customer_Id__c!=null ){
                acc1=accountInsert[i].Id;
                
            }
            if(accountInsert[i].CS_Partner_Function__c=='Ship To'){
                acc2=accountInsert[i].Id;  
                
            }
            if(accountInsert[i].CS_SAP_Customer_Id__c==null){
                acc3=accountInsert[i].Id;  
                
            }
        }
        
        
        CS_Integration_Attributes__mdt integrationTest = new CS_Integration_Attributes__mdt();
        integrationTest = CS_TestUtility.fetchIntegrationAttributeRecord(attributeName);
        Test.startTest();
        String body = '{""d"":{""results"":[{"__metadata":{"id":"","uri":"","type":"API_BILLING_DOCUMENT_SRV.A_BillingDocumentType"},"BillingDocument":"7001007147","SDDocumentCategory":"M","BillingDocumentCategory":"L","BillingDocumentType":"ZF2","CreationDate":"","CreationTime":"PT00H17M18S","LastChangeDate":null,"LastChangeDateTime":"","LogicalSystem":"SHACLNT100","SalesOrganization":"ES02","DistributionChannel":"10","Division":"C0","BillingDocumentDate":"","BillingDocumentIsCancelled":false,"CancelledBillingDocument":"","ForeignTrade":"","IsExportDelivery":"","BillingDocCombinationCriteria":"","ManualInvoiceMaintIsRelevant":false,"IsIntrastatReportingRelevant":false,"IsIntrastatReportingExcluded":false,"TotalNetAmount":"700.66","TransactionCurrency":"EUR","StatisticsCurrency":"EUR","TaxAmount":"28.03","TotalGrossAmount":"728.69","CustomerPriceGroup":"YM","PriceListType":"ZI","TaxDepartureCountry":"ES","VATRegistration":"ESB50871284","VATRegistrationOrigin":"B","VATRegistrationCountry":"ES","HierarchyTypePricing":"A","IsEUTriangularDeal":false,"SDPricingProcedure":"ZESA01","ShippingCondition":"ZH","PayerParty":"100035392","ContractAccount":"","CustomerPaymentTerms":"I060","PaymentMethod":"","PaymentReference":"7001007147","FixedValueDate":null,"AdditionalValueDays":"0","SEPAMandate":"","CompanyCode":"ES02","FiscalYear":"0000","AccountingDocument":"","CustomerAccountAssignmentGroup":"01","AccountingExchangeRateIsSet":false,"AbsltAccountingExchangeRate":"1.00000","AcctgExchangeRateIsIndrctQtan":false,"ExchangeRateDate":"","ExchangeRateType":"","DocumentReferenceID":"7001007147","AssignmentReference":"1822004270","DunningArea":"","DunningBlockingReason":"","DunningKey":"","InternalFinancialDocument":"","IsRelevantForAccrual":false,"SoldToParty":"100028060","PartnerCompany":"","PurchaseOrderByCustomer":"1822004270","CustomerGroup":"IB","Country":"ES","CityCode":"","SalesDistrict":"ES0001","Region":"29","County":"","CreditControlArea":"ES01","CustomerRebateAgreement":"","SalesDocumentCondition":"0000071420","OverallSDProcessStatus":"C","OverallBillingStatus":"A","AccountingPostingStatus":"C","AccountingTransferStatus":"C","BillingIssueType":"","InvoiceListStatus":"","OvrlItmGeneralIncompletionSts":"","OverallPricingIncompletionSts":"","BillingDocumentListType":"ZLR","BillingDocumentListDate":null}]}}';
        CS_MockHttpResponseGenerator mock = new CS_MockHttpResponseGenerator(body,200,'OK');
        Test.setMock(HttpCalloutMock.class, mock);
        CS_Invoice_LwcController.getAccountDetails(acc1);
        CS_Invoice_LwcController.getAccountRecordDetails(acc1);
        
        CS_Invoice_LwcController.getAccountDetails(acc2);
        CS_Invoice_LwcController.getAccountRecordDetails(acc2);
        CS_Invoice_LwcController.getAccountDetails(acc3);
        CS_Invoice_LwcController.getAccountDetails('');
        CS_Invoice_LwcController.getAccountRecordDetails('');
        CS_Invoice_LwcController.getAccountDetails('abc');
        CS_Invoice_LwcController.getAccountRecordDetails('abc');
        CS_SAPSFInvoiceResponseData.getInvoiceItems('12345');
        CS_SAPSFInvoiceResponseData.getInvoiceDetailsByInvoiceNumber('1', '123456');
        CS_SAPSFInvoiceResponseData.getInvoiceDetailsByPONumber('2', '1234567');
        Test.stopTest();
        System.assertEquals(invoiceItemsResponse.isSuccess=true, invoiceItemsResponse.isSuccess);
        
    }
    @isTest
    public static void getInvoiceItemsTestNegative1(){
        
        CS_Integration_Attributes__mdt integrationTest = new CS_Integration_Attributes__mdt();
        integrationTest = CS_TestUtility.fetchIntegrationAttributeRecord(attributeName);
        Test.startTest();
        String body = '';
        CS_MockHttpResponseGenerator mock = new CS_MockHttpResponseGenerator(body,500,'OK');
        Test.setMock(HttpCalloutMock.class, mock);
        CS_SAPSFInvoiceResponseData.getInvoiceItems('12345');
        CS_SAPSFInvoiceResponseData.getInvoiceDetailsByInvoiceNumber('1', '123456');
        CS_SAPSFInvoiceResponseData.getInvoiceDetailsByPONumber('2', '1234567');
        Test.stopTest();
        System.assertEquals(invoiceItemsResponse.isSuccess=false, invoiceItemsResponse.isSuccess);
        
    }
    @isTest
    public static void getInvoiceItemsTestPositve(){
        
        CS_Integration_Attributes__mdt integrationTest = new CS_Integration_Attributes__mdt();
        integrationTest = CS_TestUtility.fetchIntegrationAttributeRecord(attributeName);
        Test.startTest();
        String body = '{ "d": { "results": [ { "BillingDocument": "string", "SDDocumentCategory": "string", "BillingDocumentCategory": "string", "BillingDocumentType": "string", "CreationDate": "/Date(1492041600000)/", "BillingDocumentListType": "string", "BillingDocumentListDate": "/Date(1492041600000)/", "to_Item": { "results": [ { "ItemWeightUnit": "string", "ItemVolume": "0", "ShippingPoint": "string", "HigherLevelItemUsage": "string", "to_ItemText": { "results": [ { "LongTextID": "string", "LongText": "string" } ] }, "to_Partner": { "results": [ { "Personnel": "string", "ContactPerson": "string" } ] }, "to_PricingElement": { "results": [ { "ConditionIsManuallyChanged": true, "ConditionIsForConfiguration": true, "VariantCondition": "string" } ] } } ] }, "to_Partner": { "results": [ { "ContactPerson": "string" } ] }, "to_PricingElement": { "results": [ { "ConditionIsForConfiguration": true, "VariantCondition": "string" } ] }, "to_Text": { "results": [ { "LongTextID": "string", "LongText": "string" } ] } } ] } }';
        CS_MockHttpResponseGenerator mock = new CS_MockHttpResponseGenerator(body,200,'OK');
        Test.setMock(HttpCalloutMock.class, mock);
        CS_SAPSFInvoiceResponseData.getInvoiceItems('123456');       
        Test.stopTest();
        System.assertEquals(invoiceItemsResponse.isSuccess=true, invoiceItemsResponse.isSuccess);
        
    }
    @isTest
    public static void getInvoiceItemsTestNegative2(){
        
        CS_Integration_Attributes__mdt integrationTest = new CS_Integration_Attributes__mdt();
        integrationTest = CS_TestUtility.fetchIntegrationAttributeRecord(attributeName);
        Test.startTest();
        String body = '';
        CS_MockHttpResponseGenerator mock = new CS_MockHttpResponseGenerator(body,200,'OK');
        Test.setMock(HttpCalloutMock.class, mock);
        CS_SAPSFInvoiceResponseData.getInvoiceItems('12345');
        CS_SAPSFInvoiceResponseData.getInvoiceDetailsByInvoiceNumber('1', '123456');
        CS_SAPSFInvoiceResponseData.getInvoiceDetailsByPONumber('2', '1234567');
        Test.stopTest();
        System.assertEquals(invoiceItemsResponse.isSuccess=false, invoiceItemsResponse.isSuccess);
        
    }
    @isTest
    public static void getInvoiceDetailsByInvoiceNumberTestPositive(){
        
        CS_Integration_Attributes__mdt integrationTest = new CS_Integration_Attributes__mdt();
        integrationTest = CS_TestUtility.fetchIntegrationAttributeRecord(attributeName);
        Test.startTest();
        String body = '{ "d": { "results": [ { "BillingDocument": "string", "SDDocumentCategory": "string", "BillingDocumentCategory": "string", "BillingDocumentType": "string", "CreationDate": "/Date(1492041600000)/", "BillingDocumentListType": "string", "BillingDocumentListDate": "/Date(1492041600000)/", "to_Item": { "results": [ { "ItemWeightUnit": "string", "ItemVolume": "0", "ShippingPoint": "string", "HigherLevelItemUsage": "string", "to_ItemText": { "results": [ { "LongTextID": "string", "LongText": "string" } ] }, "to_Partner": { "results": [ { "Personnel": "string", "ContactPerson": "string" } ] }, "to_PricingElement": { "results": [ { "ConditionIsManuallyChanged": true, "ConditionIsForConfiguration": true, "VariantCondition": "string" } ] } } ] }, "to_Partner": { "results": [ { "ContactPerson": "string" } ] }, "to_PricingElement": { "results": [ { "ConditionIsForConfiguration": true, "VariantCondition": "string" } ] }, "to_Text": { "results": [ { "LongTextID": "string", "LongText": "string" } ] } } ] } }';
        CS_MockHttpResponseGenerator mock = new CS_MockHttpResponseGenerator(body,200,'OK');
        Test.setMock(HttpCalloutMock.class, mock);
        CS_SAPSFInvoiceResponseData.getInvoiceDetailsByInvoiceNumber('12', '123456');
        CS_SAPSFInvoiceResponseData.getInvoiceDetailsByInvoiceNumber(' ', ' ');        
        Test.stopTest();
        System.assertEquals(invoiceItemsResponse.isSuccess=true, invoiceItemsResponse.isSuccess);
        
    }
    @isTest
    public static void getInvoiceDetailsByPONumberTestPositive(){
        
        CS_Integration_Attributes__mdt integrationTest = new CS_Integration_Attributes__mdt();
        integrationTest = CS_TestUtility.fetchIntegrationAttributeRecord(attributeName);
        Test.startTest();
        String body = '{ "d": { "results": [ { "BillingDocument": "string", "SDDocumentCategory": "string", "BillingDocumentCategory": "string", "BillingDocumentType": "string", "CreationDate": "/Date(1492041600000)/", "BillingDocumentListType": "string", "BillingDocumentListDate": "/Date(1492041600000)/", "to_Item": { "results": [ { "ItemWeightUnit": "string", "ItemVolume": "0", "ShippingPoint": "string", "HigherLevelItemUsage": "string", "to_ItemText": { "results": [ { "LongTextID": "string", "LongText": "string" } ] }, "to_Partner": { "results": [ { "Personnel": "string", "ContactPerson": "string" } ] }, "to_PricingElement": { "results": [ { "ConditionIsManuallyChanged": true, "ConditionIsForConfiguration": true, "VariantCondition": "string" } ] } } ] }, "to_Partner": { "results": [ { "ContactPerson": "string" } ] }, "to_PricingElement": { "results": [ { "ConditionIsForConfiguration": true, "VariantCondition": "string" } ] }, "to_Text": { "results": [ { "LongTextID": "string", "LongText": "string" } ] } } ] } }';
        CS_MockHttpResponseGenerator mock = new CS_MockHttpResponseGenerator(body,200,'OK');
        Test.setMock(HttpCalloutMock.class, mock);        
        CS_SAPSFInvoiceResponseData.getInvoiceDetailsByPONumber('20', '1234567');
        CS_SAPSFInvoiceResponseData.getInvoiceDetailsByPONumber(' ', ' ');
        Test.stopTest();
        System.assertEquals(invoiceItemsResponse.isSuccess=true, invoiceItemsResponse.isSuccess);
        
    }
    
}