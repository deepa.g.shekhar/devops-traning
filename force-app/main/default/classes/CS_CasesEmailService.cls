/**
*  Created Date                                   : 04-Mar-21
*  Created By                                     : Vasanth G M
*  Requirement Reference                          : To be updated 
*  Description Of Method                          : Email Service Class to handle Email to Case

*****  MODIFICATION HISTORY
Version Number    Date    Last Modified By    Description/User Story Number

**/

global class CS_CasesEmailService implements Messaging.InboundEmailHandler{
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email,Messaging.InboundEnvelope envelope) {
        Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();
        
        // get contents of email 
        string subject      = email.subject;
        string myPlainText  = email.plainTextBody;
        String htmlbody     = email.htmlbody;
        string fromname     = email.fromname;
        string fromemail    = email.fromaddress;
        string[] toemail    = email.toaddresses;
        string emailtype    = 'new';
        String contactId=null;
        String contactAccountId=null;
        
        //print debug info
        System.debug('\n\nTo: ' + toemail + '\n' + 'From: ' + fromemail + '\nSubject\n' + subject + '\nBody\n' + myPlainText);
        System.debug('@@@Email Headers : ' + email.messageId);
        System.debug('@@@Email References : ' + email.references);
        
        // First, instantiate a new Pattern object "MyPattern"
        Pattern MyPattern = Pattern.compile('[0-9]{8}');
        
        // Then instantiate a new Matcher object "MyMatcher"
        Matcher MyMatcher = MyPattern.matcher(subject);
        
        //status of the matcher
        boolean reply = MyMatcher.find();
        system.debug('Reply: ' + reply);
        
        //determine if this is a new case or a reply to an existing one
        if(reply == true){
            emailtype = 'reply';
        }
        List<Contact> contact=new List<Contact>();
        Account account=new Account();
        if(String.isNotBlank(fromemail)){
            
        contact=[Select Id,Name,AccountId from Contact where Email=:fromemail];
            
        }
        if(contact.size()==1 && contact!=null){
              
            for(Contact con:contact){
                contactId=con.Id;
                contactAccountId=con.AccountId;
            }
        }
        
        //If the email is NEW from customer
        if(emailtype == 'new'){
            // new Case object to be created
            Case[] newCase = new Case[0];
            try {
                newCase.add(new Case(Description = myPlainText,
                                     Subject = subject,
                                     Origin = 'Email',
                                     SuppliedEmail = email.fromAddress,
                                     SuppliedName = email.fromName,
                                    OwnerId = '00Gq0000004I8Dh',AccountId=contactAccountId,ContactId=contactId));
                insert newCase;  
                System.debug('New Case: ' + newCase );
                //Relate the Inbound email to the case
                emailmsg(email,newCase[0]);
            }
            catch (Exception e) {
                System.debug('\n\nError:: ' + e + '\n\n');
                string excep = 'Message: ' + e.getMessage() + '\n' + e.getCause() + '\n\n' + email.subject + '\n' + fromemail + '\n' + fromname + '\n' + toemail;
                System.debug(+excep);
            }
        }
        
        //If the Email is a REPLY to the existing CASE
        else if (emailtype == 'reply') { 
            string caseNumber = MyMatcher.group(0);
            system.debug('Case Number: ' + caseNumber); 
            
            //Find the case to which the email is related to
            Case[] getCase = new Case[0];
            getCase = [select casenumber, id from case where casenumber = :caseNumber limit 1];
            
            try {
                getCase[0].Status = 'Reply from Customer';
                update getCase[0];
                emailmsg(email,getCase[0]);
                result.success=true;
            }
            catch (Exception e) {
                result.success = false;
                result.message = 'Oops, I failed. Error : '+e.getMessage();
            }
        }
        return result;
    }
    
/**
*  Created Date                                   : 04-Mar-21
*  Created By                                     : Vasanth G M
*  Requirement Reference                          : To be updated 
*  Description Of Method                          : Method to create email record in EmailMessage object and relate it to Case
**/
    Public void emailmsg(Messaging.InboundEmail email, Case getCase){
        EmailMessage[] newEmail = new EmailMessage[0];
        string cText = '';
        integer i=0;
        newEmail.add(new EmailMessage(FromAddress   = email.fromaddress,
                                      FromName      = email.fromname,
                                      ToAddress     = email.toaddresses[0],
                                      Subject       = email.subject,
                                      TextBody      = email.plainTextBody,
                                      HtmlBody      = email.htmlbody,
                                      Incoming      = true,
                                      MessageDate   = System.now(),    
                                      ParentId      = getCase.Id));
        if ( email.ccAddresses!=null && email.ccAddresses.size() > 0){
            
            for (i = 0 ; i < email.ccAddresses.size() ; i++) {
                cText = cText + email.ccAddresses[i]+';';
            }
            newEmail[0].CcAddress = cText;
        }
        if ( email.headers!=null && email.headers.size() > 0){
            cText = '';
            for (i = 0 ; i < email.headers.size() ; i++) {
                cText = cText + email.headers[i].name+' = '+email.headers[i].value;
            }
            newEmail[0].headers = cText;
        }
        insert newEmail;
        System.debug('New Email Object: ' + newEmail );
        addAttachment(email,newEmail[0]);
    }
    
/**
*  Created Date                                   : 04-Mar-21
*  Created By                                     : Vasanth G M
*  Requirement Reference                          : To be updated 
*  Description Of Method                          : Method to ADD incoming attachments to the EmailMessage record
**/
    
    Public void addAttachment(Messaging.InboundEmail email, EmailMessage newEmail){
        //For Binary Attachments:
        if (email.binaryAttachments != null)
        {
            for (Messaging.InboundEmail.binaryAttachment binAttach :email.binaryAttachments)
            {
                ContentVersion cv = new ContentVersion();
                String binfilename = binAttach.fileName;
                String[] bfn_list = binfilename.split('\\.');  /*used escape parameter for '.' delimiter */
                cv.VersionData = binAttach.body;
                cv.Title = bfn_list[0];
                cv.PathOnClient = binAttach.fileName;
                insert cv;
                
                cv = [select ContentDocumentId from ContentVersion where id = :cv.id limit 1];
                
                ContentDocumentLink cdl = new ContentDocumentLink();
                cdl.LinkedEntityId = newEmail.id;
                cdl.ContentDocumentId = cv.ContentDocumentId;
                cdl.ShareType = 'V';
                insert cdl;
            } 
        }
        
        //For Text Attachments:
        if (email.textAttachments != null)
        {
            for (Messaging.InboundEmail.textAttachment tAttach :email.textAttachments)
            {
                ContentVersion cv = new ContentVersion();
                String tfilename = tAttach.fileName;
                String[] tfn_list = tfilename.split('\\.');
                cv.VersionData = Blob.valueOf(tAttach.body); /* VersionData is a Blob field whereas tAttach.Body is text field, so converting it to Blob and storing it in VersionData*/
                cv.Title = tfn_list[0];
                cv.PathOnClient = tAttach.fileName;
                insert cv;
                
                cv = [select ContentDocumentId from ContentVersion where id = :cv.id limit 1];
                
                ContentDocumentLink cdl = new ContentDocumentLink();
                cdl.LinkedEntityId = newEmail.id;
                cdl.ContentDocumentId = cv.ContentDocumentId;
                cdl.ShareType = 'V';
                insert cdl;
            } 
        }
    }
}