/**
* Created Date : 14-April-21
* Created By : Indu 
* Requirement Reference :
* Description Of Method : Helper class On Case Trigger 

***** MODIFICATION HISTORY
Version Number  Date  Last Modified By   Description/User   Story Number
                                                               CCM-357
**/
public with sharing class CS_CaseTriggerHelper {
    @testVisible private static List<CS_Return_Orders_PE__e> eventReturnCase = new List<CS_Return_Orders_PE__e>();
    public static void publishCaseId(List<Case> caseList) {
        try {
            List<CS_SFSAPReturnOrderParser> parserData = new List<CS_SFSAPReturnOrderParser>();
            
            Set<Id> listOfCaseIds = new Set<Id>();
            for (Case caseDetail : caseList) {
                listOfCaseIds.add(caseDetail.Id);
            }
            
            List<Case> listOfCase = new List<Case>();
            listOfCase = [Select Id, CS_Case_Created_From_SAP__c, CS_Initial_Order__r.CS_Return_Order_Type__c, 
                          CS_Initial_Order__r.CS_SAP_Order_Number__c,Account.BillingCountry, Account.CS_SAP_Customer_Id__c
                          FROM Case
                          WHERE Id IN:listOfCaseIds];
            
            for(Case caseData : listOfCase) {
                if(CaseData.CS_Case_Created_From_SAP__c == true){
                    CS_SFSAPReturnOrderParser wrap = new CS_SFSAPReturnOrderParser();
                    CS_SFSAPReturnOrderParser.CaseDetails caseWrap = new CS_SFSAPReturnOrderParser.CaseDetails();
                    CS_SFSAPReturnOrderParser.CustomerDetails custWrap = new CS_SFSAPReturnOrderParser.CustomerDetails();
                    CS_SFSAPReturnOrderParser.OrderDetails orderWrap = new CS_SFSAPReturnOrderParser.OrderDetails();
                    caseWrap.CaseID  = JSON.serialize(caseData.Id);
                    caseWrap.CountryCode = JSON.serialize(caseData.Account.BillingCountry);
                    caseWrap.Market = '10';
                    wrap.CaseDetails = caseWrap;
                    custWrap.CustomerId = JSON.serialize(caseData.Account.CS_SAP_Customer_Id__c);
                    wrap.CustomerDetails = custWrap;
                    orderWrap.ReturnOrderType ='ZERB';
                    orderWrap.SapOrderId = JSON.serialize(caseData.CS_Initial_Order__r.CS_SAP_Order_Number__c);
                    wrap.OrderDetails = orderWrap;
                    parserData.add(wrap);
                }
            }  
            
            for (CS_SFSAPReturnOrderParser data : parserData )
            {
                CS_Return_Orders_PE__e dataToPublish = new CS_Return_Orders_PE__e();
                dataToPublish.CS_Case_Details__c = JSON.serialize(data.CaseDetails);
                dataToPublish.CS_Order_Details__c = JSON.serialize(data.OrderDetails);
                dataToPublish.CS_Customer_Details__c = JSON.serialize(data.CustomerDetails);
                eventReturnCase.add(dataToPublish);
            }
            
            List<Database.SaveResult> results = EventBus.publish(eventReturnCase);
            for (Database.SaveResult sr : results) {
                if (sr.isSuccess()) {
                    System.debug('Successfully published event.');
                } else {
                    for(Database.Error err : sr.getErrors()) {
                        System.debug('Error returned: ' + err.getStatusCode() + ' - ' +err.getMessage());
                    }
                }
            }
        }
        catch(Exception ex) {
            ExceptionLogHandler.createErrorRecord(ex, 'CS_CaseTriggerHelper');
        }  
        
    }
    /*public static void assignEntitlementOnNewlyCreatedRequests(List<Case> newCaseList) {         
      //Fetching the Entitlement that needs to be populated in cases
      try {
          List<Entitlement> eList = [Select Id From Entitlement WHERE Name =: CS_ConstantClass.GLOBAL_ENTITLEMENT_FOR_REQUEST_NAME];
          if (eList != null && !eList.isEmpty()) {
              Entitlement e = eList[0];
              for (Case c : newCaseList) {
                  if (c.Type == CS_ConstantClass.CASE_REQUEST) {
                      // Populating the entitlement process on case
                      c.EntitlementId = e.ID;
                  }
              }              
          }
      } catch (Exception ex) {
            ExceptionLogHandler.createErrorRecord(ex, 'CS_CaseTriggerHelper_assignEntitlementOnNewlyCreatedRequests');
      }
    }
    public static void assignEntitlementOnUpdatedRequestsAndCloseMilestonesOnClosedCases(List<Case> updatedCaseList) { 
        try {
            List<Id> caseIdList = new List<Id>();    
            //Fetching the Entitlement that needs to be populated in cases
            List<Entitlement> eList = [Select Id From Entitlement WHERE Name =:CS_ConstantClass.GLOBAL_ENTITLEMENT_FOR_REQUEST_NAME];
            for (Case c : updatedCaseList) {
                if(c.Type == CS_ConstantClass.CASE_REQUEST && c.EntitlementId == null && eList != null && !eList.isEmpty()) {
                    // Populating the entitlement process on case
                    c.Entitlementid = eList[0].Id;
                }
                if(c.Status=='Closed') {
                    caseIdList.add(c.Id);
                }
            }    
            //Fetching the Case Milestones that needs to be closed because they are related to closed cases    
            List<CaseMilestone> cmList = [SELECT Id FROM CaseMilestone WHERE CaseId IN :caseIdList];
            if (!cmList.isEmpty()) {
                for (CaseMilestone cm : cmList) {
                    // Completing the milestone
                    cm.CompletionDate = System.now();
                }
                update cmList;
            }
        } catch (Exception ex) {
            ExceptionLogHandler.createErrorRecord(ex, 'CS_CaseTriggerHelper_assignEntitlementOnUpdatedRequestsAndCloseMilestonesOnClosedCases');
      	}
    }*/
}