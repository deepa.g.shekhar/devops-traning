/**
*  Created Date                                   : 07-April-21
*  Created By                                     : Harshita Gupta
*  Requirement Reference                          : To be updated 
*  Description			                          : Test class for Parsing Delivery Data

*****  MODIFICATION HISTORY
Version Number		Date		Last Modified By		Description/User Story Number

**/
@IsTest
public class CS_SAPSFDeliveryParserTest {
	static testMethod void testParse() {
		String json = '{'+
		'  \"d\": {'+
		'    \"results\": ['+
		'      {'+
		'        \"CreationDate\": \"/Date(1492098664000)/\",'+
		'        \"FormattedCreationDate\": \"2018-07-01T00:00:00.000Z\",'+
		'        \"DeliveryDocument\": \"string\",'+
		'        \"DeliveryDocumentType\": \"string\",'+
		'        \"ShipToParty\": \"string\",'+
		'        \"SoldToParty\": \"string\",'+
        '        \"ActualGoodsMovementDate\": \"string\",'+
        '        \"OverallProofOfDeliveryStatus\": \"string\",'+
		'              \"to_Address\": {'+
		'                \"AddressID\": \"string\",'+
		'                \"BusinessPartnerName1\": \"string\",'+
		'                \"TransportZone\": \"string\"'+
		'              }'+
		'            }'+
		'          ]'+
		'        },'+
		'        \"to_DeliveryDocumentItem\": {'+
		'          \"results\": ['+
		'            {'+
		'              \"WarehouseStockCategory\": \"string\",'+
		'              \"WarehouseStorageBin\": \"string\",'+
		'              \"to_DocumentFlow\": {'+
		'                \"results\": ['+
		'                  {'+
		'                    \"TransferOrderInWrhsMgmtIsConfd\": \"Unknown Type: boolean,null\"'+
		'                  }'+
		'                ]'+
		'              },'+
		'              \"to_SerialDeliveryItem\": {'+
		'                \"SDDocumentCategory\": \"string\",'+
		'                \"to_MaintenanceItemObject\": {'+
		'                  \"results\": ['+
		'                    {'+
		'                      \"Material\": \"string\",'+
		'                      \"SerialNumber\": \"string\"'+
		'                    }'+
		'                  ]'+
		'                }'+
		'              }'+
		'            }'+
		'          ]'+
		'        }'+
		'      }'+
		'    ]'+
		'  }'+
		'}';
		CS_SAPSFDeliveryParser obj = CS_SAPSFDeliveryParser.parse(json);
		System.assert(obj != null);
	}
}