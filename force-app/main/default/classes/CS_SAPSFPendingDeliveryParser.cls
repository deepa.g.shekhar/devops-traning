/**
*  Created Date                                   : 02-Apr-2021
*  Created By                                     : Bajrang
*  Requirement Reference                          : To be updated 
*  Description Of Method                          : Json Parsing for all pending delivery response body

*****  MODIFICATION HISTORY
Version Number		Date		Last Modified By		Description/User Story Number                           
**/

public class CS_SAPSFPendingDeliveryParser {
    
    @AuraEnabled public AllPendingDelivery d;
    @AuraEnabled public Boolean isSuccess;
    @AuraEnabled public String errorMessage;
    
    public class AllPendingDelivery {        
        @AuraEnabled public List<PendingDelivery> results;
    }
    public class PendingDelivery {
        @AuraEnabled public String salesOrder;
        @AuraEnabled public String creationDate;
        @AuraEnabled public DateTime formattedCreationDate;
        @AuraEnabled public String purchaseOrderByCustomer;
        @AuraEnabled public String customerPurchaseOrderType;
        @AuraEnabled public String material;
        @AuraEnabled public String salesOrderItemText;
        @AuraEnabled public String itemVolume;
        @AuraEnabled public String itemVolumeUnit;
        @AuraEnabled public String requestedQuantity;
    }
    
    @AuraEnabled(cacheable=true)
    public static CS_SAPSFPendingDeliveryParser parse(String json) {
        return (CS_SAPSFPendingDeliveryParser) System.JSON.deserialize(json, CS_SAPSFPendingDeliveryParser.class);
    }
}