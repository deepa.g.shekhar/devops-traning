/**
*  Created Date                                   : 06-April-21
*  Created By                                     : Narmada
*  Requirement Reference                          :  
*  Description Of Method                          : Return orders wrapper

*****  MODIFICATION HISTORY
Version Number      Date        Last Modified By        Description/User Story Number

**/
public With Sharing class CS_SAPSFReturnOrdersParser {
    
    @AuraEnabled public ReturnOrderItem d;
    @AuraEnabled public Boolean isSuccess;
    @AuraEnabled public String errorMessage;
    public class ReturnOrderItem {
        @AuraEnabled public List<ReturnOrderResults> results;         
    }
    
    public class ReturnOrderResults {
        
        @AuraEnabled public String customerReturn; 
        @AuraEnabled public String customerReturnType;
        @AuraEnabled public String formattedCustomerReturnType;
        @AuraEnabled public String creationDate; 
        @AuraEnabled public String createdByUser;
        @AuraEnabled public DateTime formattedCreationDate;       
        @AuraEnabled public String purchaseOrderByCustomer;        
        @AuraEnabled public String totalNetAmount;         
        @AuraEnabled public String requestedDeliveryDate; 
        @AuraEnabled public DateTime formattedDeliveryDate;    
        @AuraEnabled public String headerBillingBlockReason; 
        @AuraEnabled public String formattedHeaderBillingBlockReason;
        @AuraEnabled public String deliveryBlockReason;
        @AuraEnabled public String formattedDeliveryBlockReason;
        @AuraEnabled public String overallSDProcessStatus; 
        @AuraEnabled public String formattedOverallSDProcessStatus;
        
    }
    @AuraEnabled(cacheable=true)
    public static CS_SAPSFReturnOrdersParser parse(String json) {
        return (CS_SAPSFReturnOrdersParser) System.JSON.deserialize(json, CS_SAPSFReturnOrdersParser.class);
    }   
    
}