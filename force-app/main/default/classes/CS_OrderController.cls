/**
*  Created Date                                   : 02-Apr-21
*  Created By                                     : Indu
*  Requirement Reference                          : CCM-124
*  Description Of Method                          : Order LWC Controller

*****  MODIFICATION HISTORY
Version Number		Date		Last Modified By		Description/User Story Number
01                 02-April-2021                            CCM-124                     
**/
public with sharing class CS_OrderController {  
    
    /**
*  Created Date                                   : 02-Apr-21
*  Created By                                     : Indu
*  Requirement Reference                          : CCM-124     
*  Description Of Method                          : To get the SAP Account details form salesforce
*/
    
    @AuraEnabled(cacheable=true)    
    public static CS_OrderParser getAccountDetails(String RecordId) {  
        
        String SapCustomerId;
        CS_OrderParser responseWrapper = new CS_OrderParser() ;
        try{
            if(String.isNotBlank(RecordId)){
                responseWrapper.isSuccess = true;
                Account accountDetails=new Account();
                accountDetails = [SELECT CS_Partner_Function__c, CS_SAP_Customer_Id__c, ParentId, Parent.CS_Partner_Function__c, 
                                  Parent.CS_SAP_Customer_Id__c 
                                  FROM Account 
                                  WHERE Id =: RecordId];
                if(accountDetails != null){
                    if(accountDetails.CS_Partner_Function__c.Contains(CS_ConstantClass.ACCOUNT_SOLD_TO)){
                        SapCustomerId=accountDetails.CS_SAP_Customer_Id__c;                      
                    }
                    
                    else if(accountDetails.CS_Partner_Function__c.Contains(CS_ConstantClass.ACCOUNT_SHIP_TO)){
                        if(accountDetails.ParentId != null){
                            SapCustomerId=accountDetails.Parent.CS_SAP_Customer_Id__c;    
                        }                                                
                    }
                } //If account details != null END
                else{ //If account details = null
                    responseWrapper.isSuccess = false;
                    responseWrapper.errorMessage = Label.CS_AccountNotFound; 
                    
                }             
            }
            else if(String.isBlank(RecordId)){
                responseWrapper.isSuccess=false;
                responseWrapper.errorMessage = Label.CS_AccountIDNull; 
            }
        }
        catch(Exception ex){
            ExceptionLogHandler.createErrorRecord(ex, 'CS_OrderController');
        }
        responseWrapper = CS_SAPSFOrderResponse.getOrderItems(SapCustomerId);        
        return responseWrapper;        
    }        
}