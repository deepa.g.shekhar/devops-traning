/**
*  Created Date                                   : 24-Mar-21
*  Created By                                     : Narmada
*  Requirement Reference                          : CCM44 
*  Description Of Method                          : Invoice LWC Controller



*****  MODIFICATION HISTORY
Version Number        Date        Last Modified By        Description/User Story Number



**/
public With Sharing class CS_InvoiceLwcController {
    
    /**
*  Created Date                                   : 24-Mar-21
*  Created By                                     : Narmada
*  Requirement Reference                          : CCM44
*  Description Of Method                          : To get the SAP Account details form salesforce
*/
    public static String getAccountRecordDetails(String recordId){
        String sapCustomerId = '';
        try{
            if(String.isNotBlank(recordId)){
                Account accountDetails=new Account();
                accountDetails = [SELECT CS_Partner_Function__c, CS_SAP_Customer_Id__c, ParentId, Parent.CS_Partner_Function__c, 
                                  Parent.CS_SAP_Customer_Id__c 
                                  from Account 
                                  where Id =: recordId];
               // if(accountDetails != null){
                    if(accountDetails.CS_Partner_Function__c.Contains(CS_ConstantClass.ACCOUNT_SOLD_TO)){
                        sapCustomerId=accountDetails.CS_SAP_Customer_Id__c;                        
                    }
                    else if(accountDetails.CS_Partner_Function__c.Contains(CS_ConstantClass.ACCOUNT_SHIP_TO)){
                        if(accountDetails.ParentId!=null){
                            sapCustomerId=accountDetails.Parent.CS_SAP_Customer_Id__c;    
                        }                                                
                    }
                //}               
            }            
        }
        catch(Exception ex){
            ExceptionLogHandler.createErrorRecord(ex, 'CS_InvoiceLwcController');
        }
        return sapCustomerId;        
    }   
    
    /**
*  Created Date                                   : 24-Mar-21
*  Created By                                     : Narmada
*  Requirement Reference                          : CCM44
*  Description Of Method                          : To get the SAP Account details form salesforce
*/
    @AuraEnabled(cacheable=true)    
    public static CS_SAPSFInvoiceParser getAccountDetails(String accountRecordId) {  
        
        String sapCustomerId;
        CS_SAPSFInvoiceParser responseWrapper=new CS_SAPSFInvoiceParser();
        try{
            if(String.isNotBlank(accountRecordId))
            {
                responseWrapper.isSuccess = true;
                Account accountDetails=new Account();
                accountDetails = [SELECT CS_Partner_Function__c, CS_SAP_Customer_Id__c, ParentId, Parent.CS_Partner_Function__c, 
                                  Parent.CS_SAP_Customer_Id__c 
                                  from Account 
                                  where Id =: accountRecordId];
                if(accountDetails != null){
                    if(accountDetails.CS_Partner_Function__c.Contains(CS_ConstantClass.ACCOUNT_SOLD_TO)){
                        sapCustomerId=accountDetails.CS_SAP_Customer_Id__c;                        
                    }
                    else if(accountDetails.CS_Partner_Function__c.Contains(CS_ConstantClass.ACCOUNT_SHIP_TO)){
                        if(accountDetails.ParentId!=null){
                            sapCustomerId=accountDetails.Parent.CS_SAP_Customer_Id__c;    
                        }                                                
                    }
                    if(String.isNotBlank(sapCustomerId)){
                        responseWrapper.isSuccess=true;
                        responseWrapper = CS_SAPSFInvoiceResponseData.getInvoiceItems(sapCustomerId); 
                        System.debug(responseWrapper);
                    }
                    else{
                        responseWrapper.errorMessage = Label.CS_NullSAPId;
                        responseWrapper.isSuccess = false;  
                    }
                } //If account details != null END
                else{ //If account details = null
                    responseWrapper.isSuccess = false;
                    responseWrapper.errorMessage = Label.CS_AccountNotFound;
                    
                }                
            }
            else if(String.isBlank(accountRecordId)){
                
                responseWrapper.isSuccess=false;
                responseWrapper.errorMessage = Label.CS_AccountIDNull;  
                
            }
        }
        catch(Exception ex){
            responseWrapper.isSuccess=false;
            responseWrapper.errorMessage = Label.CS_SAPException;  
            ExceptionLogHandler.createErrorRecord(ex, 'CS_InvoiceLwcController');
        }
        
        return responseWrapper;        
    }        
}