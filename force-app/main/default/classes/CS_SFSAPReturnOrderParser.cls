public class CS_SFSAPReturnOrderParser {
    public CustomerDetails CustomerDetails;
    public OrderDetails OrderDetails;
    public CaseDetails CaseDetails;
    public OrderLineItems OrderLineItems;
    
    public class CustomerDetails {
        public String CustomerID;
    }
    
    public class OrderDetails {
        public String Action;
        public String OrderDate;
        public String OrderStatus;
        public String ReferenceDelivery;
        public String ReferenceInvoice;
        public String RequestedExecutionDate;
        public String ReturnOrderType;
        public String ReturnReason;
        public String SAPNotes;
        public String SapOrderId;
        public String SourceSystemNotes;
        public String SourceSystemOrderId;
    }    
    
    public class CaseDetails {
        public String CountryCode;
        public String SourceSystem;
        public String CurrencyCode;
        public String Market;
        public String CreatorUserId;
        public String CaseID;
    }
        
    public class OrderLineItems {
        public List<OrderLineItemDetails> OrderLineItemDetails;
    }
    
    public class OrderLineItemDetails {
        public String DeliveryAddressId;
        public String InspectionCode;
        public String OrderLineStatus;
        public String Plant;
        public String ProductId;
        public String Quantity;
        public String ReferenceInvoiceLineId;
        public String RefundType;
        public String ReturnReasonLine;
        public String SourceSystemOrderLIneId;
        public String UnitOfMeasure;
    }
    
    public static CS_SFSAPReturnOrderParser parse(String json) {
		return (CS_SFSAPReturnOrderParser) System.JSON.deserialize(json, CS_SFSAPReturnOrderParser.class);
	}
}