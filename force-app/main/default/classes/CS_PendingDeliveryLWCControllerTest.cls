/**
*  Created Date                                   : 07-April-21
*  Created By                                     : Bajrang
*  Requirement Reference                          : To be updated
*  Description			                          : Test class for order Response Data

*****  MODIFICATION HISTORY
Version Number		Date		Last Modified By		Description/User Story Number

**/

@isTest
public class CS_PendingDeliveryLWCControllerTest{
    public Static Id RecordTypeId;
    public static FINAL String attributeName = 'SAP_All_Order_Service';
    public CS_SAPSFPendingDeliveryParser AllPendingDelivery;
    
    public static  String acc;
    public static  String acc1;
    public static  String acc2;
    public static  String acc3;
    
    public static testmethod void testResponseData(){
        Integer i;
        List<Account> accountInsert = new List<Account>();
        accountInsert = CS_TestUtility.createAccount(); 
        
        for(i=0; i<accountInsert.size(); i++){
            if(accountInsert[i].CS_Partner_Function__c=='Sold To' && accountInsert[i].CS_SAP_Customer_Id__c!=' ' ){
                acc1=accountInsert[i].Id;
                
            }
            if(accountInsert[i].CS_Partner_Function__c=='Ship To'){
                acc2=accountInsert[i].Id;  
                
            }
            if(accountInsert[i].CS_SAP_Customer_Id__c==' '){
                acc3=accountInsert[i].Id;  
                
            }
        }
        
        
        CS_Integration_Attributes__mdt integrationTest = new CS_Integration_Attributes__mdt();
        integrationTest = CS_TestUtility.fetchIntegrationAttributeRecord(attributeName);
        Test.startTest();
        String body = '{ ""d"": { ""results"": [  "CreationDate": "/Date(1530403200000)/", "DeliveryBlockReason": "", "HeaderBillingBlockReason": " ", "OverallSDProcessStatus": "C", "PurchaseOrderByCustomer": "1822003766", "RequestedDeliveryDate": "/Date(1530144000000)/", "SalesOrder": "1001005385", "SalesOrderType": "ZOR", "TotalNetAmount": "0.00" ] } }';
     
        CS_MockHttpResponseGenerator mock = new CS_MockHttpResponseGenerator(body,200,'OK');
        Test.setMock(HttpCalloutMock.class, mock);
        CS_SAPSFPendingDeliveryParser AllPendingDelivery=new CS_SAPSFPendingDeliveryParser();
        CS_PendingDelivery_LwcController.getDeliveryRecordDetails(acc1);
        AllPendingDelivery.isSuccess=true;
        CS_PendingDelivery_LwcController.getDeliveryRecordDetails(acc2);
        CS_PendingDelivery_LwcController.getDeliveryRecordDetails(acc3);
        CS_PendingDelivery_LwcController.getDeliveryRecordDetails('');
        
        
        Test.stopTest();
        
       System.assertEquals(true, AllPendingDelivery.isSuccess, 'Getting response from SAP');
    }
        
    }