/**
*  Created Date                                   : 14-Apr-21
*  Created By                                     : Spoorthi B R
*  Requirement Reference                          : CCM - 793
*  Description			                          : Merge orders and return orders to display on Order's Tab in Account 360 view.

*****  MODIFICATION HISTORY
Version Number		Date		Last Modified By		Description/User Story Number

**/
public class CS_MergeOrder_LWCController {
/**
*  Created Date            : 14-apr-2021
*  Created By              : Spoorthi B R
*  Requirement Reference   : 
*  Description Of Method   : Merge Order and Return order callouts
**/
    @AuraEnabled(cacheable=true)      
    public static CS_OrderParser getAccountDetails(String RecordId) { 
        CS_SAPSFReturnOrdersParser returnWrap = new CS_SAPSFReturnOrdersParser();
        CS_OrderParser orderWrap = new CS_OrderParser();
        try{ 
            if(String.isNotBlank(RecordId)) {       
                returnWrap = CS_ReturnOrder_LwcController.getAccountDetails(RecordId);
                orderWrap = CS_OrderController.getAccountDetails(RecordId);  
                
                if(orderWrap != NULL && returnWrap != NULL){   
                    if(orderWrap.isSuccess == True && returnWrap.isSuccess == True ) {             
                        for(CS_SAPSFReturnOrdersParser.ReturnOrderResults forwrap : returnWrap.d.results){   
                            CS_OrderParser.OrderList oWrap = new CS_OrderParser.OrderList();
                            oWrap.createdByUser = forwrap.createdByUser;
                            oWrap.creationDate = forwrap.creationDate;
                            oWrap.formattedDeliveryBlockReason = forwrap.formattedDeliveryBlockReason;
                            oWrap.formattedCreationDate = forwrap.formattedCreationDate;
                            oWrap.formattedDeliveryDate = forwrap.formattedDeliveryDate;
                            oWrap.formattedHeaderBillingBlockReason = forwrap.formattedHeaderBillingBlockReason;
                            oWrap.purchaseOrderByCustomer = forwrap.purchaseOrderByCustomer;
                            oWrap.requestedDeliveryDate = forwrap.requestedDeliveryDate;
                            oWrap.formattedSalesOrderType = forwrap.formattedCustomerReturnType;
                            oWrap.totalNetAmount = forwrap.totalNetAmount;
                            oWrap.formattedOverallSDProcessStatus = forwrap.formattedOverallSDProcessStatus;
                            oWrap.salesOrder = forwrap.customerReturn;
                            orderWrap.d.results.add(oWrap);                 
                        }
                        orderWrap.checkForSuccess = True;            
                    } 
                    
                    if(orderWrap.isSuccess == True && returnWrap.isSuccess == False ) { 
                        orderWrap.checkForError = Label.CS_Return_orders + returnWrap.errorMessage;   
                        orderWrap.checkForSuccess = True;
                    } 
                    
                    if(orderWrap.isSuccess == False && returnWrap.isSuccess == True ) {            
                        CS_OrderParser.OrderItem orderItemWrap = new CS_OrderParser.OrderItem();
                        List< CS_OrderParser.OrderList> orderListWrap = new List<CS_OrderParser.OrderList>();          
                        for(CS_SAPSFReturnOrdersParser.ReturnOrderResults forwrap : returnWrap.d.results){
                            CS_OrderParser.OrderList oWrap = new CS_OrderParser.OrderList();
                            oWrap.createdByUser = forwrap.createdByUser;
                            oWrap.creationDate = forwrap.creationDate;                           
                            oWrap.formattedDeliveryBlockReason = forwrap.formattedDeliveryBlockReason;
                            oWrap.formattedCreationDate = forwrap.formattedCreationDate;
                            oWrap.formattedDeliveryDate = forwrap.formattedDeliveryDate;
                            oWrap.formattedHeaderBillingBlockReason = forwrap.formattedHeaderBillingBlockReason;
                            oWrap.purchaseOrderByCustomer = forwrap.purchaseOrderByCustomer;
                            oWrap.requestedDeliveryDate = forwrap.requestedDeliveryDate;
                            oWrap.formattedSalesOrderType = forwrap.formattedCustomerReturnType;
                            oWrap.totalNetAmount = forwrap.totalNetAmount;
                            oWrap.formattedOverallSDProcessStatus = forwrap.formattedOverallSDProcessStatus;
                            oWrap.salesOrder = forwrap.formattedCustomerReturnType;   
                            orderListWrap.add(oWrap);               
                        }
                        orderItemWrap.results = orderListWrap;           
                        orderWrap.d = orderItemWrap;
                        orderWrap.checkForError = Label.CS_Orders + orderWrap.errorMessage;     
                        orderWrap.checkForSuccess = True;
                    }  
                    
                    if(orderWrap.isSuccess == False && returnWrap.isSuccess == False ) { 
                        orderWrap.checkForError = Label.CS_Orders + orderWrap.errorMessage+ '.';
                        orderWrap.checkForReturnError = Label.CS_Return_orders + returnWrap.errorMessage + '.';              
                        orderWrap.checkForSuccess = False;  
                    }  
                } 
            } else if(String.isBlank(RecordId)) {
                orderWrap.checkForSuccess=false;
                orderWrap.checkForError = Label.CS_AccountIDNull; 
            }              
        } catch(exception ex) {
            orderWrap.checkForSuccess = false;
            orderWrap.checkForError = Label.CS_SAPException; 
            ExceptionLogHandler.createErrorRecord(ex, 'CS_MergeOrder_LWCController');
        }
        return orderWrap;          
    }    
    
}