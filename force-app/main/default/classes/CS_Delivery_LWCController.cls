/**
*  Created Date                                   : 06-Apr-21
*  Created By                                     : Harshita Gupta
*  Requirement Reference                          :  
*  Description Of Method                          : Delievery LWC Controller

*****  MODIFICATION HISTORY
Version Number		Date		Last Modified By		Description/User Story Number
01                 06-April-2021                            CCM-635                     
**/

public class CS_Delivery_LWCController {
    /**
	*  Created Date                                   : 06-Apr-21
	*  Created By                                     : Harshita Gupta
	*  Requirement Reference                          : CCM-635     
	*  Description Of Method                          : To get the SAP Account details form salesforce
*/
    @AuraEnabled(cacheable=true)    
    public static CS_SAPSFDeliveryParser getAccDetails(String RecordId) {        
        String SapCustomerId;
        CS_SAPSFDeliveryParser responseWrapper = new CS_SAPSFDeliveryParser();
        try{
            if(String.isNotBlank(RecordId)){
                Account accountDetails=new Account();
                accountDetails = [SELECT CS_Partner_Function__c, CS_SAP_Customer_Id__c, ParentId, Parent.CS_Partner_Function__c, 
                                  Parent.CS_SAP_Customer_Id__c 
                                  from Account 
                                  where Id =: RecordId];
                if(accountDetails != null){
                    if(accountDetails.CS_Partner_Function__c.Contains(CS_ConstantClass.ACCOUNT_SOLD_TO)){
                        SapCustomerId=accountDetails.CS_SAP_Customer_Id__c;                        
                    }
                    else if(accountDetails.CS_Partner_Function__c.Contains(CS_ConstantClass.ACCOUNT_SHIP_TO)){
                        if(accountDetails.ParentId!=null){
                            SapCustomerId=accountDetails.Parent.CS_SAP_Customer_Id__c;    
                        }                                                
                    }
                    if(String.isNotBlank(SapCustomerId)){
                        responseWrapper.isSuccess=true;
                        responseWrapper = CS_SAPSFDeliveryResponse.getDeliveryItems(SapCustomerId);
                        System.debug('Wrapper Response  '+responseWrapper);
                    }
                    else{
                        responseWrapper.errorMessage = Label.CS_NullSAPId;
                        responseWrapper.isSuccess = false;
                        System.debug(responseWrapper.errorMessage);
                    } 
                }
                 
                //If account details = null
                else{ 
                    responseWrapper.isSuccess = false;
                    responseWrapper.errorMessage = Label.CS_AccountNotFound;                     
                }
                System.debug('Sap Customer Id--->'+SapCustomerId);
            }
            else{
                responseWrapper.isSuccess=false;
                responseWrapper.errorMessage = Label.CS_AccountIDNull;                
            }
        }
        catch(Exception ex){
            
            responseWrapper.isSuccess=false;
            responseWrapper.errorMessage = Label.CS_SAPException;  
            ExceptionLogHandler.createErrorRecord(ex, 'CS_Delivery_LwcController');
        }
        return responseWrapper;
    }
}