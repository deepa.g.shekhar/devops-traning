/**
*  Created Date                                   : 20-Apr-21
*  Created By                                     : Spoorthi B R
*  Requirement Reference                          : CCM - 365
*  Description			                          : This interface is used to update CCM Case After Acknowledge GR via stock mov.
													(Advance Return Delivery ZLRA). Corresponds to S&D - 01

*****  MODIFICATION HISTORY
Version Number		Date		Last Modified By		Description/User Story Number

**/
public class CS_ShippingNotificationTriggerHandler implements CS_TriggerInterface {
    public void beforeInsert(List<SObject> newRecordList) {
        
    }
/**
*  Created Date            : 20-Apr-2021
*  Created By              : Spoorthi B R
*  Requirement Reference   : CCM - 365
*  Description Of Method   : Pass the response from iPaaS to CS_ShippingNotificationTriggerHelper method.
**/ 
    public static void afterInsert(List<SObject> newRecordList,Map<Id, SObject> newRecordsMap) {
        List<CS_Shipping_Notification_PE__e> shipNotification = New List<CS_Shipping_Notification_PE__e>();
        shipNotification =  (List<CS_Shipping_Notification_PE__e>)newRecordList;        
        if(shipNotification != NULL) {
            CS_ShippingNotificationTriggerHelper.getEventData(shipNotification);
        }
    }
    public void beforeUpdate(Map<Id, SObject> newRecordsMap, Map<Id, SObject> oldRecordsMap) {
        
    }
    public void afterUpdate(Map<Id, SObject> newRecordsMap, Map<Id, SObject> oldRecordsMap) {
        
    }
    public void beforeDelete(List<SObject> oldRecordList,Map<Id, SObject> oldRecordsMap) {
        
    }
    public void afterDelete(Map<Id, SObject> oldRecordsMap) {
        
    }
    public void afterUndelete(List<SObject> newRecordList,Map<Id, SObject> newRecordsMap) {
        
    }   
}