/**
*  Created Date                                   : 05-April-2021 
*  Created By                                     : Narmada
*  Requirement Reference                          : CCM 44
*  Description Of Method                          : Test class for CS_InvoiceWrapper class
**/
@IsTest
public class CS_InvoiceWrapperTest {
	
	static testMethod void testParse() {
		String json = '{'+
		'  \"d\": {'+
		'    \"results\": ['+
		'      {'+
		'        \"BillingDocument\": \"string\",'+
		'        \"SDDocumentCategory\": \"string\",'+
		'        \"BillingDocumentCategory\": \"string\",'+
		'        \"BillingDocumentType\": \"string\",'+
		'        \"CreationDate\": \"/Date(1492041600000)/\",'+
		'        \"CreationTime\": \"PT15H51M04S\",'+
		'        \"LastChangeDate\": \"/Date(1492041600000)/\",'+
		'        \"LastChangeDateTime\": \"/Date(1492098664000)/\",'+
		'        \"LogicalSystem\": \"string\",'+
		'        \"SalesOrganization\": \"string\",'+
		'        \"DistributionChannel\": \"string\",'+
		'        \"Division\": \"string\",'+
		'        \"BillingDocumentDate\": \"/Date(1492041600000)/\",'+
		'        \"BillingDocumentIsCancelled\": true,'+
		'        \"CancelledBillingDocument\": \"string\",'+
		'        \"ForeignTrade\": \"string\",'+
		'        \"IsExportDelivery\": \"string\",'+
		'        \"BillingDocCombinationCriteria\": \"string\",'+
		'        \"ManualInvoiceMaintIsRelevant\": true,'+
		'        \"IsIntrastatReportingRelevant\": true,'+
		'        \"IsIntrastatReportingExcluded\": true,'+
		'        \"TotalNetAmount\": \"0\",'+
		'        \"TransactionCurrency\": \"string\",'+
		'        \"StatisticsCurrency\": \"string\",'+
		'        \"TaxAmount\": \"0\",'+
		'        \"TotalGrossAmount\": \"0\",'+
		'        \"CustomerPriceGroup\": \"string\",'+
		'        \"PriceListType\": \"string\",'+
		'        \"TaxDepartureCountry\": \"string\",'+
		'        \"VATRegistration\": \"string\",'+
		'        \"VATRegistrationOrigin\": \"string\",'+
		'        \"VATRegistrationCountry\": \"string\",'+
		'        \"HierarchyTypePricing\": \"string\",'+
		'        \"CustomerTaxClassification1\": \"string\",'+
		'        \"CustomerTaxClassification2\": \"string\",'+
		'        \"CustomerTaxClassification3\": \"string\",'+
		'        \"CustomerTaxClassification4\": \"string\",'+
		'        \"CustomerTaxClassification5\": \"string\",'+
		'        \"CustomerTaxClassification6\": \"string\",'+
		'        \"CustomerTaxClassification7\": \"string\",'+
		'        \"CustomerTaxClassification8\": \"string\",'+
		'        \"CustomerTaxClassification9\": \"string\",'+
		'        \"IsEUTriangularDeal\": true,'+
		'        \"SDPricingProcedure\": \"string\",'+
		'        \"ShippingCondition\": \"string\",'+
		'        \"IncotermsVersion\": \"string\",'+
		'        \"IncotermsClassification\": \"string\",'+
		'        \"IncotermsTransferLocation\": \"string\",'+
		'        \"IncotermsLocation1\": \"string\",'+
		'        \"IncotermsLocation2\": \"string\",'+
		'        \"PayerParty\": \"string\",'+
		'        \"ContractAccount\": \"string\",'+
		'        \"CustomerPaymentTerms\": \"string\",'+
		'        \"PaymentMethod\": \"string\",'+
		'        \"PaymentReference\": \"string\",'+
		'        \"FixedValueDate\": \"/Date(1492041600000)/\",'+
		'        \"AdditionalValueDays\": \"string\",'+
		'        \"SEPAMandate\": \"string\",'+
		'        \"CompanyCode\": \"string\",'+
		'        \"FiscalYear\": \"string\",'+
		'        \"AccountingDocument\": \"string\",'+
		'        \"CustomerAccountAssignmentGroup\": \"string\",'+
		'        \"AccountingExchangeRateIsSet\": true,'+
		'        \"AbsltAccountingExchangeRate\": \"0\",'+
		'        \"AcctgExchangeRateIsIndrctQtan\": true,'+
		'        \"ExchangeRateDate\": \"/Date(1492041600000)/\",'+
		'        \"ExchangeRateType\": \"string\",'+
		'        \"DocumentReferenceID\": \"string\",'+
		'        \"AssignmentReference\": \"string\",'+
		'        \"DunningArea\": \"string\",'+
		'        \"DunningBlockingReason\": \"string\",'+
		'        \"DunningKey\": \"string\",'+
		'        \"InternalFinancialDocument\": \"string\",'+
		'        \"IsRelevantForAccrual\": true,'+
		'        \"SoldToParty\": \"string\",'+
		'        \"PartnerCompany\": \"string\",'+
		'        \"PurchaseOrderByCustomer\": \"string\",'+
		'        \"CustomerGroup\": \"string\",'+
		'        \"Country\": \"string\",'+
		'        \"CityCode\": \"string\",'+
		'        \"SalesDistrict\": \"string\",'+
		'        \"Region\": \"string\",'+
		'        \"County\": \"string\",'+
		'        \"CreditControlArea\": \"string\",'+
		'        \"CustomerRebateAgreement\": \"string\",'+
		'        \"SalesDocumentCondition\": \"string\",'+
		'        \"OverallSDProcessStatus\": \"string\",'+
		'        \"OverallBillingStatus\": \"string\",'+
		'        \"AccountingPostingStatus\": \"string\",'+
		'        \"AccountingTransferStatus\": \"string\",'+
		'        \"BillingIssueType\": \"string\",'+
		'        \"InvoiceListStatus\": \"string\",'+
		'        \"OvrlItmGeneralIncompletionSts\": \"string\",'+
		'        \"OverallPricingIncompletionSts\": \"string\",'+
		'        \"InvoiceClearingStatus\": \"string\",'+
		'        \"BillingDocumentListType\": \"string\",'+
		'        \"BillingDocumentListDate\": \"/Date(1492041600000)/\",'+
		'        \"to_Item\": {'+
		'          \"results\": ['+
		'            {'+
		'              \"BillingDocument\": \"string\",'+
		'              \"BillingDocumentItem\": \"string\",'+
		'              \"SalesDocumentItemCategory\": \"string\",'+
		'              \"SalesDocumentItemType\": \"string\",'+
		'              \"ReturnItemProcessingType\": \"string\",'+
		'              \"CreatedByUser\": \"string\",'+
		'              \"CreationDate\": \"/Date(1492041600000)/\",'+
		'              \"CreationTime\": \"PT15H51M04S\",'+
		'              \"ReferenceLogicalSystem\": \"string\",'+
		'              \"OrganizationDivision\": \"string\",'+
		'              \"Division\": \"string\",'+
		'              \"SalesOffice\": \"string\",'+
		'              \"Material\": \"string\",'+
		'              \"OriginallyRequestedMaterial\": \"string\",'+
		'              \"InternationalArticleNumber\": \"string\",'+
		'              \"PricingReferenceMaterial\": \"string\",'+
		'              \"Batch\": \"string\",'+
		'              \"ProductHierarchyNode\": \"string\",'+
		'              \"MaterialGroup\": \"string\",'+
		'              \"AdditionalMaterialGroup1\": \"string\",'+
		'              \"AdditionalMaterialGroup2\": \"string\",'+
		'              \"AdditionalMaterialGroup3\": \"string\",'+
		'              \"AdditionalMaterialGroup4\": \"string\",'+
		'              \"AdditionalMaterialGroup5\": \"string\",'+
		'              \"MaterialCommissionGroup\": \"string\",'+
		'              \"Plant\": \"string\",'+
		'              \"StorageLocation\": \"string\",'+
		'              \"ReplacementPartType\": \"string\",'+
		'              \"MaterialGroupHierarchy1\": \"string\",'+
		'              \"MaterialGroupHierarchy2\": \"string\",'+
		'              \"PlantRegion\": \"string\",'+
		'              \"PlantCounty\": \"string\",'+
		'              \"PlantCity\": \"string\",'+
		'              \"BOMExplosion\": \"string\",'+
		'              \"MaterialDeterminationType\": \"string\",'+
		'              \"BillingDocumentItemText\": \"string\",'+
		'              \"ServicesRenderedDate\": \"/Date(1492041600000)/\",'+
		'              \"BillingQuantity\": \"0\",'+
		'              \"BillingQuantityUnit\": \"string\",'+
		'              \"BillingQuantityInBaseUnit\": \"0\",'+
		'              \"BaseUnit\": \"string\",'+
		'              \"MRPRequiredQuantityInBaseUnit\": \"0\",'+
		'              \"BillingToBaseQuantityDnmntr\": \"0\",'+
		'              \"BillingToBaseQuantityNmrtr\": \"0\",'+
		'              \"ItemGrossWeight\": \"0\",'+
		'              \"ItemNetWeight\": \"0\",'+
		'              \"ItemWeightUnit\": \"string\",'+
		'              \"ItemVolume\": \"0\",'+
		'              \"ItemVolumeUnit\": \"string\",'+
		'              \"BillToPartyCountry\": \"string\",'+
		'              \"BillToPartyRegion\": \"string\",'+
		'              \"BillingPlanRule\": \"string\",'+
		'              \"BillingPlan\": \"string\",'+
		'              \"BillingPlanItem\": \"string\",'+
		'              \"NetAmount\": \"0\",'+
		'              \"TransactionCurrency\": \"string\",'+
		'              \"GrossAmount\": \"0\",'+
		'              \"PricingDate\": \"/Date(1492041600000)/\",'+
		'              \"AbsltPriceDetnExchangeRate\": \"0\",'+
		'              \"PriceDetnExchRateIsIndrctQtan\": true,'+
		'              \"PriceDetnExchangeRateDate\": \"/Date(1492041600000)/\",'+
		'              \"PricingScaleQuantityInBaseUnit\": \"0\",'+
		'              \"TaxAmount\": \"0\",'+
		'              \"CostAmount\": \"0\",'+
		'              \"Subtotal1Amount\": \"0\",'+
		'              \"Subtotal2Amount\": \"0\",'+
		'              \"Subtotal3Amount\": \"0\",'+
		'              \"Subtotal4Amount\": \"0\",'+
		'              \"Subtotal5Amount\": \"0\",'+
		'              \"Subtotal6Amount\": \"0\",'+
		'              \"StatisticalValueControl\": \"string\",'+
		'              \"CashDiscountIsDeductible\": true,'+
		'              \"CustomerConditionGroup1\": \"string\",'+
		'              \"CustomerConditionGroup2\": \"string\",'+
		'              \"CustomerConditionGroup3\": \"string\",'+
		'              \"CustomerConditionGroup4\": \"string\",'+
		'              \"CustomerConditionGroup5\": \"string\",'+
		'              \"ManualPriceChangeType\": \"string\",'+
		'              \"MaterialPricingGroup\": \"string\",'+
		'              \"AbsltStatisticsExchangeRate\": \"0\",'+
		'              \"StatisticsExchRateIsIndrctQtan\": true,'+
		'              \"MainItemPricingRefMaterial\": \"string\",'+
		'              \"MainItemMaterialPricingGroup\": \"string\",'+
		'              \"DepartureCountry\": \"string\",'+
		'              \"TaxJurisdiction\": \"string\",'+
		'              \"ProductTaxClassification1\": \"string\",'+
		'              \"ProductTaxClassification2\": \"string\",'+
		'              \"ProductTaxClassification3\": \"string\",'+
		'              \"ProductTaxClassification4\": \"string\",'+
		'              \"ProductTaxClassification5\": \"string\",'+
		'              \"ProductTaxClassification6\": \"string\",'+
		'              \"ProductTaxClassification7\": \"string\",'+
		'              \"ProductTaxClassification8\": \"string\",'+
		'              \"ProductTaxClassification9\": \"string\",'+
		'              \"ZeroVATRsn\": \"string\",'+
		'              \"EligibleAmountForCashDiscount\": \"0\",'+
		'              \"BusinessArea\": \"string\",'+
		'              \"ProfitCenter\": \"string\",'+
		'              \"WBSElement\": \"string\",'+
		'              \"ControllingArea\": \"string\",'+
		'              \"ProfitabilitySegment\": \"string\",'+
		'              \"OrderID\": \"string\",'+
		'              \"CostCenter\": \"string\",'+
		'              \"OriginSDDocument\": \"string\",'+
		'              \"OriginSDDocumentItem\": \"string\",'+
		'              \"MatlAccountAssignmentGroup\": \"string\",'+
		'              \"ReferenceSDDocument\": \"string\",'+
		'              \"ReferenceSDDocumentItem\": \"string\",'+
		'              \"ReferenceSDDocumentCategory\": \"string\",'+
		'              \"SalesDocument\": \"string\",'+
		'              \"SalesDocumentItem\": \"string\",'+
		'              \"SalesSDDocumentCategory\": \"string\",'+
		'              \"HigherLevelItem\": \"string\",'+
		'              \"BillingDocumentItemInPartSgmt\": \"string\",'+
		'              \"ExternalReferenceDocument\": \"string\",'+
		'              \"ExternalReferenceDocumentItem\": \"string\",'+
		'              \"SalesGroup\": \"string\",'+
		'              \"AdditionalCustomerGroup1\": \"string\",'+
		'              \"AdditionalCustomerGroup2\": \"string\",'+
		'              \"AdditionalCustomerGroup3\": \"string\",'+
		'              \"AdditionalCustomerGroup4\": \"string\",'+
		'              \"AdditionalCustomerGroup5\": \"string\",'+
		'              \"SDDocumentReason\": \"string\",'+
		'              \"RetailPromotion\": \"string\",'+
		'              \"RebateBasisAmount\": \"0\",'+
		'              \"VolumeRebateGroup\": \"string\",'+
		'              \"ItemIsRelevantForCredit\": true,'+
		'              \"CreditRelatedPrice\": \"0\",'+
		'              \"SalesDeal\": \"string\",'+
		'              \"SalesPromotion\": \"string\",'+
		'              \"SalesOrderSalesDistrict\": \"string\",'+
		'              \"SalesOrderCustomerGroup\": \"string\",'+
		'              \"SalesOrderCustomerPriceGroup\": \"string\",'+
		'              \"SalesOrderPriceListType\": \"string\",'+
		'              \"SalesOrderSalesOrganization\": \"string\",'+
		'              \"SalesOrderDistributionChannel\": \"string\",'+
		'              \"SalesDocIsCreatedFromReference\": true,'+
		'              \"ShippingPoint\": \"string\",'+
		'              \"HigherLevelItemUsage\": \"string\",'+
		'              \"to_ItemText\": {'+
		'                \"results\": ['+
		'                  {'+
		'                    \"BillingDocument\": \"string\",'+
		'                    \"BillingDocumentItem\": \"string\",'+
		'                    \"Language\": \"string\",'+
		'                    \"LongTextID\": \"string\",'+
		'                    \"LongText\": \"string\"'+
		'                  }'+
		'                ]'+
		'              },'+
		'              \"to_Partner\": {'+
		'                \"results\": ['+
		'                  {'+
		'                    \"BillingDocument\": \"string\",'+
		'                    \"BillingDocumentItem\": \"string\",'+
		'                    \"PartnerFunction\": \"string\",'+
		'                    \"Customer\": \"string\",'+
		'                    \"Supplier\": \"string\",'+
		'                    \"Personnel\": \"string\",'+
		'                    \"ContactPerson\": \"string\"'+
		'                  }'+
		'                ]'+
		'              },'+
		'              \"to_PricingElement\": {'+
		'                \"results\": ['+
		'                  {'+
		'                    \"BillingDocument\": \"string\",'+
		'                    \"BillingDocumentItem\": \"string\",'+
		'                    \"PricingProcedureStep\": \"string\",'+
		'                    \"PricingProcedureCounter\": \"string\",'+
		'                    \"ConditionType\": \"string\",'+
		'                    \"PricingDateTime\": \"string\",'+
		'                    \"ConditionCalculationType\": \"string\",'+
		'                    \"ConditionBaseValue\": \"0\",'+
		'                    \"ConditionRateValue\": \"0\",'+
		'                    \"ConditionCurrency\": \"string\",'+
		'                    \"ConditionQuantity\": \"0\",'+
		'                    \"ConditionQuantityUnit\": \"string\",'+
		'                    \"ConditionCategory\": \"string\",'+
		'                    \"ConditionIsForStatistics\": true,'+
		'                    \"PricingScaleType\": \"string\",'+
		'                    \"IsRelevantForAccrual\": true,'+
		'                    \"CndnIsRelevantForInvoiceList\": \"string\",'+
		'                    \"ConditionOrigin\": \"string\",'+
		'                    \"IsGroupCondition\": \"string\",'+
		'                    \"ConditionRecord\": \"string\",'+
		'                    \"ConditionSequentialNumber\": \"string\",'+
		'                    \"TaxCode\": \"string\",'+
		'                    \"WithholdingTaxCode\": \"string\",'+
		'                    \"CndnRoundingOffDiffAmount\": \"0\",'+
		'                    \"ConditionAmount\": \"0\",'+
		'                    \"TransactionCurrency\": \"string\",'+
		'                    \"ConditionControl\": \"string\",'+
		'                    \"ConditionInactiveReason\": \"string\",'+
		'                    \"ConditionClass\": \"string\",'+
		'                    \"PrcgProcedureCounterForHeader\": \"string\",'+
		'                    \"FactorForConditionBasisValue\": 3.14,'+
		'                    \"StructureCondition\": \"string\",'+
		'                    \"PeriodFactorForCndnBasisValue\": 3.14,'+
		'                    \"PricingScaleBasis\": \"string\",'+
		'                    \"ConditionScaleBasisValue\": \"0\",'+
		'                    \"ConditionScaleBasisUnit\": \"string\",'+
		'                    \"ConditionScaleBasisCurrency\": \"string\",'+
		'                    \"CndnIsRelevantForIntcoBilling\": true,'+
		'                    \"ConditionIsManuallyChanged\": true,'+
		'                    \"ConditionIsForConfiguration\": true,'+
		'                    \"VariantCondition\": \"string\"'+
		'                  }'+
		'                ]'+
		'              }'+
		'            }'+
		'          ]'+
		'        },'+
		'        \"to_Partner\": {'+
		'          \"results\": ['+
		'            {'+
		'              \"BillingDocument\": \"string\",'+
		'              \"PartnerFunction\": \"string\",'+
		'              \"Customer\": \"string\",'+
		'              \"Supplier\": \"string\",'+
		'              \"Personnel\": \"string\",'+
		'              \"ContactPerson\": \"string\"'+
		'            }'+
		'          ]'+
		'        },'+
		'        \"to_PricingElement\": {'+
		'          \"results\": ['+
		'            {'+
		'              \"BillingDocument\": \"string\",'+
		'              \"PricingProcedureStep\": \"string\",'+
		'              \"PricingProcedureCounter\": \"string\",'+
		'              \"ConditionType\": \"string\",'+
		'              \"PricingDateTime\": \"string\",'+
		'              \"ConditionCalculationType\": \"string\",'+
		'              \"ConditionBaseValue\": \"0\",'+
		'              \"ConditionRateValue\": \"0\",'+
		'              \"ConditionCurrency\": \"string\",'+
		'              \"ConditionQuantity\": \"0\",'+
		'              \"ConditionQuantityUnit\": \"string\",'+
		'              \"ConditionCategory\": \"string\",'+
		'              \"ConditionIsForStatistics\": true,'+
		'              \"PricingScaleType\": \"string\",'+
		'              \"ConditionOrigin\": \"string\",'+
		'              \"IsGroupCondition\": \"string\",'+
		'              \"ConditionRecord\": \"string\",'+
		'              \"ConditionSequentialNumber\": \"string\",'+
		'              \"TaxCode\": \"string\",'+
		'              \"WithholdingTaxCode\": \"string\",'+
		'              \"CndnRoundingOffDiffAmount\": \"0\",'+
		'              \"ConditionAmount\": \"0\",'+
		'              \"TransactionCurrency\": \"string\",'+
		'              \"ConditionControl\": \"string\",'+
		'              \"ConditionInactiveReason\": \"string\",'+
		'              \"ConditionClass\": \"string\",'+
		'              \"PrcgProcedureCounterForHeader\": \"string\",'+
		'              \"FactorForConditionBasisValue\": 3.14,'+
		'              \"StructureCondition\": \"string\",'+
		'              \"PeriodFactorForCndnBasisValue\": 3.14,'+
		'              \"PricingScaleBasis\": \"string\",'+
		'              \"ConditionScaleBasisValue\": \"0\",'+
		'              \"ConditionScaleBasisUnit\": \"string\",'+
		'              \"ConditionScaleBasisCurrency\": \"string\",'+
		'              \"CndnIsRelevantForIntcoBilling\": true,'+
		'              \"ConditionIsManuallyChanged\": true,'+
		'              \"ConditionIsForConfiguration\": true,'+
		'              \"VariantCondition\": \"string\"'+
		'            }'+
		'          ]'+
		'        },'+
		'        \"to_Text\": {'+
		'          \"results\": ['+
		'            {'+
		'              \"BillingDocument\": \"string\",'+
		'              \"Language\": \"string\",'+
		'              \"LongTextID\": \"string\",'+
		'              \"LongText\": \"string\"'+
		'            }'+
		'          ]'+
		'        }'+
		'      }'+
		'    ]'+
		'  }'+
		'}';
		CS_InvoiceWrapper obj = CS_InvoiceWrapper.parse(json);
		System.assert(obj != null);
	}
}