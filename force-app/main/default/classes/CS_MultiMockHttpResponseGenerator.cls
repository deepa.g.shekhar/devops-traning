/**
*  Created Date                                   : 14-Apr-21
*  Created By                                     : Spoorthi B R
*  Requirement Reference                          : CCM - 793
*  Description			                          : Mock Response Generator for multiple HTTP Callouts

*****  MODIFICATION HISTORY
Version Number		Date		Last Modified By		Description/User Story Number

**/
@isTest
global class CS_MultiMockHttpResponseGenerator implements HttpCalloutMock {
    Map<String, HttpCalloutMock> requests;
    public CS_MultiMockHttpResponseGenerator(Map<String, HttpCalloutMock> requests) {
        this.requests = requests;
        system.assert( this.requests != NULL);                     
     } 
    /**
    *  Created Date            : 14-apr-2021
    *  Created By              : Spoorthi B R
    *  Requirement Reference   : CCM-793
    *  Description Of Method   : Create fake response
    **/
    global HTTPResponse respond(HTTPRequest req) {
        HTTPResponse mock = new HTTPResponse();   
        HttpCalloutMock mock1 = requests.get(req.getEndpoint());
        if (mock1 != null) {        
           mock = mock1.respond(req);              
        }
        system.assertEquals(req.getMethod(), 'GET');
        return mock;        
    }
}