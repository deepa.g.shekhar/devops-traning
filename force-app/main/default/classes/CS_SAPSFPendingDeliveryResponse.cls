/**
*  Created Date                                   : 05-Apr-2021 
*  Created By                                     : Bajrang
*  Requirement Reference                          : To be updated 
*  Description Of Method                          : To get the invoice wrapper body

*****  MODIFICATION HISTORY
Version Number		Date		Last Modified By		Description/User Story Number                            
**/

public class CS_SAPSFPendingDeliveryResponse{
    
    public static String deliveryResponseBody;
    public static CS_SAPSFPendingDeliveryParser deliveryResponse;
    public static FINAL String attributeName = 'SAP_All_Order_Service';
    
    /**
*  Created Date                                   : 05-Apr-2021 
*  Created By                                     : Bajrang
*  Requirement Reference                          : To be updated 
*  Description Of Method                          : To get the getDeliveryItems

*****  MODIFICATION HISTORY
Version Number		Date		Last Modified By		Description/User Story Number                            
**/
    @AuraEnabled(cacheable=true)
    public static CS_SAPSFPendingDeliveryParser getDeliveryItems(String SapCustomerId){
        try{
            deliveryResponse = new CS_SAPSFPendingDeliveryParser();
            if(String.isNotBlank(SapCustomerId)){
                deliveryResponse.isSuccess=true;
                String actualDate = '/Date(1530403200000)/';
                Long longtime = long.valueOf( actualDate.substringBetween('(', ')') );
                DateTime formattedDateTime = DateTime.newInstance(longtime);
                HTTPCalloutFramework.HttpCalloutWrapper wrapper=new HTTPCalloutFramework.HttpCalloutWrapper();
                
                Map<String,String> mapHeader=new Map<String,String>();
                mapHeader.put('accept','application/json');
                mapHeader.put('Content-Type','application/json');
                
                Map<String,String> mapURL=new Map<String,String>();
                DateTime dt = system.now().addMonths(integer.valueOf(Label.CS_CreationDate));
                String dtString = String.valueOf(dt);
                dtString = dtString.replace(' ', 'T');
                
                String endpointURL='?$filter=SoldToParty+eq+\''+SapCustomerId+'\'&$filter=OverallSDProcessStatus+ne+\''+'C'+'\'+and+CreationDate+gt+datetime\''+dtString+'\'&$format=json&$top=100';
                
                wrapper.integrationAttributeName=attributeName;
                wrapper.mapOfHttpHeaders=mapHeader;
                wrapper.mapOfURLParameters =mapURL;
                wrapper.endPointParamString=endpointURL;
                
                
                
                HTTPResponse response=HTTPCalloutFramework.sendHTTPGETRequest(wrapper);
                
                deliveryResponseBody =response.getBody();
                if(response.getStatusCode()==200){
                    if(String.isNotBlank(response.getBody())){                        
                        deliveryResponse = CS_SAPSFPendingDeliveryParser.parse(deliveryResponseBody);
                        for(CS_SAPSFPendingDeliveryParser.PendingDelivery responseData : deliveryResponse.d.results){
                            Long longtimedate = long.valueOf( responseData.creationDate.substringBetween('(', ')') );
                            responseData.formattedCreationDate = DateTime.newInstance(longtimedate);                        }
                        
                        deliveryResponse.isSuccess = true;   
                    }
                    else{
                        deliveryResponse.isSuccess=false; 
                        deliveryResponse.errorMessage = Label.CS_NullSAPId;
                    }
                }
                else{
                    deliveryResponse.isSuccess=false;   
                    deliveryResponse.errorMessage=Label.CS_SAPException;
                }
            } 
            else {
                deliveryResponse.errorMessage =Label.CS_NullSAPId;
                deliveryResponse.isSuccess = false;
            }           
        }
        catch(Exception ex){
            ExceptionLogHandler.createErrorRecord(ex, 'CS_SAPSFPendingDeliveryResponse');
        }         
        return deliveryResponse;
    }
}