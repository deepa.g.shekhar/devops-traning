/**
*  Created Date                                   : 05-April-21
*  Created By                                     : Narmada
*  Requirement Reference                          :  
*  Description Of Method                          : To get the return orders response body

*****  MODIFICATION HISTORY
Version Number		Date		Last Modified By		Description/User Story Number

**/
public With Sharing class CS_SAPSFReturnOrderResponseData {
    
    public static FINAL String attributeName = 'SAP_Customer_Return_Order_Service';    
    public static String returnOrderResponseBody;
    public static String userLanguage = UserInfo.getLanguage();
    public static string language;
    public static String entityType = 'Order';
    public static String deliveryBlockReason = 'DeliveryBlockReason';
    public static String customerReturnType = 'CustomerReturnType';
    public static String headerBillingBlockReason = 'HeaderBillingBlockReason';
    public static String overallSDProcessStatus = 'OverallSDProcessStatus';
    public static List<CS_SAP_Picklist_Mapping__c> orderTypekeyValue = new List<CS_SAP_Picklist_Mapping__c>();
    public static Map<String,Map<String,String>> orderTypeKeyValueMap = new Map<String,Map<String,String>>();
    public static Map<String,String> subOrderTypeKeyValueMap;
    public static CS_SAPSFReturnOrdersParser returnOrderWrapperResponse;
    
    /**
*  Created Date                                   : 05-April-21
*  Created By                                     : Narmada
*  Requirement Reference                          :  
*  Description Of Method                          : To get the http response and to parse the json using wrapper and to get the wrapper body
**/
    @AuraEnabled(cacheable=true)
    public static CS_SAPSFReturnOrdersParser getReturnOrdersBySAPCustomerId(String sapCustomerId){
        try{
            returnOrderWrapperResponse=new CS_SAPSFReturnOrdersParser();            
            for(PicklistEntry value: User.LanguageLocalekey.getDescribe().getPicklistValues()) {
                if(value.getValue() == userLanguage) {
                    language = value.getValue();                    
                }                
            }  
             
            DateTime dt = system.now().addMonths(Integer.valueOf(Label.CS_Creationdate));
            String dtString = String.valueOf(dt);
            dtString = dtString.replace(' ','T');
            HTTPCalloutFramework.HttpCalloutWrapper wrapper=new HTTPCalloutFramework.HttpCalloutWrapper();
            
            Map<String,String> mapHeader=new Map<String,String>();
            mapHeader.put('accept','application/json');
            mapHeader.put('Content-Type','application/json');
            
            Map<String,String> mapURL=new Map<String,String>();
            
            String endpointURL='?$filter=SoldToParty+eq+\''+sapCustomerId+'\'+and+CreationDate+gt+datetime\''+dtString+'\'&$format=json';
            
            wrapper.integrationAttributeName=attributeName;
            wrapper.mapOfHttpHeaders=mapHeader;
            wrapper.mapOfURLParameters =mapURL;
            wrapper.endPointParamString=endpointURL;
            
            HTTPResponse response=HTTPCalloutFramework.sendHTTPGETRequest(wrapper);
            
            returnOrderResponseBody=response.getBody();
            if(response.getStatusCode() == 200){
                if(String.isNotBlank(response.getBody())){                        
                    returnOrderWrapperResponse = CS_SAPSFReturnOrdersParser.parse(returnOrderResponseBody);
                    orderTypekeyValue =[Select CS_Property_Name__c,CS_Key__c,CS_Value__c,CS_Entity__c,CS_Language__c 
                                        From CS_SAP_Picklist_Mapping__c 
                                        where CS_Entity__c =: entityType 
                                        AND CS_Language__c =: language];
                    
                    for (CS_SAP_Picklist_Mapping__c kVal : orderTypekeyValue ){
                        subOrderTypeKeyValueMap=new Map<String,String>();
                        if(orderTypeKeyValueMap.keySet().contains(kVal.CS_Property_Name__c)){                            
                            subOrderTypeKeyValueMap = orderTypeKeyValueMap.get(kVal.CS_Property_Name__c);
                            subOrderTypeKeyValueMap.put(kVal.CS_Key__c,kVal.CS_Value__c);
                            orderTypeKeyValueMap.put(kVal.CS_Property_Name__c,subOrderTypeKeyValueMap);
                        }
                        else{
                            subOrderTypeKeyValueMap.put(kVal.CS_Key__c,kVal.CS_Value__c);
                            orderTypeKeyValueMap.put(kVal.CS_Property_Name__c,subOrderTypeKeyValueMap); 
                        }                        
                    }
                    
                    for(CS_SAPSFReturnOrdersParser.ReturnOrderResults result:returnOrderWrapperResponse.d.results){
                        if(String.isNotBlank(result.creationDate)) { 
                            Long longtimeCreatedDate = long.valueOf( result.creationDate.substringBetween('(', ')') );
                            result.formattedCreationDate = DateTime.newInstance(longtimeCreatedDate);
                           
                        }
                        if(String.isNotBlank(result.requestedDeliveryDate)) {
                            Long longtimeDeliveryDate = long.valueOf( result.requestedDeliveryDate.substringBetween('(', ')') );
                            result.formattedDeliveryDate = DateTime.newInstance(longtimeDeliveryDate); 
                        }
                        if(String.isNotBlank(result.deliveryBlockReason)){
                            if(orderTypeKeyValueMap.keySet().contains(deliveryBlockReason)){                                
                                subOrderTypeKeyValueMap = orderTypeKeyValueMap.get(deliveryBlockReason);
                                if(subOrderTypeKeyValueMap.containsKey(result.deliveryBlockReason)){                                    
                                    result.formattedDeliveryBlockReason=result.deliveryBlockReason +' - '+ subOrderTypeKeyValueMap.get(result.deliveryBlockReason);                                    
                                }
                            }
                        }
                        
                        if(String.isNotBlank(result.customerReturnType)){
                            if(orderTypeKeyValueMap.keySet().contains(customerReturnType)){                                
                                subOrderTypeKeyValueMap = orderTypeKeyValueMap.get(customerReturnType);
                                if(subOrderTypeKeyValueMap.containsKey(result.customerReturnType)){                                    
                                    result.formattedCustomerReturnType=result.customerReturnType +' - '+ subOrderTypeKeyValueMap.get(result.customerReturnType);                                    
                                }
                            }
                        }
                        if(String.isNotBlank(result.headerBillingBlockReason)){
                            if(orderTypeKeyValueMap.keySet().contains(headerBillingBlockReason)){                                
                                subOrderTypeKeyValueMap = orderTypeKeyValueMap.get(headerBillingBlockReason);
                                if(subOrderTypeKeyValueMap.containsKey(result.headerBillingBlockReason)){                                    
                                    result.formattedHeaderBillingBlockReason=result.headerBillingBlockReason +' - '+ subOrderTypeKeyValueMap.get(result.headerBillingBlockReason);                                    
                                }
                            }
                        }
                        if(String.isNotBlank(result.overallSDProcessStatus)){
                            if(orderTypeKeyValueMap.keySet().contains(overallSDProcessStatus)){                                
                                subOrderTypeKeyValueMap = orderTypeKeyValueMap.get(overallSDProcessStatus);
                                if(subOrderTypeKeyValueMap.containsKey(result.overallSDProcessStatus)){                                    
                                    result.formattedOverallSDProcessStatus=result.overallSDProcessStatus +' - '+ subOrderTypeKeyValueMap.get(result.overallSDProcessStatus);                                    
                                }
                            }
                        }                        
                    }
                    
                    returnOrderWrapperResponse.isSuccess = true;   
                }
                else{
                    returnOrderWrapperResponse.isSuccess=false; 
                    returnOrderWrapperResponse.errorMessage = Label.CS_ReturnOrderResponse;
                }
            }
            else{
                returnOrderWrapperResponse.isSuccess=false;   
                returnOrderWrapperResponse.errorMessage = Label.CS_SAPException;
            }            
            System.debug('result---->>' +returnOrderWrapperResponse);            
        }
        catch(Exception ex){
            ExceptionLogHandler.createErrorRecord(ex, 'CS_SAPSFReturnOrdersResponseData');
        }         
        return returnOrderWrapperResponse;
    }
}