/**
*  Created Date                                   : 05-Apr-21
*  Created By                                     : Prachi Dhaundiyal
*  Requirement Reference                          : To be updated 
*  Description			                          : Mock Response Generator for HTTP Callouts

*****  MODIFICATION HISTORY
Version Number		Date		Last Modified By		Description/User Story Number

**/
@isTest
global class CS_MockHttpResponseGenerator implements HttpCalloutMock {
    public String jsonBody;
    public Integer statusCode;
    public String statusValue;
    
    public CS_MockHttpResponseGenerator(String body, Integer sCode, String sValue){
        this.jsonBody = body;
        this.statusCode = sCode;
        this.statusValue = sValue;
    }
    
    /**
    *  Created Date            : 05-apr-2021
    *  Created By              : Prachi Dhaundiyal
    *  Requirement Reference   : 
    *  Description Of Method   : Set timeout for the request
    **/
    global HTTPResponse respond(HTTPRequest req) {
        // Create a fake response
        HttpResponse httpResponse = new HttpResponse();
        httpResponse.setBody(jsonBody);
        httpResponse.setStatus(statusValue);
        httpResponse.setStatusCode(statusCode);
        return httpResponse;
    }
    static TestMethod void CS_ConstantClassTest(){
        Test.startTest();
        CS_ConstantClass cons = new CS_ConstantClass();
        System.assertEquals(CS_ConstantClass.ACCOUNT_SOLD_TO,'Sold To');
        System.assertEquals(CS_ConstantClass.ACCOUNT_SHIP_TO,'Ship To');
        test.stopTest();
    }
}