/**
*  Created Date                                   : 02-April-2021 
*  Created By                                     : Indu
*  Requirement Reference                          : To be updated 
*  Description Of Method                          : To get the Order wrapper body

*****  MODIFICATION HISTORY
Version Number		Date		Last Modified By		Description/User Story Number
01                 02-April-2021                           CCM-124
**/

public with sharing class CS_SAPSFOrderResponse {
    public static String orderResponseBody;
    public static CS_OrderParser ordersResponse;
    public static FINAL String attributeName = 'SAP_All_Order_Service';
    public static String userLanguage = UserInfo.getLanguage();
    public static string language;
    public static String entityType='Order';
    public static String deliveryBlockReason='DeliveryBlockReason';
    public static String salesOrderType='SalesOrderType';
    public static String headerBillingBlockReason='HeaderBillingBlockReason';
    public static String overallSDProcessStatus='OverallSDProcessStatus';
    public static List<CS_SAP_Picklist_Mapping__c> orderTypekeyValue=new List<CS_SAP_Picklist_Mapping__c>();
    public static Map<String,Map<String,String>> orderTypeKeyValueMap = new Map<String,Map<String,String>>();
    public static Map<String,String> subOrderTypeKeyValueMap;
    
    /**
*  Created Date                                   : 02-April-2021
*  Created By                                     : Indu
*  Requirement Reference                          : CCM-124
*  Description Of Method                          : To get the response from http and to parse the json response and to get the wrapper response
**/
    
    @AuraEnabled(cacheable=true)
    public static CS_OrderParser getOrderItems(String SapCustomerId) {
        try{
            ordersResponse = new CS_OrderParser();
            
            for(PicklistEntry value: User.LanguageLocalekey.getDescribe().getPicklistValues()) {
                if(value.getValue() == userLanguage) {
                    language = value.getValue();                    
                }                
            }
            if(String.isNotBlank(SapCustomerId)) {
                ordersResponse.isSuccess=true;
                String actualDate = '/Date(1530403200000)/';
                Long longtime = long.valueOf( actualDate.substringBetween('(', ')') );
                DateTime FormattedDateTime = DateTime.newInstance(longtime);  
                DateTime dt = system.now().addMonths(Integer.valueOf(Label.CS_Creationdate));
                String dtString = String.valueOf(dt);
                dtString = dtString.replace(' ','T');
                HTTPCalloutFramework.HttpCalloutWrapper wrapper=new HTTPCalloutFramework.HttpCalloutWrapper();
                
                Map<String,String> Mapheader=new Map<String,String>();
                Mapheader.put('accept','application/json');
                Mapheader.put('Content-Type','application/json');
                
                Map<String,String> MapURL=new Map<String,String>();
                
                String EndpointURL='?$filter=SoldToParty+eq+\''+SapCustomerId+'\'+and+CreationDate+gt+datetime\''+dtString+'\'&$format=json';
                
                wrapper.integrationAttributeName=attributeName;
                wrapper.mapOfHttpHeaders=Mapheader;
                wrapper.mapOfURLParameters =MapURL;
                wrapper.endPointParamString=EndpointURL;                
                
                HTTPResponse response=HTTPCalloutFramework.sendHTTPGETRequest(wrapper);
                
                orderResponseBody =response.getBody();
                system.debug('orderResponseBody-->'+response.getStatusCode());
                if(response.getStatusCode()==200){
                    if(String.isNotBlank(response.getBody())) {                        
                        ordersResponse = CS_OrderParser.parse(orderResponseBody);
                        orderTypekeyValue =[Select CS_Property_Name__c,CS_Key__c,CS_Value__c,CS_Entity__c,CS_Language__c From
                                            CS_SAP_Picklist_Mapping__c where CS_Entity__c =: entityType AND CS_Language__c =: language];
                        for (CS_SAP_Picklist_Mapping__c kVal : orderTypekeyValue ) {
                            subOrderTypeKeyValueMap=new Map<String,String>();
                            if(orderTypeKeyValueMap.keySet().contains(kVal.CS_Property_Name__c)) {
                                
                                subOrderTypeKeyValueMap = orderTypeKeyValueMap.get(kVal.CS_Property_Name__c);
                                subOrderTypeKeyValueMap.put(kVal.CS_Key__c,kVal.CS_Value__c);
                                orderTypeKeyValueMap.put(kVal.CS_Property_Name__c,subOrderTypeKeyValueMap);
                            }
                            else {
                                subOrderTypeKeyValueMap.put(kVal.CS_Key__c,kVal.CS_Value__c);
                                orderTypeKeyValueMap.put(kVal.CS_Property_Name__c,subOrderTypeKeyValueMap); 
                            }
                            
                        }
                        for(CS_OrderParser.OrderList responseData:ordersResponse.d.results) {   
                            if(String.isNotBlank(responseData.creationDate)) { 
                                Long longtimedate = long.valueOf( responseData.creationDate.substringBetween('(', ')') );
                                responseData.formattedCreationDate = DateTime.newInstance(longtimedate);
                            }
                            if(String.isNotBlank(responseData.requestedDeliveryDate)) { 
                                Long longtimedate1 = long.valueOf( responseData.requestedDeliveryDate.substringBetween('(', ')') );
                                responseData.formattedDeliveryDate = DateTime.newInstance(longtimedate1);
                            }
                            if(String.isNotBlank(responseData.deliveryBlockReason)) {
                                if(orderTypeKeyValueMap.keySet().contains(deliveryBlockReason)) {
                                    
                                    subOrderTypeKeyValueMap = orderTypeKeyValueMap.get(deliveryBlockReason);
                                    if(subOrderTypeKeyValueMap.containsKey(responseData.deliveryBlockReason)) {
                                        
                                        responseData.formattedDeliveryBlockReason=responseData.deliveryBlockReason +' - '+ subOrderTypeKeyValueMap.get(responseData.deliveryBlockReason);
                                        
                                    }
                                }
                            }
                            
                            if(String.isNotBlank(responseData.salesOrderType)) {
                                if(orderTypeKeyValueMap.keySet().contains(salesOrderType)) {
                                    
                                    subOrderTypeKeyValueMap = orderTypeKeyValueMap.get(salesOrderType);
                                    if(subOrderTypeKeyValueMap.containsKey(responseData.salesOrderType)) {
                                        
                                        responseData.formattedSalesOrderType=responseData.salesOrderType +' - '+ subOrderTypeKeyValueMap.get(responseData.salesOrderType);
                                        
                                    }
                                }
                            }
                            if(String.isNotBlank(responseData.headerBillingBlockReason)) {
                                if(orderTypeKeyValueMap.keySet().contains(headerBillingBlockReason)){
                                    
                                    subOrderTypeKeyValueMap = orderTypeKeyValueMap.get(headerBillingBlockReason);
                                    if(subOrderTypeKeyValueMap.containsKey(responseData.headerBillingBlockReason)) {
                                        
                                        responseData.formattedHeaderBillingBlockReason=responseData.headerBillingBlockReason +' - '+ subOrderTypeKeyValueMap.get(responseData.headerBillingBlockReason);
                                        
                                    }
                                }
                            }
                            if(String.isNotBlank(responseData.overallSDProcessStatus)){
                                if(orderTypeKeyValueMap.keySet().contains(overallSDProcessStatus)){
                                    
                                    subOrderTypeKeyValueMap = orderTypeKeyValueMap.get(overallSDProcessStatus);
                                    if(subOrderTypeKeyValueMap.containsKey(responseData.overallSDProcessStatus)){
                                        
                                        responseData.formattedOverallSDProcessStatus=responseData.overallSDProcessStatus +' - '+ subOrderTypeKeyValueMap.get(responseData.overallSDProcessStatus);
                                    }
                                }
                            }
                        }
                        ordersResponse.isSuccess = true;
                    }
                    else{
                        ordersResponse.isSuccess=false; 
                        ordersResponse.errorMessage = Label.CS_ReturnOrderResponse;
                    }
                }
                else {
                    ordersResponse.isSuccess=false;   
                    ordersResponse.errorMessage = Label.CS_SAPException;
                }
                
            } // When Sap Customer Id equals to null
            
            else { // When SAP Customer Id is blank
                ordersResponse.errorMessage = Label.CS_NullSAPId;
                ordersResponse.isSuccess = false;
            }            
        }
        catch(Exception ex){
            ExceptionLogHandler.createErrorRecord(ex, 'CS_SAPSFOrderResponse');
        }         
        return ordersResponse;
    }   
}