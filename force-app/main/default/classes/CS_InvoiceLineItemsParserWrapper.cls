/**
*  Created Date                                    : 24-Mar-21
*  Created By                                      : Harshita Gupta
*  Requirement Reference                           : To be updated 
*  Description Of Method                           : Parse the JSON of the Invoice LineItems


*****  MODIFICATION HISTORY
Version Number		Date		Last Modified By		Description/User Story Number
**/
public class CS_InvoiceLineItemsParserWrapper {
    public InvoiceLineItemsWrapper d;
    @AuraEnabled public Boolean isSuccess;
    @AuraEnabled public String errorMessage;
    
    public class InvoiceLineItemsWrapper {
        public List<LineItems> results;
    }
    
    public class LineItems {
        public String BillingDocument;
        public String BillingDocumentItem;
        public String SalesDocumentItemCategory;
        public String SalesDocumentItemType;
        public DateTime FormattedCreationDate;
        public String ReturnItemProcessingType;
        public String CreatedByUser;
        public String CreationDate;
        public String CreationTime;
        public String ReferenceLogicalSystem;
        public String StorageLocation;
        public String BillingDocumentItemText;
        public String ServicesRenderedDate;
        public String BillingQuantity;
        public String BillingQuantityUnit;
        public String BillingQuantityInBaseUnit;
        public String BaseUnit;
        public String MRPRequiredQuantityInBaseUnit;
        public String BillingToBaseQuantityDnmntr;
        public String BillingToBaseQuantityNmrtr;
        public String ItemNetWeight;
        public String ItemWeightUnit;
        public String ItemVolume;
        public String ItemVolumeUnit;
        public String BillToPartyCountry;
        public String BillToPartyRegion;
        public String BillingPlanRule;
        public String BillingPlan;
        public String BillingPlanItem;
        public String NetAmount;
        public String GrossAmount;
        public String PricingDate;
        public String ProfitCenter;
        public String WBSElement;
        public String ControllingArea;
        public String ProfitabilitySegment;
        public String OrderID;
        public String CostCenter;
        public String BillingDocumentItemInPartSgmt;
        public String ExternalReferenceDocument;
        public String ExternalReferenceDocumentItem;
        public String ShippingPoint;
        public String HigherLevelItemUsage;
        public To_BillingDocument to_BillingDocument;
    }
    public class To_BillingDocument {
        public String PurchaseOrderByCustomer;
        public String BillingDocumentDate;
    }
    
    
    
    /**
	*  Created Date            : 24-Mar-2021
	*  Created By              : Harshita Gupta
	*  Requirement Reference   : To be updated 
	*  Description Of Method   : Deserialize JSON received from the response
	**/
    public static CS_InvoiceLineItemsParserWrapper parseInvoiceLineItems(String json) {
        return (CS_InvoiceLineItemsParserWrapper) System.JSON.deserialize(json, CS_InvoiceLineItemsParserWrapper.class);
    }
    
}