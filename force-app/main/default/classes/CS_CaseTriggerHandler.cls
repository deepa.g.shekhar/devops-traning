/**
* Created Date : 14-April-21
* Created By : Indu 
* Requirement Reference :
* Description Of Method : Handler class On Case Object 

***** MODIFICATION HISTORY
Version Number  Date  Last Modified By   Description/User   Story Number
                                                               CCM-357
**/
public class CS_CaseTriggerHandler implements CS_TriggerInterface {
    public void beforeInsert(List<SObject> newRecordList){
       // CS_CaseTriggerHelper.assignEntitlementOnNewlyCreatedRequests((List<Case>)Trigger.new);
    }
    public void afterInsert(List<SObject> newRecordList,Map<Id, SObject> newRecordsMap){ 
        CS_CaseTriggerHelper.publishCaseId((List<Case>)newRecordList);
    }
    public void beforeUpdate(Map<Id, SObject> newRecordsMap, Map<Id, SObject> oldRecordsMap){
        //CS_CaseTriggerHelper.assignEntitlementOnUpdatedRequestsAndCloseMilestonesOnClosedCases((List<Case>)Trigger.new);
    }
    public void afterUpdate(Map<Id, SObject> newRecordsMap, Map<Id, SObject> oldRecordsMap){
        // CS_CaseTriggerHelper.publishCaseId();       
    }
    public void beforeDelete(List<SObject> oldRecordList,Map<Id, SObject> oldRecordsMap){}
    public void afterDelete(Map<Id, SObject> oldRecordsMap){}
    public void afterUndelete(List<SObject> newRecordList,Map<Id, SObject> newRecordsMap){}  
    
}