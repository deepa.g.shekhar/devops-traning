/**
*  Created Date                                   : 25-Mar-2021
*  Created By                                     : Narmada
*  Requirement Reference                          : CCM 44
*  Description Of Method                          : Json Parsing for invoice response body

*****  MODIFICATION HISTORY
Version Number		Date		Last Modified By		Description/User Story Number
01                 25-Mar-2021                           CCM 44
**/

 	public With Sharing class CS_SAPSFInvoiceParser {
   	@AuraEnabled public InvoiceItem d;
   	@AuraEnabled public Boolean isSuccess;
   	@AuraEnabled public String errorMessage; 
	public class InvoiceItem {        
		@AuraEnabled public List<InvoiceResult> results;        
	}
    
	public class InvoiceResult {
         @AuraEnabled public String formattedBillingDocumentType;
		 @AuraEnabled public String billingDocument;
         @AuraEnabled public String sDDocumentCategory;
         @AuraEnabled public String billingDocumentCategory;
         @AuraEnabled public String billingDocumentType;
         @AuraEnabled public String creationDate;
         @AuraEnabled public DateTime formattedCreationDate;
         @AuraEnabled public String creationTime;
         @AuraEnabled public String lastChangeDate;
         @AuraEnabled public String lastChangeDateTime;
         @AuraEnabled public String logicalSystem;
         @AuraEnabled public String salesOrganization;
         @AuraEnabled public String distributionChannel;
         @AuraEnabled public String division;
         @AuraEnabled public String billingDocumentDate;
         @AuraEnabled public String resolutionDate;
         @AuraEnabled public Boolean billingDocumentIsCancelled;
		 @AuraEnabled public String cancelledBillingDocument;
		 @AuraEnabled public String foreignTrade;
		 @AuraEnabled public String isExportDelivery;
		 @AuraEnabled public String billingDocCombinationCriteria;
		 @AuraEnabled public Boolean manualInvoiceMaintIsRelevant;
		 @AuraEnabled public Boolean isIntrastatReportingRelevant;
		 @AuraEnabled public Boolean isIntrastatReportingExcluded;
		 @AuraEnabled public String totalNetAmount;
		 @AuraEnabled public String transactionCurrency;
		 @AuraEnabled public String statisticsCurrency;
		 @AuraEnabled public String taxAmount;
		 @AuraEnabled public String totalGrossAmount;
		 @AuraEnabled public String customerPriceGroup;
		 @AuraEnabled public String priceListType;
		 @AuraEnabled public String taxDepartureCountry;
		 @AuraEnabled public String vATRegistration;
		 @AuraEnabled public String vATRegistrationOrigin;
		 @AuraEnabled public String vATRegistrationCountry;
    	 @AuraEnabled public String hierarchyTypePricing;
		 @AuraEnabled public String customerTaxClassification1;
		 @AuraEnabled public String customerTaxClassification2;
		 @AuraEnabled public String customerTaxClassification3;
		 @AuraEnabled public String customerTaxClassification4;
		 @AuraEnabled public String customerTaxClassification5;
		 @AuraEnabled public String customerTaxClassification6;
		 @AuraEnabled public String customerTaxClassification7;
		 @AuraEnabled public String customerTaxClassification8;
		 @AuraEnabled public String customerTaxClassification9;
		 @AuraEnabled public Boolean isEUTriangularDeal;
		 @AuraEnabled public String sDPricingProcedure;
		 @AuraEnabled public String shippingCondition;
		 @AuraEnabled public String incotermsVersion;
		 @AuraEnabled public String incotermsClassification;
		 @AuraEnabled public String incotermsTransferLocation;
		 @AuraEnabled public String incotermsLocation1;
		 @AuraEnabled public String incotermsLocation2;
		 @AuraEnabled public String payerParty;
		 @AuraEnabled public String contractAccount;
		 @AuraEnabled public String customerPaymentTerms;
		 @AuraEnabled public String paymentMethod;
		 @AuraEnabled public String paymentReference;
		 @AuraEnabled public String fixedValueDate;
		 @AuraEnabled public String additionalValueDays;
		 @AuraEnabled public String sEPAMandate;
		 @AuraEnabled public String companyCode;
		 @AuraEnabled public String fiscalYear;
		 @AuraEnabled public String accountingDocument;
		 @AuraEnabled public String customerAccountAssignmentGroup;
		 @AuraEnabled public Boolean accountingExchangeRateIsSet;
		 @AuraEnabled public String absltAccountingExchangeRate;
		 @AuraEnabled public Boolean acctgExchangeRateIsIndrctQtan;
		 @AuraEnabled public String exchangeRateDate;
		 @AuraEnabled public String exchangeRateType;
		 @AuraEnabled public String documentReferenceID;
		 @AuraEnabled public String assignmentReference;
		 @AuraEnabled public String dunningArea;
		 @AuraEnabled public String dunningBlockingReason;
		 @AuraEnabled public String dunningKey;
		 @AuraEnabled public String internalFinancialDocument;
		 @AuraEnabled public Boolean isRelevantForAccrual;
		 @AuraEnabled public String soldToParty;
		 @AuraEnabled public String partnerCompany;
		 @AuraEnabled public String purchaseOrderByCustomer;
		 @AuraEnabled public String customerGroup;
		 @AuraEnabled public String country;
		 @AuraEnabled public String cityCode;
		 @AuraEnabled public String salesDistrict;
		 @AuraEnabled public String region;
		 @AuraEnabled public String county;
		 @AuraEnabled public String creditControlArea;
		 @AuraEnabled public String customerRebateAgreement;
		 @AuraEnabled public String salesDocumentCondition;
		 @AuraEnabled public String overallSDProcessStatus;
		 @AuraEnabled public String overallBillingStatus;
         @AuraEnabled public String formattedOverallBillingStatus;
		 @AuraEnabled public String accountingPostingStatus;
		 @AuraEnabled public String accountingTransferStatus;
		 @AuraEnabled public String billingIssueType;
		 @AuraEnabled public String invoiceListStatus;
		 @AuraEnabled public String ovrlItmGeneralIncompletionSts;
		 @AuraEnabled public String overallPricingIncompletionSts;
		 @AuraEnabled public String invoiceClearingStatus;
		 @AuraEnabled public String billingDocumentListType;
		 @AuraEnabled public String billingDocumentListDate;
        
		
	}
    
@AuraEnabled(cacheable=true)
	public static CS_SAPSFInvoiceParser parse(String json) {
     
		return (CS_SAPSFInvoiceParser) System.JSON.deserialize(json, CS_SAPSFInvoiceParser.class);
	}
}