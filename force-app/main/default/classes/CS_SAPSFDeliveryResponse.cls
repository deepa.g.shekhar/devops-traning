/**
*  Created Date                                    : 06-Apr-21
*  Created By                                      : Harshita Gupta
*  Requirement Reference                           : To be updated 
*  Description Of Method                           : Integration of Delivery

*****  MODIFICATION HISTORY
Version Number		Date		Last Modified By		Description/User Story Number
**/
public class CS_SAPSFDeliveryResponse {
    public static String deliveryResponseBody;
    public static CS_SAPSFDeliveryParser deliveryResponse;
    public static FINAL String attributeName = 'SAP_All_Delivery_Document_Service';
    public static String userLanguage = UserInfo.getLanguage();
    public static string language;
    public static String entityType = 'Delivery';
    public static String OverallProofOfDeliveryStatus = 'OverallProofOfDeliveryStatus';
    public static String DeliveryDocumentType ='DeliveryDocumentType';
    public static  List<CS_SAP_Picklist_Mapping__c> deliveryTypekeyValue=new List<CS_SAP_Picklist_Mapping__c>();
    public static Map<String,Map<String,String>> deliveryTypeKeyValueMap = new Map<String,Map<String,String>>();
    public static Map<String,String> subDeliveryTypeKeyValueMap;
    
    
    /**
*  Created Date            : 06-Apr-2021
*  Created By              : Harshita Gupta
*  Requirement Reference   : To be updated 
*  Description Of Method   : Get Delivery Details
**/
    @AuraEnabled(cacheable=true)
    public static CS_SAPSFDeliveryParser getDeliveryItems(String SapCustomerId){
        try{
            if(String.isNotBlank(SapCustomerId)){
                deliveryResponse = new CS_SAPSFDeliveryParser();
                if(String.isNotBlank(SapCustomerId)){
                    String actualDate = '/Date(1530403200000)/';
                    Long longtime = long.valueOf( actualDate.substringBetween('(', ')') );
                    DateTime FormattedDateTime = DateTime.newInstance(longtime);  
                    DateTime dt = system.now().addYears(Integer.valueOf(Label.CS_Creationdate));
                    String dtString = String.valueOf(dt);
                    dtString = dtString.replace(' ','T');
                    
                    deliveryResponse.isSuccess=true;
                    HTTPCalloutFramework.HttpCalloutWrapper wrapper=new HTTPCalloutFramework.HttpCalloutWrapper();
                    
                    Map<String,String> Mapheader=new Map<String,String>();
                    Mapheader.put('accept','application/json');
                    Mapheader.put('Content-Type','application/json');
                    
                    Map<String,String> MapURL=new Map<String,String>();
                    
                    String EndpointURL='?$filter=SoldToParty+eq+\''+SapCustomerId+'\'+and+CreationDate+gt+datetime\''+dtString+'\'&$format=json&$top=100';
                    
                    wrapper.integrationAttributeName=attributeName;
                    wrapper.mapOfHttpHeaders=Mapheader;
                    wrapper.mapOfURLParameters =MapURL;
                    wrapper.endPointParamString=EndpointURL;
                    
                    HTTPResponse response=HTTPCalloutFramework.sendHTTPGETRequest(wrapper);
                    
                    deliveryResponseBody =response.getBody();
                    System.debug('--->>>deliveryResponseBody'+deliveryResponseBody);
                    if(response.getStatusCode()==200){
                        if(String.isNotBlank(response.getBody())){                        
                            deliveryResponse = CS_SAPSFDeliveryParser.parse(deliveryResponseBody);
                            System.debug(deliveryResponse.d.results);
                            deliveryTypekeyValue =[Select CS_Property_Name__c,CS_Key__c,CS_Value__c,CS_Entity__c,CS_Language__c From
                                                   CS_SAP_Picklist_Mapping__c where CS_Entity__c =: entityType];
                            
                            for (CS_SAP_Picklist_Mapping__c kVal : deliveryTypekeyValue )
                            {
                                subDeliveryTypeKeyValueMap=new Map<String,String>();
                                if(deliveryTypeKeyValueMap.keySet().contains(kVal.CS_Property_Name__c)){
                                    
                                    subDeliveryTypeKeyValueMap = deliveryTypeKeyValueMap.get(kVal.CS_Property_Name__c);
                                    subDeliveryTypeKeyValueMap.put(kVal.CS_Key__c,kVal.CS_Value__c);
                                    deliveryTypeKeyValueMap.put(kVal.CS_Property_Name__c,subDeliveryTypeKeyValueMap);
                                }
                                else{
                                    subDeliveryTypeKeyValueMap.put(kVal.CS_Key__c,kVal.CS_Value__c);
                                    deliveryTypeKeyValueMap.put(kVal.CS_Property_Name__c,subDeliveryTypeKeyValueMap); 
                                }
                                
                            }
                            
                            for(CS_SAPSFDeliveryParser.DeliveryItems responseData : deliveryResponse.d.results){
                                if(String.isNotBlank(responseData.creationDate)){
                                    Long longtimedate = long.valueOf( responseData.CreationDate.substringBetween('(', ')') );
                                    responseData.FormattedCreationDate = DateTime.newInstance(longtimedate);
                                }
                                if(String.isNotBlank(responseData.ActualGoodsMovementDate)) {
                                    Long longtimedateActual = long.valueOf( responseData.ActualGoodsMovementDate.substringBetween('(', ')') );
                                    responseData.FormattedActualGoodsMovementDate = DateTime.newInstance(longtimedateActual);
                                }
                                
                                if(String.isNotBlank(responseData.DeliveryDocumentType)){
                                    if(deliveryTypeKeyValueMap.keySet().contains('DeliveryDocumentType')){
                                        subDeliveryTypeKeyValueMap = deliveryTypeKeyValueMap.get('DeliveryDocumentType');
                                        if(subDeliveryTypeKeyValueMap.containsKey(responseData.DeliveryDocumentType)){
                                            if(String.isNotBlank(subDeliveryTypeKeyValueMap.get(responseData.DeliveryDocumentType))){
                                                responseData.FormattedDeliveryDocumentType = responseData.DeliveryDocumentType +' - '+ subDeliveryTypeKeyValueMap.get(responseData.DeliveryDocumentType);
                                            }
                                            else{
                                                responseData.FormattedDeliveryDocumentType = responseData.DeliveryDocumentType;
                                            }
                                        }
                                        
                                    }
                                }
                                
                                if(String.isNotBlank(responseData.OverallProofOfDeliveryStatus)){
                                    if(deliveryTypeKeyValueMap.keySet().contains('OverallProofOfDeliveryStatus')){
                                        subDeliveryTypeKeyValueMap = deliveryTypeKeyValueMap.get('OverallProofOfDeliveryStatus');
                                        if(subDeliveryTypeKeyValueMap.containsKey(responseData.OverallProofOfDeliveryStatus)){
                                            if(String.isNotBlank(subDeliveryTypeKeyValueMap.get(responseData.OverallProofOfDeliveryStatus))){
                                                responseData.FormattedOverallProofOfDeliveryStatus = responseData.OverallProofOfDeliveryStatus +' - '+ subDeliveryTypeKeyValueMap.get(responseData.OverallProofOfDeliveryStatus);
                                            }
                                            else{
                                                responseData.FormattedOverallProofOfDeliveryStatus = responseData.OverallProofOfDeliveryStatus;
                                            }
                                        }
                                    }
                                }
                            }
                            deliveryResponse.isSuccess = true;   
                        }
                        else{
                            deliveryResponse.isSuccess=false; 
                            deliveryResponse.errorMessage = Label.CS_ReturnOrderResponse;
                        }
                    }
                    else{
                        deliveryResponse.isSuccess=false;   
                        deliveryResponse.errorMessage = Label.CS_SAPException;
                    }
                    
                    System.debug('result---->>' +deliveryResponse);
                } 
            }
            // When SAP Customer Id is blank
            else { 
                deliveryResponse.isSuccess = false;
                deliveryResponse.errorMessage = Label.CS_NullSAPId; 
                
            }        
            
        }
        catch(Exception ex){
            ExceptionLogHandler.createErrorRecord(ex, 'CS_deliveryResponseData');
        }         
        return deliveryResponse;
    }
}