/**
	* Created Date : 14-April-21
	* Created By : Bhushan K
	* Requirement Reference :
	* Description Of Method : Helper class On OrderItem Object 
**/
public class CS_OrderItemTriggerHelper {
   @testVisible private static List<CS_Return_Orders_PE__e> returnOrderEveList = new List<CS_Return_Orders_PE__e>();
    public static void publishReturnOrder(List<OrderItem> orderItemList) {
        try {
            List<CS_SFSAPReturnOrderParser> parserData = new List<CS_SFSAPReturnOrderParser>();
            Set<Id> listOfOrderLineIds = new Set<Id>();
            for (OrderItem ordItem : orderItemList) {
                listOfOrderLineIds.add(ordItem.Id);
            }
            List<OrderItem> listOfOrderLineItems = new List<OrderItem>();
            listOfOrderLineItems = [Select Id,Order.CS_Return_Order_Type__c,Order.CS_Requested_Execution_Date__c,
                                    Order.CS_Return_Reason__c,Order.Status,Order.CS_Order_Date__c,CS_Case__c,CS_Case__r.Comments,
                                    CS_Case__r.CS_Invoice__r.CS_SAP_Invoice_Number__c,CS_Order_Line_Status__c,
                                    CS_Case__r.Account.BillingCountry,CS_Case__r.Account.CS_SAP_Customer_Id__c,
                                    CS_Inspection_Code__c,CS_Quantity__c,CS_SAP_Invoice_Line_Id__c,CS_Refund_Type__c,
                                    CS_Return_Reason_Line__c,CS_Unit_Of_Measure__c,CS_Plant__c,Product2.CS_GMID__c,
                                    CS_SAP_Reference_Delivery__c,orderId
                                    FROM OrderItem WHERE Id IN:listOfOrderLineIds];
            
            for(OrderItem ordItem : listOfOrderLineItems){
                CS_SFSAPReturnOrderParser wrap = new CS_SFSAPReturnOrderParser();
                CS_SFSAPReturnOrderParser.OrderDetails orderWrap = new CS_SFSAPReturnOrderParser.OrderDetails();
                orderWrap.ReturnOrderType  = ordItem.Order.CS_Return_Order_Type__c;
                orderWrap.OrderDate = String.valueOf(ordItem.Order.CS_Order_Date__c);
                orderWrap.OrderStatus = String.valueOf(ordItem.Order.Status);
                orderWrap.ReferenceDelivery = (ordItem.CS_SAP_Reference_Delivery__c);
                orderWrap.ReferenceInvoice = String.valueOf(ordItem.CS_Case__r.CS_Invoice__r.CS_SAP_Invoice_Number__c);
                orderWrap.RequestedExecutionDate = String.valueOf(ordItem.Order.CS_Requested_Execution_Date__c);
                wrap.OrderDetails = orderWrap;
                
                CS_SFSAPReturnOrderParser.OrderLineItems orderWrap1 = new CS_SFSAPReturnOrderParser.OrderLineItems();
                List<CS_SFSAPReturnOrderParser.OrderLineItemDetails> listOfOrderWrap = new List<CS_SFSAPReturnOrderParser.OrderLineItemDetails>();
                CS_SFSAPReturnOrderParser.OrderLineItemDetails orderWrap2 = new CS_SFSAPReturnOrderParser.OrderLineItemDetails();
                orderWrap2.SourceSystemOrderLIneId = String.valueOf(ordItem.Id);
                orderWrap2.InspectionCode = String.valueOf(ordItem.CS_Inspection_Code__c);
                orderWrap2.OrderLineStatus = String.valueOf(ordItem.CS_Order_Line_Status__c);
                orderWrap2.Plant = String.valueOf(ordItem.CS_Plant__c);
                orderWrap2.ProductId = String.valueOf(ordItem.Product2.CS_GMID__c);
                orderWrap2.Quantity = String.valueOf(ordItem.CS_Quantity__c);
                orderWrap2.ReferenceInvoiceLineId = String.valueOf(ordItem.CS_SAP_Invoice_Line_Id__c);
                orderWrap2.RefundType = String.valueOf(ordItem.CS_Refund_Type__c);
                orderWrap2.ReturnReasonLine = String.valueOf(ordItem.CS_Return_Reason_Line__c);
                orderWrap2.UnitOfMeasure = String.valueOf(ordItem.CS_Unit_Of_Measure__c);
                orderWrap2.SourceSystemOrderLIneId = String.valueOf(ordItem.orderId);
                listOfOrderWrap.add(orderWrap2);
                orderWrap1.OrderLineItemDetails = listOfOrderWrap;
                wrap.OrderLineItems = orderWrap1;
                CS_SFSAPReturnOrderParser.CaseDetails caseWrap = new CS_SFSAPReturnOrderParser.CaseDetails();
                caseWrap.CaseID = JSON.serialize(ordItem.CS_Case__c);
                wrap.CaseDetails = caseWrap;
                CS_SFSAPReturnOrderParser.CustomerDetails customerWrap = new CS_SFSAPReturnOrderParser.CustomerDetails();
                customerWrap.CustomerID = JSON.serialize(ordItem.CS_Case__r.Account.CS_SAP_Customer_Id__c);
                wrap.CustomerDetails = customerWrap;
                parserData.add(wrap);
            }    
            
            for (CS_SFSAPReturnOrderParser data : parserData) {
                CS_Return_Orders_PE__e returnOrderEvent = new CS_Return_Orders_PE__e();
                returnOrderEvent.CS_Customer_Details__c = JSON.serialize(data.CustomerDetails);
                returnOrderEvent.CS_Case_Details__c = JSON.serialize(data.CaseDetails);
                returnOrderEvent.CS_Order_Details__c = JSON.serialize(data.OrderDetails);
                returnOrderEvent.CS_Order_Line_Items__c =JSON.serialize(data.OrderLineItems);
                returnOrderEveList.add(returnOrderEvent);   
            }
            system.debug('returnOrderEveList==>'+returnOrderEveList);
            List<Database.SaveResult> results = EventBus.publish(returnOrderEveList);
            for (Database.SaveResult sr : results) {
                if (sr.isSuccess()) {
                    System.debug('Successfully published event.');
                } else {
                    for(Database.Error err : sr.getErrors()) {
                        System.debug('Error returned: ' + err.getStatusCode() + ' - ' +err.getMessage());
                    }
                }
            }  
        }
        catch(Exception ex) {
            ExceptionLogHandler.createErrorRecord(ex, 'CS_OrderItemTriggerHelper');
        }  
    }       
}