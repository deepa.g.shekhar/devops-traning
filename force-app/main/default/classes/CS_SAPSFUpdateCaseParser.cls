/**
*  Created Date                                   : 05-Apr-21
*  Created By                                     : Pratigyan
*  Requirement Reference                          : CCM 373
*  Description Of Method                          : Parse the JSON of the Invoice Notification.

*****  MODIFICATION HISTORY
Version Number		Date		Last Modified By		Description/User Story Number

**/
public class CS_SAPSFUpdateCaseParser {
    @AuraEnabled public caseDetails caseDetails;
    @AuraEnabled public String errorMessage;
    public class caseDetails{
        
    @AuraEnabled public String caseID;
    @AuraEnabled public String caseStatus;
    @AuraEnabled public String invoiceType;
    @AuraEnabled public String SAPInvoiceId;
    @AuraEnabled public String systemName;
   							
    }
    
    /**  Description Of Method   : Deserialize JSON received from the Case Updation Publisher
	**/
    
    @AuraEnabled(cacheable=true)
    public static CS_SAPSFUpdateCaseParser parse(String json) {
        return (CS_SAPSFUpdateCaseParser) System.JSON.deserialize(json, CS_SAPSFUpdateCaseParser.class);
    }
    
    
}