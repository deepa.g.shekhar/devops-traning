/**
*  Created Date                                   : 20-Apr-21
*  Created By                                     : Spoorthi B R
*  Requirement Reference                          : CCM - 365
*  Description			                          : This interface is used to update CCM Case After Acknowledge GR via stock mov.
													(Advance Return Delivery ZLRA). Corresponds to S&D - 01
*****  MODIFICATION HISTORY
Version Number		Date		Last Modified By		Description/User Story Number

**/
public class CS_ShippingNotificationTriggerHelper {    
    public static CS_SAPSFShippingNotificationParser returnOrderwrapperResponse;    
/**
*  Created Date            : 20-Apr-2021
*  Created By              : Spoorthi B R
*  Requirement Reference   : CCM - 365
*  Description Of Method   : Update case and related Order Products based on the response from iPaaS
**/    
    public static void getEventData(List<CS_Shipping_Notification_PE__e> res) {
            CS_SAPSFShippingNotificationParser orderParse = new CS_SAPSFShippingNotificationParser();
        	List<Case> caseUpdate = new List<Case>(); 
       		List<OrderItem> oiUpdateList = new List<OrderItem>();  
            List<CS_SAPSFShippingNotificationParser> caseWrapperResponseList = new List<CS_SAPSFShippingNotificationParser>();
        	List<CS_SAPSFShippingNotificationParser> orderWrapperResponseList = new List<CS_SAPSFShippingNotificationParser>();
        	List<CS_SAPSFShippingNotificationParser.OrderLineItemDetails> orderLineItemsWrap = new List<CS_SAPSFShippingNotificationParser.OrderLineItemDetails>();
        	Set<Id> oiIds = new Set<Id>();   
        	Set<Id> caseIds = new Set<Id>();       
       		Map<Id, String> caseStatusMap = new Map<Id, String>();
            Map<Id, String> prodMap = new Map<Id, STring>();        
            Map<String, String> gMap = new Map<String, String>();
        	
        	if(res != NULL) { 
                for(CS_Shipping_Notification_PE__e parseRes : res) {
                    if(String.isNotBlank(parseRes.CS_Case_Details__c)) {
                    string parseCase = parseRes.CS_Case_Details__c;
                    returnOrderwrapperResponse = CS_SAPSFShippingNotificationParser.parse(parseCase); 
                    String myIdPrefix = String.valueOf(returnOrderwrapperResponse.CaseDetails.CaseID).substring(0,3);
                    if((String.isNotBlank(returnOrderwrapperResponse.CaseDetails.CaseID)) 
                           && ((returnOrderwrapperResponse.CaseDetails.CaseID.length() == 18)
                               || (returnOrderwrapperResponse.CaseDetails.CaseID.length() == 15))
                           && (myIdPrefix == '500')) {
                    caseWrapperResponseList.add(returnOrderwrapperResponse);
                    caseIds.add(Id.valueOf(returnOrderwrapperResponse.CaseDetails.CaseID));
                           } else {
                                throw new CS_CustomException(CS_CustomException.CS_INVALID_CASE_ID); 
                           }
                    } else {
                        throw new CS_CustomException(CS_CustomException.CS_CASERESPONSE); 
                    }
                    if(String.isNotBlank(parseRes.CS_Order_Line_Items__c)) {
                    String parseOrderItem = parseRes.CS_Order_Line_Items__c;                   
                    orderParse = CS_SAPSFShippingNotificationParser.parse(parseOrderItem);
                    orderLineItemsWrap.addAll(orderParse.OrderLineItems.OrderLineItemDetails);
                    } else {
                        throw new CS_CustomException(CS_CustomException.CS_ORDERPRODUCTRESPONSE);
                    }
                    
                }
            }
        
        for(Case cMap : [SELECT Id, Status from Case where ID IN : caseIds]) {
            caseStatusMap.put(cMap.Id,cMap.Status);
        }
      
        if(caseWrapperResponseList != NULL) {
        for(CS_SAPSFShippingNotificationParser wrap : caseWrapperResponseList) {
            	if(caseStatusMap.containsKey(Id.valueOf(wrap.CaseDetails.CaseID))) {
                    Case c = new Case();
                    c.Id = Id.valueOf(wrap.CaseDetails.CaseID); 
                    c.Status = wrap.CaseDetails.caseStatus;        
                    caseUpdate.add(c);   
            } 
        }
        } else {
            throw new CS_CustomException(CS_CustomException.CS_ORDERPRODUCTRESPONSE);
        }
       
        for(CS_SAPSFShippingNotificationParser.OrderLineItemDetails wrap: orderLineItemsWrap) { 
            oiIds.add(Id.ValueOf(wrap.SourceSystemOrderLIneId));            
            gMap.put(wrap.ProductId, wrap.OrderLineStatus);
        }
        
        for(Product2 prod : [Select Id, CS_GMID__c from Product2 where CS_GMID__c IN : gMap.keyset()]) {
            prodMap.put(prod.Id, gmap.get(prod.CS_GMID__c));
        }
        
        for(OrderItem oi : [SELECT Id, CS_Case__c, CS_Order_Line_Status__c, Product2.CS_GMID__c FROM OrderItem WHERE ID IN: oiIds AND CS_Case__c IN: caseIds]) {
           		orderItem o = new OrderItem();
                o.Id = oi.Id;
                o.CS_Order_Line_Status__c = prodMap.get(oi.Product2Id);                          
                oiUpdateList.add(o);          
        }       
      
      update oiUpdateList;
      update caseUpdate;

    }
}