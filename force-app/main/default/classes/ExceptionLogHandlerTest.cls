/**
*  Created Date                                   : 22-Feb-21
*  Created By                                     : Udaybhan Singh Kushwah
*  Requirement Reference                          : To be updated 
*  Description Of Method                          : Test Class for ExceptionLogHandler class.

*****  MODIFICATION HISTORY
Version Number		Date		Last Modified By		Description/User Story Number

**/

@isTest
public class ExceptionLogHandlerTest {
    
    @isTest
    public static void testCreateErrorRecordPositive(){
        try {
            Account m = new Account();
            insert m;
        } catch (Exception e) {
            ExceptionLogHandler.createErrorRecord(e,'Test Exception');
        }
    }
    
    @isTest
    public static void testCreateErrorRecordNegative(){
        try {
            Account m = new Account();
            insert m;
        } catch (Exception e) {
            //providing more than 255 characters in 'functionalityName' to throw standard exception
            String functionalityName = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris hendrerit purus tortor, '+
                						'ut laoreet lectus porttitor a. Aenean ullamcorper blandit mauris sit amet varius. '+
                						'Etiam aliquam venenatis ante, nec dignissim massa ornare ut. Morbi et eros neque. '+
                						'Pellentesque sit amet dolor ut ante ultricies pretium.';
            ExceptionLogHandler.createErrorRecord(e,functionalityName);
        }
    }
 
}