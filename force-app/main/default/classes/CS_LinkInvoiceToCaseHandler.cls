/**
*  Created Date                                   : 24-March-21
*  Created By                                     : Udaybhan Singh Kushwah
*  Requirement Reference                          : To be updated 
*  Description Of Method                          : This Method is to get SoldToId from case.

*****  MODIFICATION HISTORY
Version Number		Date		Last Modified By		Description/User Story Number
1.0                 24-March-2021   Udaybhan Singh		CCM-20
**/
public with sharing class CS_LinkInvoiceToCaseHandler {
    @AuraEnabled
    public static string getCase(String recId){
        
        case caseDetail = [SELECT Id,CS_Sold_To__c,accountId FROM Case WHERE Id =:recId][0];
        CS_SAPSFInvoiceParser invoiceResult = CS_Invoice_LwcController.getAccountDetails(caseDetail.CS_Sold_To__c);
        return JSON.serialize(invoiceResult);
        
    }
    /**
    *  Created Date            : 24-March-21
    *  Created By              : Udaybhan Singh Kushwah
    *  Requirement Reference   : To be updated 
    *  Description Of Method   : This Method to create the invoice, invoice Line item,claim Item records.
    **/
    @AuraEnabled
    public static string insertInvoice(string StrInvoiceData,string strInvoiceLineItemData , string strClaimItemData,string strCaseData)
    {
        try
        {
            
            list<CS_Invoice__c> lstinvoice =  (list<CS_Invoice__c>)JSON.deserializeStrict(StrInvoiceData, list<CS_Invoice__c>.class);
            list<case> lstCase =  (list<case>)JSON.deserializeStrict(strCaseData, list<case>.class);
            list<CS_Invoice_Line_Item__c> lstInvoiceLineItem =  (list<CS_Invoice_Line_Item__c>)JSON.deserializeStrict(strInvoiceLineItemData,list<CS_Invoice_Line_Item__c>.class);
            list<CS_Claim_Line_Item__c> lstClaimItem =  (list<CS_Claim_Line_Item__c>)JSON.deserializeStrict(strClaimItemData,list<CS_Claim_Line_Item__c>.class);
            System.debug('-->>lstCase' +lstCase);
            System.debug('-->>lstInvoiceLineItem' +lstInvoiceLineItem);
            System.debug('-->>lstClaimItem' +lstClaimItem);
            caseEstimateValue(lstClaimItem);
            
            list<SObject> lstSobject = new list<SObject>();
            string errorString;
            string PassString;
            if(lstinvoice != null && lstinvoice.size()>0)
            {
                
                if(lstInvoiceLineItem != null && lstInvoiceLineItem.size()>0)
                {
                    lstSobject.addAll(lstInvoiceLineItem);
                }
                if(lstClaimItem != null && lstClaimItem.size()>0)
                {
                    lstSobject.addAll(lstClaimItem);
                }
            }
            upsert (list<CS_Invoice__c>)lstinvoice CS_SAP_Invoice_Number__c;
            update lstCase;
            insert lstSobject;
            return 'true';
        }
        catch(Exception ex){
            
            ExceptionLogHandler.createErrorRecord(ex, 'CS_LinkInvoiceToCaseHandler');
            system.debug('ex=====>>');
            throw new AuraHandledException(ex.getMessage());
        }
        
        
       
       
        
    }
    public static void caseEstimateValue(list<CS_Claim_Line_Item__c> lstClaimItem){
        if(lstClaimItem != null && !lstClaimItem.isEmpty()){
            Case cs = [SELECT Id, CS_Case_Estimate_Value__c from Case where id=:lstClaimItem[0].CS_Case__c];
            Integer estimated = 0;
            
            for (CS_Claim_Line_Item__c items : lstClaimItem){
                estimated += Integer.valueof(items.CS_Net_Value__c) * Integer.valueof(items.CS_Quantity__c);
                
            }
            cs.CS_Case_Estimate_Value__c= String.valueof(estimated);
            update cs;
        }
    } 
    
}