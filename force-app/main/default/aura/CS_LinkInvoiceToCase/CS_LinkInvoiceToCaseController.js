({
    doInit : function(component, event, helper) {
        helper.sendCaseId(component, event, helper);
    },
    nextEventHandler : function(cmp, event, helper){
        
        let eventData = event.getParam('result');
        let caseid = event.getParam('caseid');
        console.log('caseid===>>',caseid);
        cmp.set("v.caseId",caseid)
        cmp.set('v.invoice',JSON.parse(JSON.stringify(eventData)));
        cmp.set('v.headerText','Link Invoice LineItem To Invoice');
        console.log('eventData[0]==in aura==>>',eventData[0].CS_SAP_Invoice_Number__c);
        cmp.set('v.sapInvoiceNumber',eventData[0].CS_SAP_Invoice_Number__c);
        console.log('eventData==in aura==>>',JSON.parse(JSON.stringify(eventData)));
        
    },
    handleSubmitRecords : function(cmp, event, helper){
        console.log('in handleSubmitRecords==>>');
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();
        
    }
})