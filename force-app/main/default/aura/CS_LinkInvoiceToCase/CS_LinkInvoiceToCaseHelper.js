({
    sendCaseId : function(component, event, helper) {
        // create a one-time use instance of the getCase action
        // in the server-side controller
        var action = component.get("c.getCase");
        
        action.setParams({
            "recId": component.get("v.recordId")
        });
        // Create a callback that is executed after 
        // the server-side action returns
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                //Set the component case attribute to the returned case object
                var responseDetail =  response.getReturnValue();  
            }
            else  if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                    errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        
        // $A.enqueueAction adds the server-side action to the queue.
        $A.enqueueAction(action);
    }
})